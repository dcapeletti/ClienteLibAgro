# coding=utf-8
'''
Created on 17/10/2015

@author: diegoariel
Este archivo contiene a la clase Mediador y a sus distintas implementaciones.
Puede haber varios mediadores correlacionados por distintos patrones si es necesario.
'''
import wx, os
from observador import Observador
from variablesutiles import AdminVariables
from chatnetworking import ChatConnect
from src.widgets import widget
from src.widgets.widget import *
import rendezvous
from service_bd import *
import funciones_varias
from src.dialogs.GUITareasProgramadas import *
from src.dialogs.GUIConfigAlertas import *
from src.widgets.widget import *
from src.dialogs import GUIAcercaDe, GUIIniciarSesion
from src.dialogs import GUIConfigParametrosAlimentos
from src.dialogs.GUIConfigurarFechaYHora import GUIConfigurarFechaYHora
import time
from datetime import datetime
from src.utils.serviceReconect import ServiceReconect
from src.dialogs.GUIConfigCompuerta import GUIConfigCompuerta

class Mediador():
    
    def widgetModificado(self, control, evento, mensaje=None):
        #el parametro mensaje, se puede usar para enviar distintos mensajes de un mismo control.
        raise NotImplemented('Este método es abstracto. Debe sobrescribirlo.')
    

class DirectorView(Mediador, Observador):
    def __init__(self):
        print "Version wxpython: ", wx.version()
        self.connected = False
        self.adminvariables= AdminVariables() #Clase singleton para almacenar variables de interes comunes al cliente
        self.adminvariables.setHostServer("192.168.1.106") #Host de prueba rápida
        x = ServiceBD(serverBD="localhost", nombreBD="comedor", password="33426000", usuario="dcapeletti")
        self.adminvariables.setConexionBd(x)
        self.inicializarGUI() #La GUI es lo último que se construye porque sino se entra en su loop
    
    def widgetModificado(self, control, evento, mensaje=None):
        #Escribir la lógica del mediador...Hacer algo con el widget que lanzo el evento...
        objeto = evento.GetEventObject()
        '''
        nuevoMenu = wx.Menu()
        item = nuevoMenu.Append(-1, '&Nuevo archivo',
                'Restablece el nombre a localhost')
        control.Append(nuevoMenu, 'Nuevo')'''
        
        if type(objeto) == wx.Menu: #Es un menu el que ha laznado el evento
            label_menu = objeto.GetLabel(evento.GetId()) #Para Windows funciona &
            print label_menu
            if evento.GetId() == 1: #Por el número del Id, obtengo el origen del menu
                print self.adminvariables.getHostServer()
                self.app.getFrame().GetStatusBar().SetStatusText("El servidor ha cambiado", 0)
            elif evento.GetId() == 2:
                loguin = GUIIniciarSesion.GUILogin(None)
                loguin.CenterOnScreen()
                if loguin.ShowModal() == wx.ID_OK: #Si dio en aceptar
                    self.adminvariables.setHostServer(loguin.getServidor())
                    self.conectar() #Nos conectamos de igual manera....
                loguin.Destroy()
            elif label_menu == "&Tareas programadas..." or label_menu== "_Tareas programadas...": #Otra forma de identificar al menú item
            #elif evento.GetId() == 3:
                print "Abrir dialogo"
                #x = ConfigTareasProgramadas(None, mediador=self, idCompuerta=1)
                x = GUIConfigCompuerta(None, 1, 1, mediador=self)
                x.ShowModal()
                x.Destroy()
                #print objeto.GetLabel(evento.GetId())
            elif label_menu == u"_Configuración de notificaciones..." or label_menu == u"&Configuración de notificaciones...":
                print "Configuracion de notificaciones"
                config = ConfigNotificaciones(None)
                config.ShowModal()
                config.Destroy()
            elif label_menu == u"Configuración peso alimento..." or label_menu == u"&Configuración peso alimento...":
                print "Abriendo config alimento..."
                config = GUIConfigParametrosAlimentos.ConfigParametroAlimento(None)
                config.ShowModal()
                config.Destroy()
            elif label_menu == u"Configuración fecha y hora" or label_menu == u"&Configuración fecha y hora":
                print "Abriendo config fecha y Hora..."
                config = GUIConfigurarFechaYHora(None, mediador=self)
                config.CenterOnParent()
                config.ShowModal()
                config.Destroy()
            elif label_menu == "&Acerca de..." or label_menu== "_Acerca de...": #Otra forma de identificar al menú item
                dialog_acerca_de = GUIAcercaDe.AcercaDe(None)
                dialog_acerca_de.CenterOnScreen()
                dialog_acerca_de.ShowModal()
                dialog_acerca_de.Destroy()
        if type(objeto) == FrameSensor: #Es el frame que ha lanzado el evento
            from src.widgets.widget import eventDict
            if eventDict[evento.GetEventType()] == "EVT_CLOSE":
                self.cerrarConexion()
                print "Conexiones cerradas y aplicación destruida."
        if type(objeto) == wx.Button and objeto.GetName() == "btnActualizarSensorAlimento":
            self.adminvariables.getNet().send("dist")
            x = ServiceBD(serverBD=self.adminvariables.getHostServerBD(), nombreBD=self.adminvariables.getNombreBD(), password="33426000", usuario="dcapeletti")
            x.agregarRegistro()
            #b = ConfiguracionCliente()
        if type(objeto) == wx.Button and objeto.GetName() == "btnActualizarTareasProgramadas":
            print "Recargado las tareas programadas..."
            self.adminvariables.getNet().send("<tareaprogramada>Todas</tareaprogramada>") #Envío el comando para recargar todas las tareas programadas...

        if type(objeto) == wx.Slider: #Para todos los controles slider
            if objeto.GetName() == "Control_Maestro": #Mueve todos los motores
                val = objeto.GetValue()
                self.adminvariables.getNet().send("pservo:"+ str(val))
            if objeto.GetName() == "Control_M1": #Mueve solamente el motor 1
                val = objeto.GetValue()
                self.adminvariables.getNet().send("<moverServo id=23>"+str(val)+"</moverServo>")
            if objeto.GetName() == "Control_M2": #Mueve solamente el motor 2
                val = objeto.GetValue()
                self.adminvariables.getNet().send("<moverServo id=18>"+str(val)+"</moverServo>")
            if objeto.GetName() == "Control_M3": #Mueve solamente el motor 3
                val = objeto.GetValue()
                self.adminvariables.getNet().send("<moverServo id=15>"+str(val)+"</moverServo>")
            if objeto.GetName() == "Control_M4": #Mueve solamente el motor 4
                val = objeto.GetValue()
                self.adminvariables.getNet().send("<moverServo id=14>"+str(val)+"</moverServo>")
        
        if type(objeto) == wx.Button and mensaje == "Actualizar_Fecha_Hora":
            self.adminvariables.getNet().send("actualizarHoraInternet")
        elif type(objeto) == wx.Button and mensaje == "Actualizacion_Ordenador":
            self.adminvariables.getNet().send("<actualizar_fecha_hora>"+str(time.mktime(datetime.now().timetuple()))+"</actualizar_fecha_hora>")
        elif type(objeto) == wx.Button and mensaje == "Actualizacion_Manual":
            fecha_hora_manual = control.getFechaHoraManual()
            #fecha_datetime_object = datetime.strptime(fecha_hora_manual, '%d/%m/%Y %H:%M:%S')
            self.adminvariables.getNet().send("<actualizar_fecha_hora>"+str(time.mktime(datetime.strptime(fecha_hora_manual, '%d/%m/%Y %H:%M:%S').timetuple()))+"</actualizar_fecha_hora>")              
            
                
    
    def actualizar(self, sujetoDato, eventoInteres):
        print eventoInteres
                
        
    
    def inicializarGUI(self):
        """Inicializa la GUI"""
        self.app = App(redirect=False)
        
        frame = FrameSensor(parent=None, id=-1, titulo=u'Central de comandos-COMEDOR ELECTRÓNICO', mediador=self)
        self.app.setFrame(frame)
        self.app.getFrame().Show()
        self.app.SetTopWindow(self.app.getFrame()) #Debe llamarse a este método
        self.rendezvous = rendezvous.Rendezvous(self.connected, self.chatDisplay, 
                                                self.lostConnection) 
        #self.panel_autenticacion = Autenticacion(self.app.getFrame(), self) #GUI de prueba, mejorarla!!!
        #self.panel_autenticacion.Show()        
        self.serviceReconect = ServiceReconect(self, 5, "192.168.1.104") #Intento una reconexión en 5 segundos...
        self.app.MainLoop()
            
            
    def conectar(self):        
        wx.BeginBusyCursor() #Cursor en estado procesando
        net = ChatConnect(self.adminvariables.getHostServer(), self.rendezvous.connected, 
                          self.rendezvous.display, self.rendezvous.lost)
        #net.addObserver(self)
        self.adminvariables.setNet(net)
        
        self.adminvariables.getNet().start()
        self.connected=True
        imagen = wx.StaticBitmap(self.app.getFrame().GetStatusBar())
        if os.name == "posix": 
            imagen.SetBitmap(wx.Bitmap('images/LedOn.png'))
        else:
            imagen.SetBitmap(wx.Bitmap('images\LedOn.png')) #para sistemas Windows.
        self.app.getFrame().GetStatusBar().AddWidget(imagen, pos=1)
        self.app.getFrame().GetStatusBar().SetStatusText("Conectado...", 0)
        self.panel_autenticacion = Autenticacion(self.app.getFrame(), self) #GUI de prueba, mejorarla!!!
        self.panel_autenticacion.Show()
        self.app.getFrame().GetMenuBar().agregarTodosLosMenus() #Agrego el menú de tareas luego del loguin        
        if self.serviceReconect.check_connection() and self.serviceReconect.isAlive()==False:
            self.serviceReconect.start()
        wx.EndBusyCursor()
    
           
    
    def lostConnection(self, msg):
        """"
        Hemos perdido nuestra conexión con el servidor
        Invocado via :func:`wx.CallAfter` en :mod:`rendezvous`.
        """
        #self.connectBtn.SetLabel('Conectar')
        #self._not_connected()
        #self.add_readWin('\n\n'+ msg)
        self.connected = False
        self.adminvariables.getNet().join()
        print msg
        self.app.getFrame().GetStatusBar().SetStatusText(msg, 0)
        imagen = wx.StaticBitmap(self.app.getFrame().GetStatusBar())
        if os.name == "posix": 
            imagen.SetBitmap(wx.Bitmap('images/LedOff.png'))
            self.app.getFrame().GetStatusBar().SetStatusText("Reconectando...", 0)
        else:
            imagen.SetBitmap(wx.Bitmap('images\LedOff.png')) #para sistemas Windows.
        self.app.getFrame().GetStatusBar().AddWidget(imagen, pos=1)
        if self.serviceReconect.is_server_connected() == True and self.connected == False:
            self.serviceReconect.reconectar()
    
    
    def pruebaLost(self):
        self.app.getFrame().GetStatusBar().SetStatusText("Se perdió la comunicación con el servidor...", 0)
        imagen = wx.StaticBitmap(self.app.getFrame().GetStatusBar())
        if os.name == "posix": 
            imagen.SetBitmap(wx.Bitmap('images/LedOff.png'))
        else:
            imagen.SetBitmap(wx.Bitmap('images\LedOff.png')) #para sistemas Windows.
        self.app.getFrame().GetStatusBar().AddWidget(imagen, pos=1)
        
    
        
    def lostConnectionServer(self):
        if self.serviceReconect.is_server_connected() == False:
            #self.getAdminVariable().getNet().join()
            self.connected = False
            #self.app.getFrame().GetStatusBar().SetStatusText("Se perdió la comunicación con el servidor...", 0)
            wx.CallAfter(self.pruebaLost)
        elif self.serviceReconect.is_server_connected() == True and self.connected == False:
            wx.CallAfter(self.conectar)
            self.connected = True
                    
        
    def chatDisplay(self, msg):
        try:
            if msg.find("\n") != -1: #Si encuentro un \n del mensaje del server
                self.comando = msg[msg.find("\n"):].strip()
                print msg
                if msg.find("<servidorBD>") != -1: #Path del servidor
                    server_bd = msg[msg.find("<servidorBD>")+12:msg.find("</servidorBD>")]
                    self.adminvariables.setHostServerBD(server_bd)
                if msg.find("<nombreBD>") != -1: #Nombre de la BD
                    base_dato = msg[msg.find("<nombreBD>")+10:msg.find("</nombreBD>")]
                    self.adminvariables.setNombreBD(base_dato)
                if msg.find("<sensorProx>") != -1:
                    cm =  msg[msg.find("<sensorProx>")+12:msg.find("</sensorProx>")].strip()
                    #self.calcularDistancia(float(cm))
                    self.panel_autenticacion.getPanelSensorAlimento().setPos(self.calcularDistancia(float(cm)))
                    wx.EndBusyCursor()
                    #print "ddasdsa ", cm
                if msg.find("<sensorTapa>") != -1: #Si encuentro que el botón se ha apretado...
                    estado_puerta = msg[msg.find("<sensorTapa>")+12:msg.find("</sensorTapa>")].strip()
                    #print estado_puerta
                    fecha = self.adminvariables.getConexionBd().getUltimaAperturaPuertaTolva() 
                    fecha_y_hora = fecha.strftime('%d/%m/%Y') + " a las \n " + fecha.strftime('%H:%M:%S')
                    self.panel_autenticacion.getPanelSensorPuerta().cambiarEstado(int(estado_puerta), fecha_y_hora) 
                if msg.find("<botonCompuerta comp=") != -1: #Si encuentro la palabra compuerta
                    estado_compuerta = msg[msg.find(">")+1:msg.find("</")].strip()
                    id_compuerta = msg[msg.find("=")+1:msg.find(">")].strip() #identificador de compuerta.
                    self.panel_autenticacion.getPanelMotor().cambiarEstadoControlPuerta(10*(1-int(estado_compuerta)), int(id_compuerta)) #Imprimo el valor en el slider...
                    self.panel_autenticacion.getPanelMotor().cambiarEstadoControlMaestro(3)#Lo pongo en estado bajo.
                    mensaje_estado = "Abierta." if int(estado_compuerta) == 0 else "Cerrada."
                    self.app.getFrame().GetStatusBar().SetStatusText("Evento en compuerta " + id_compuerta + ". Estado " + mensaje_estado, 0)
                if msg.find("pservo:") != -1: #Si encuentro la palabra pservo
                    estado_compuerta = msg[msg.find("o:")+2:msg.find("o:")+4].strip()
                    #self.panel_autenticacion.getPanelMotor().cambiarEstadoControlPuerta(int(estado_compuerta)) #Imprimo el valor en el slider...
                    self.panel_autenticacion.getPanelMotor().cambiarTodasLasCompuertas(int(estado_compuerta))
                if msg.find("<moverServo id=") != -1: #Si encuentro la palabra moverServo
                    pos_motor = msg[msg.find(">")+1:msg.find("</")].strip()
                    id_motor = msg[msg.find("=")+1:msg.find(">")].strip()
                    #self.panel_autenticacion.getPanelMotor().cambiarEstadoControlPuerta(int(estado_compuerta)) #Imprimo el valor en el slider...
                    #self.panel_autenticacion.getPanelMotor().cambiarTodasLasCompuertas(int(estado_compuerta))
                    self.actualizarSliderMotor(int(id_motor), int(pos_motor))
                if msg.find("Ejecutando tarea:") != -1:
                    id_tarea = int(msg.split(",")[0].split(":")[1])
                    id_compuerta = msg.split(",")[1]     
                    self.app.getFrame().GetStatusBar().SetStatusText("Ejecutando tarea " + str(id_tarea), 0)         
                #self.app.getFrame().GetStatusBar().SetStatusText(self.comando, 0) #Imprimo TODOS los mensajes del server en la barra de noticicaciones        
            #self.app.getFrame().GetStatusBar().SetStatusText(self.comando, 0)
        except ImportError as e:
            self.verificarError(e)
            
            
    def cerrarConexion(self):
        "Close the application by Destroying the object"
        if self.connected:
            self.adminvariables.getNet().send("/quit")
            self.adminvariables.getNet().join()
            self.serviceReconect.pause()
            self.serviceReconect.join()
            import sys
            sys.exit()
            
    
    def calcularDistancia(self, distancia):
        #Esta función hace un cálculo estimado del nivel de alimento que tenemos en la tolva...
        if int(distancia) > 91:
            return 5
        if int(distancia) in range(80, 90): #Tolva vacía
            return 5
        if int(distancia) in range(70, 80): #Casi vacía
            return 15
        if int(distancia) in range(50, 70): #Menos de la mitad
            return 30
        if int(distancia) in range(40, 50): #En la mitad
            return 50
        if int(distancia) in range(20, 40): #Mas de la mitad
            return 70
        if int(distancia) in range(10, 20): #Casi llena
            return 85
        if int(distancia) in range(1, 10): #Tolva llena
            return 95 #Tolva llena
        
    def actualizarSliderMotor(self, motor, posicion):
        #Se debe escribir un modelo para cargar o recargar la configuracion de los motores de la maquina...
        if motor == 23:
            self.panel_autenticacion.getPanelMotor().cambiarEstadoControlPuerta(int(posicion), 1)
        if motor == 18:
            self.panel_autenticacion.getPanelMotor().cambiarEstadoControlPuerta(int(posicion), 2)
        if motor == 15:
            self.panel_autenticacion.getPanelMotor().cambiarEstadoControlPuerta(int(posicion), 3)
        if motor == 14:
            self.panel_autenticacion.getPanelMotor().cambiarEstadoControlPuerta(int(posicion), 4)
    
    def verificarError(self, error):
        if error.message == "python-mysqldb":
            if os.name == "posix": #Si es un sistema GNU/Linux
                dlg = wx.PasswordEntryDialog(self.panel_autenticacion, "Necesito instalar el paquete python-mysqldb. Ingrese su contraseña...",
                    "Instalación de Software", style=wx.OK | wx.CANCEL)
                if dlg.ShowModal() == wx.ID_OK: #Si el usuario acepto
                    funciones_varias.instalarPaquete(error.message, dlg.GetValue())
                    dlg.Destroy()
            else: #Si es un sistema Winchot...
                print "Deberá instalar el paquete python-mysqldb..."                
        else:
            print "Ha ocurrido un error imprevisto. Mensaje: " + error.message
            
            
            
            
    def getAdminVariable(self):
        return self.adminvariables
            
            
    
    
            
            
            
