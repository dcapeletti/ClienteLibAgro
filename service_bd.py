# coding=utf-8
'''
Created on 17/11/2015

@author: diegoariel
# Copyright 2015 Diego Ariel Capeletti
# Este archivo ha sido desarrollado para el control digital del servo
# que comanda la compuerta del proyecto ....
#
# This program is licensed as Open Source Software using the Apache License,
# Version 2.0 (the "License"); you may not use this file except in compliance
# with the License. You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# You are free to use, copy, distribute, and display the work, and to make
# derivative works. If you do, you must give the original author credit. The
# author specifically permits (and encourages) teachers to post, reproduce,
# and distribute some or all of this material for use in their classes or by
# their students.
'''

#Para distros debian y derivados: sudo apt-get install python-mysqldb
import threading
import datetime
from macpath import curdir

class ServiceBD(threading.Thread):
    def __init__(self, serverBD, nombreBD, usuario, password):
        threading.Thread.__init__(self)
        self.daemon = True  # OK for main to exit even if instance is still running #permite salir aunque el main se este ejecutando...
        self.paused = True  # Comienzo en pausa
        self.state = threading.Condition() #Devuelve un objeto de condición variable. Permite que uno o mas hilos esperen hasta que sean notificados...
        self.tiempo_frecuencia=0.5 #Medio seg por defecto...
        try:
            import MySQLdb
            self.db = MySQLdb.connect(host=serverBD, user=usuario, passwd=password, db=nombreBD)
        except ImportError:
            raise ImportError("python-mysqldb") #Se necesita el paquete python-mysqldb.
    
    
    
    def run(self):
        self.resume()
        while True:
            with self.state:
                if self.paused: #Se evalua el estado del hilo, si esta pausado, lo pongo en espera...
                    self.state.wait() # block until notified #bloqeuar hasta que se notifica
            print "Hacer algo con la BD..."
            time.sleep(self.tiempo_frecuencia)



    def resume(self):
        #Pone en marcha la ejecución del thread
        with self.state:
            self.paused = False
            self.state.notify()  # unblock self if waiting #desbloquear auto esperando

    def pause(self):
        #Para la ejecución del thread
        with self.state:
            self.paused = True  # make self block and wait #El hilo deja de ejecutarse, entra en estado de espera..

    
    def agregarRegistro(self):
        cur = self.db.cursor() 
        #Consulta sql
        cur.execute("SELECT * FROM EVENTOS_PUERTA")
        
        #Imprimo todas las filas...
        for row in cur.fetchall() :
            print row[1], "   ", row[2] 
            
    
    def getResulSet(self, consulta_sql):
        #Retorna tupla con todas las filas...
        cur = self.db.cursor() 
        cur.execute(consulta_sql) #Consulta sql
        return cur.fetchall()
    
    def setFrecuenciaTrabajo(self, tiempo_frecuencia):
        #Cambia la frecuencia de trabajo del hilo...
        self.tiempo_frecuencia = tiempo_frecuencia
        
        
    def getConfigCompuertas(self):
        cur = self.db.cursor()
        cur.execute("Select * from COMPUERTAS")
        return cur.fetchall()
        
        
    def getTiempoUltimaApertura(self):
        #Busca los dos últimos registros de la BD y realiza el cálculo del tiempo abierto.
        #Si los ultimos registros es una tarea programada, debe lanzar algun error para avisar que debe hacerlo con botones...
        cur = self.db.cursor()
        cur.execute("SELECT * FROM comedor.EVENTOS_COMPUERTA order by id_EVENTOS_COMPUERTA DESC limit 2")
        
        resultado = cur.fetchall()
        apertura = resultado[1][2]
        cierre = resultado[0][2]
        t_cierre= datetime.datetime.strptime(cierre, '%Y-%m-%d %H:%M:%S.%f')
        t_apertura = datetime.datetime.strptime(apertura, '%Y-%m-%d %H:%M:%S.%f')
        diferencia_fecha = t_cierre - t_apertura
        return str(diferencia_fecha.seconds) + "." + str(diferencia_fecha.microseconds) #devuelvo la diferencia en segundos.microsegundos
    
    
    def getUltimaAperturaPuertaTolva(self):
        cur = self.db.cursor()
        cur.execute("SELECT Fecha FROM comedor.EVENTOS_PUERTA order by id_EVENTO_PUERTA desc limit 1")
        resultado = cur.fetchall()
        cur.close()
        return resultado[0][0]

    
    

#------------------------------------------------------------------------------------------------------


import sqlite3 as dbapi
import time

class ConfiguracionCliente:
    def __init__(self):
        '''Esta clase se encarga de leer, escribir y actualizar la configuración del cliente...'''
        self.base_dato = dbapi.connect("./Config.dat") #Me conecto a la bd de configuración sqlite
        
   
    def agregarServidor(self, ip, puerto):
        #Inserto un registro de servidor...
        cur = self.base_dato.cursor()
        cur.execute("Insert into servidor (Ip, Puerto) values (?, ?)", (ip[0][0], puerto))
        self.base_dato.commit()
        print "Registro de servidor guardado correctamente..."
        
    
    def imprimirServidores(self):
        cur = self.base_dato.cursor()
        cur.execute("Select * from servidor")
        lista = cur.fetchall()
        #for tupla in lista:
        #    print tupla
        return lista
            
    
    def cambiarFrecuenciaActualizacion(self, frecuencia):
        cur = self.base_dato.cursor()
        fecha = str(datetime.datetime.now())
        cur.execute("Insert into configvarias (FrecuenciaActualizacion, FechaCreacion, Activa) values (?, ?, ?)", (frecuencia, fecha, 1))
        self.base_dato.commit()
        cur.execute("update configvarias set Activa=0 where FechaCreacion <> '"+ fecha +"'") #Dejo la última fecha de creación como activa...
        self.base_dato.commit()
        print "Registro de frecuencia guardado correctamente..."
        
    
    def getFrecuenciaActualizacion(self):
        cur = self.base_dato.cursor()
        cur.execute("Select FrecuenciaActualizacion from configvarias Where Activa=1")
        return cur.fetchall()[0][0] #retorna la frecuencia de actualización activa, es la última que se agrego...
    
    
    def agregarUsuarioBD(self, nombre):
        cur = self.base_dato.cursor()
        cur.execute("Insert into UsuariosBD values ('"+ nombre +"')")
        self.base_dato.commit()
        
    def eliminarUsuarioBD(self, nombre):
        cur = self.base_dato.cursor()
        cur.execute("delete from usuariosbd where Nombre= '"+ nombre + "'")
        self.base_dato.commit()
        print "Registro de usuario eliminado..."
        
    def getUsuariosBD(self):
        cur = self.base_dato.cursor()
        cur.execute("Select Nombre from usuariosbd")
        print cur.fetchall()
        
    def getBD(self):
        return self.base_dato
    
    
    
    
    
    
    
    
    
    
    
