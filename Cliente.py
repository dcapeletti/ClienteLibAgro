#!/usr/bin/python
# coding=utf-8
'''
Created on 17/10/2015

@author: diegoariel capeletti
'''

# Copyright 2015 Diego Ariel Capeletti 
# This program was developed for education purposes for the Network
# Programming Class, CMST 355, at Kansas State University at Salina.
#
# This program is licensed as Open Source Software using the Apache License,
# Version 2.0 (the "License"); you may not use this file except in compliance
# with the License. You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# You are free to use, copy, distribute, and display the work, and to make
# derivative works. If you do, you must give the original author credit. The
# author specifically permits (and encourages) teachers to post, reproduce,
# and distribute some or all of this material for use in their classes or by
# their students.

#from widget import App
import os, subprocess

def main():
    try:
        from mediador import DirectorView
        med = DirectorView()
    except ImportError as error:
        if os.name == "posix": #Es un sistema GNU/Linux...
            print "Falta el paquete python-wxtools"
            if "wx" in error.message: #Falta la dependencia a la interfa gráfica...
                proc = subprocess.Popen("bash -c "" env | grep DESKTOP_SESSION", stdout=subprocess.PIPE, stderr=None, shell=True)
                salida = proc.communicate()
                if "gnome" in salida[0]:
                    proc = subprocess.Popen("gnome-terminal -x sh -c 'echo Necesito instalar el paquete python-wxtools y sus dependencias. Luego de terminar la instalación reinicie la aplicación... && sudo apt-get update && sudo apt-get -y install python-wxtools'", 
                                        shell=True, stdin=None, stdout=open("/dev/null", "w"), stderr=None, executable="/bin/bash")
                elif "kde" in salida[0]:
                    #proc = subprocess.Popen("konsole -e 'echo Necesito instalar el paquete python-wxtools y sus dependencias. Luego de terminar la instalación reinicie la aplicación... && sudo apt-get update && sudo apt-get -y install python-wxtools'", 
                    #                    shell=True, stdin=None, stdout=open("/dev/null", "w"), stderr=None, executable="/bin/bash")
                    pass

        else:
            print "Error. Debe instalar wxpython..."             

main()
