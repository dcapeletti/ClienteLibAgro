# coding=utf-8
'''
Created on 17/11/2015

@author: diegoariel
# Copyright 2015 Diego Ariel Capeletti
# Este archivo ha sido desarrollado para el control digital del servo
# que comanda la compuerta del proyecto ....
#
# This program is licensed as Open Source Software using the Apache License,
# Version 2.0 (the "License"); you may not use this file except in compliance
# with the License. You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# You are free to use, copy, distribute, and display the work, and to make
# derivative works. If you do, you must give the original author credit. The
# author specifically permits (and encourages) teachers to post, reproduce,
# and distribute some or all of this material for use in their classes or by
# their students.
'''
import os, subprocess


def verificarDependencias():
        #Verifica si las dependencias necesarias de la aplicación están instaladas...
        if os.name == 'posix':
            proc = subprocess.Popen('echo 33426000 | sudo -S apt-get -y install python-mysqldb', shell=True, stdin=None, stdout=open("/dev/null", "w"), stderr=None, executable="/bin/bash")
            proc.wait()

            proc = proc = subprocess.Popen('echo 33426000 | sudo -S apt-get -y install python-wxtools', shell=True, stdin=None, stdout=open("/dev/null", "w"), stderr=None, executable="/bin/bash")
            proc.wait()
            
            print "Todos los paquetes se han instalado con éxito. Reinicie la aplicacion.."
            os._exit(0)
        else:
            print "Si usas Windows, deberá instalarlos manualmente..."
            

def imprimirValorDialogo(frame):
    pass


def verificarPaqueteInstalado(paquete):
    #Verifica si un paquete esta instalado...
    process = subprocess.Popen("dpkg -l | grep "+ paquete, stdout=subprocess.PIPE, stderr=None, shell=True)

    #Lanzo el comando...
    output = process.communicate() #La salida es una tupla....
    
    if paquete in output[0]:
        return True
        #print "Esta isntalado..."
    else:
        return False
        #print "No esta instalado..."
        
def instalarPaquete(paquete, password):
    proc = subprocess.Popen('echo '+ password +' | sudo -S apt-get update', shell=True, stdin=None, stdout=open("/dev/null", "w"), stderr=None, executable="/bin/bash")
    #proc.wait() #Con wait detiene la GUI. Debería haber un thead???
    
    proc = subprocess.Popen('echo '+ password +' | sudo -S apt-get -y install ' + paquete, shell=True, stdin=None, stdout=open("/dev/null", "w"), stderr=None, executable="/bin/bash")
    #proc.wait()
    if verificarPaqueteInstalado(paquete) == True:
        print "Paquete instalado con éxito..."
    else:
        print "Hubo un problema al instalar el paquete..."            

    
    




