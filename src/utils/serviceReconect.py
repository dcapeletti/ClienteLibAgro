# coding=utf-8
'''
Created on 16 ene. 2018

@author: diegoariel
'''

'''Esta clase proporciona el servicio de verificación de conexión al servidor.
Cuando la placa de red de la máquina cliente se apaga, se desconecta el router  
wifi, la conexión eléctrica del servidor se pierde o cualquier otro evento 
que impida verificar una conexión al servidor, esta clase emite una alerta 
al usuario de tal evento. Se podría crear un frame para alertar de tal situación
e instalarlo en la pantalla principal. Una vez que se conecta, habilitar de nuevo
los controles.
'''


import wx
import threading
import socket
import time

class ServiceReconect(threading.Thread):
    def __init__(self, mediador, segundos=5, check_address=None):
        threading.Thread.__init__(self)
        self.daemon = True  # OK for main to exit even if instance is still running #permite salir aunque el main se este ejecutando...
        self.paused = True  # Comienzo en pausa
        self.mediador = mediador
        self.segundos = segundos
        self.check_server_address = check_address
        self.is_connected=True
        self.estado_anterior=False
        self.state = threading.Condition() #Devuelve un objeto de condición variable. Permite que uno o mas hilos esperen hasta que sean notificados...
    
    
    def run(self):
        self.resume()
        while True:
            with self.state:
                if self.paused: #Se evalua el estado del hilo, si esta pausado, lo pongo en espera...
                    self.state.wait() # block until notified #bloqeuar hasta que se notifica
            try:
                socket.create_connection((self.check_server_address, 22))
                self.is_connected = True
                print "Conectado"
            except:
                self.is_connected = False
                print "No conectado"
            if self.estado_anterior != self.is_connected: #solo reporto cuando cambia la conexion
                self.estado_anterior = self.is_connected
                print "Enviando notificacion"
                self.mediador.lostConnectionServer()    
            time.sleep(5)
            
        
    def resume(self):
        #Pone en marcha la ejecución del thread
        with self.state:
            self.paused = False
            self.state.notify()  # unblock self if waiting #desbloquear auto esperando

    def pause(self):
        #Para la ejecución del thread
        with self.state:
            self.paused = True  # make self block and wait #El hilo deja de ejecutarse, entra en estado de espera..
            self.state.notify()
            
    
    def reconectar(self):
        wx.CallLater(self.segundos * 1000, self.mediador.conectar)
        
        
    def check_connection(self):
        try:
            socket.create_connection((self.check_server_address, 22))
            self.is_connected = True
            return True
        except:
            self.is_connected = False
            return False
        
    
    def is_server_connected(self):
        return self.is_connected
    
    