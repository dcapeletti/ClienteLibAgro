'''
Created on 09/11/2016

@author: diegoariel
'''

class Compuerta():
    #Clase modelo compuerta...
    def __init__(self):
        self.__id_compuerta=0
        self.__pin_senal_motor=0

    def get_id_compuerta(self):
        return self.__id_compuerta


    def set_id_compuerta(self, value):
        self.__id_compuerta = value
        
    def set_pin_motor(self, valor):
        self.__pin_senal_motor=valor
        
    def get_pin_motor(self):
        return self.__pin_senal_motor

    id_compuerta = property(get_id_compuerta, set_id_compuerta, None, None)
    pin_senal_motor = property(get_pin_motor, set_pin_motor, None, None)
        
        
    