'''
Created on 09/11/2016

@author: diegoariel
'''


class TareaProgramada():
    def __init__(self):
        self.__id_tarea=0
        self.__id_compuerta=0
        self.__dia=0
        self.__hora=0
        self.__config_kg=0

    def get_id_tarea(self):
        return self.__id_tarea


    def get_id_compuerta(self):
        return self.__id_compuerta


    def get_dia(self):
        return self.__dia


    def get_hora(self):
        return self.__hora


    def get_config_kg(self):
        return self.__config_kg


    def set_id_tarea(self, value):
        self.__id_tarea = value


    def set_id_compuerta(self, value):
        self.__id_compuerta = value


    def set_dia(self, value):
        self.__dia = value


    def set_hora(self, value):
        self.__hora = value


    def set_config_kg(self, value):
        self.__config_kg = value

    id_tarea = property(get_id_tarea, set_id_tarea, None, None)
    id_compuerta = property(get_id_compuerta, set_id_compuerta, None, None)
    dia = property(get_dia, set_dia, None, None)
    hora = property(get_hora, set_hora, None, None)
    kg = property(get_config_kg, set_config_kg, None, None)
        
        
    