# coding=utf-8
'''
Created on 07/09/2016

@author: diegoariel, Thomas M Wetherbee

Crear los paneles y poner en cada objeto como padre.
Crear lo sizer.
Agregar los objetos a los sizer.
Establecer los sizer a los paneles creados en el punto 1.

El sizer padre de todos que se establece al Frame, debe contener a los paneles.

Esta clase permite configurar dias y horarios para ejecucion de tareas de las compuertas.
Se permite crear tareas para dias posteriores a la fecha actual, pero no para dias anteriores ya
que no pueden ser ejecutadas pasada una fecha.

'''
import wx   
import wx.calendar
import wx.grid
from src.widgets.CheckListComboBox import CheckListBoxComboPopupControl
import MySQLdb
import datetime
import time
from src.widgets.widget import Widget
from src.model.TareaProgramada import TareaProgramada
from src.model.Compuerta import Compuerta

class ConfigKg():
    #Clase para almacenar la configuración de alimentos
    def __init__(self, id, alimento, kg): 
        self.idConfig = id
        self.alimento = alimento
        self.kg = kg

class GridTareas(wx.grid.Grid):
    def __init__(self, parent, resultado):
        wx.grid.Grid.__init__(self, parent, -1, size=(360, 145))
        #self.log = log
        self.down = False
        columnas = ["Id", "Dia", "Hora", "Config_Kg"]
        self.fila_seleccionada = 0
        self.lista_configKg=[]
        #self.idConfigKg=0
        self.CreateGrid(len(resultado), len(columnas))
        
        for col in columnas:
            self.SetColLabelValue(columnas.index(col), col) #indice / Columna
            
        self.SetColSize(0,30)
        
        self.Bind(wx.grid.EVT_GRID_CELL_LEFT_DCLICK, self.OnLeftDClick)
        self.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        self.Bind(wx.grid.EVT_GRID_SELECT_CELL, self.informarFila)
        self.Bind(wx.grid.EVT_GRID_EDITOR_CREATED, self.cargarKilogramosConfigurados)
        self.Bind(wx.grid.EVT_GRID_CELL_CHANGE, self.CellContentsChanged)
        
        self.cargarDatos(resultado)    
            
        
    def cargarKilogramosConfigurados(self, event):
        #Lee toda la lista de configuración de kilos y lo agrega al combo.
         #for x in resultado:
        #    self.listaKg.append(x[0])
        #self.listaKg=map(str, self.listaKg)
        #print self.listaKg
        
        Row = event.GetRow()
        Col = event.GetCol()
        
        print 'OnGrid1EditorCreated: (%u, %u)\n' % (Row, Col)
        
        #In this example, all cells in row 0 are GridCellChoiceEditors,
        #so we need to setup the selection list and bindings. We can't
        #do this in advance, because the ComboBox control is created with
        #the editor.
        if Col == 3:
            #Get a reference to the underlying ComboBox control.
            self.comboBox = event.GetControl()
            #Bind the ComboBox events.
            self.comboBox.Bind(wx.EVT_COMBOBOX, self.OnComboBoxChange)
            #self.comboBox.Bind(wx.EVT_TEXT, self.OnGrid1ComboBoxText)
            if self.comboBox.GetCount() <= 0:
                #Load the initial choice list.
                if len(self.lista_configKg) <=0:    
                    self.Parent.Parent.cursor.execute("SELECT idCONFIG_PARAMETROS_KILOS, ALIMENTOS.Nombre, Kg FROM comedor.CONFIG_PARAMETROS_KILOS_ALIMENTOS inner join ALIMENTOS on CONFIG_PARAMETROS_KILOS_ALIMENTOS.Tipo_alimento = ALIMENTOS.id_ALIMENTO")
                    resultado = self.Parent.Parent.cursor.fetchall()
                    for obj in resultado:
                        configKg = ConfigKg(obj[0], obj[1], obj[2])
                        self.lista_configKg.append(configKg) #cargo la lista por única vez
                        self.comboBox.Append(str(configKg.kg) + ' kg de ' +configKg.alimento, configKg.idConfig)
                else:
                    for obj in self.lista_configKg:
                        self.comboBox.Append(str(obj.kg) + ' kg de ' + obj.alimento, obj.idConfig)
        
        event.Skip()
        
    def cargarDatos(self, resultado):
        self.borrarGrilla()
        self.AppendRows(len(resultado)) #Agrego las filas
        for i in range(0,len(resultado)):
            self.SetCellValue(i, 0, "%s"%resultado[i][0])
            self.SetCellValue(i, 1, "%s"%resultado[i][2])
            self.SetCellValue(i, 2, "%s"%resultado[i][3])
            self.SetCellValue(i, 3, "%s"%resultado[i][4])
            editor=wx.grid.GridCellChoiceEditor([], allowOthers=False) #paso vacía y no permito agregar items a la lista
            self.SetCellEditor(i, 3, editor)
            self.SetReadOnly(i, 0, True) #Col Id no editable
            self.SetReadOnly(i, 1, True) #Col Id no editable
            
    
    def OnComboBoxChange(self, event):
        self.index = self.comboBox.GetSelection()
        self.data = self.comboBox.GetClientData(self.index)
        
        #print 'ComboBoxChanged: %s' % self.comboBox.GetValue()
        #print 'ComboBox index: %u' % self.grilla.index 
        print 'Id config seleccionado: %u\n' % self.data
        self.idConfigKg = self.data
        event.Skip()
        
    
    def CellContentsChanged(self, event):
        col = event.GetCol()
        row = event.GetRow()
        val = self.GetCellValue(row, col) #obtiene el valor de la celda que ha cambiado...
        if val == "":
            val = "null"
        if col == 2:
            val= "'"+self.GetCellValue(row, col)+"'"
        if col ==3:#Col seleccion alimento...
            #sub_val = val[4:]
            val = str(self.idConfigKg)
            self.SetCellValue(row, col, str(self.idConfigKg))
        ColLabel = self.GetColLabelValue(col)    
        sql = "UPDATE CONFIG_APERTURAS_COMPUERTAS SET "+ColLabel+"="+val+" WHERE idCONFIG_APERTURAS_COMPUERTAS ="+ self.GetCellValue(row, 0) # protects against injection of dangerous 'val' 
        #self.cursor.execute("UPDATE CONFIG_APERTURAS_COMPUERTAS SET "+ColLabel+"=? WHERE idCONFIG_APERTURAS_COMPUERTAS =?", (val, self.grilla.GetCellValue(row, 0))) # protects against injection of dangerous 'val'
        self.Parent.Parent.cursor.execute(sql)
        self.Parent.Parent.db.commit()
        
        
    def informarFila(self, event):
        self.fila_seleccionada = event.GetRow()
        self.currentlySelectedCell = (event.GetRow(),event.GetCol())
        event.Skip()
    
    def OnLeftDClick(self, evt):
        if self.CanEnableCellControl():
            self.EnableCellEditControl()
            
    def OnKeyDown(self, event):
        if event.GetKeyCode() == 127: #Tecla Supr
            self.Parent.Parent.db.cursor().execute("Delete from CONFIG_APERTURAS_COMPUERTAS Where idCONFIG_APERTURAS_COMPUERTAS="+ str(self.GetCellValue(self.fila_seleccionada, 0)))
            self.Parent.Parent.db.commit()
            self.DeleteRows(self.fila_seleccionada)
            event.Skip()
            return
        
        if event.GetKeyCode() != wx.WXK_RETURN: #Tecla enter
            event.Skip()
            return
        
        if event.ControlDown():   # the edit control needs this key
            event.Skip()
            return
        

        self.DisableCellEditControl()
        success = self.MoveCursorRight(event.ShiftDown())

        if not success:
            newRow = self.GetGridCursorRow() + 1

            if newRow < self.GetTable().GetNumberRows():
                self.SetGridCursor(newRow, 0)
                self.MakeCellVisible(newRow, 0)
            else:
                # this would be a good place to add a new row if your app
                # needs to do that
                pass
            
    
    def borrarGrilla(self):
        rows = self.GetNumberRows()
        #self.grilla.DeleteCols(0,cols,True); #Elimino las columnas
        if rows !=0: #Si hay filas en la grilla, las elimino...
            self.DeleteRows(0,rows,True); #Elimino todas las filas        
        
            

class ConfigTareasProgramadas(wx.Dialog, Widget):
    def __init__(self, parent, mediador, idCompuerta):
        #style = wx.DEFAULT_FRAME_STYLE & (~wx.MAXIMIZE_BOX) & (~wx.RESIZE_BORDER)
        #wx.Frame.__init__(self, None, wx.ID_ANY, 'Configuracion de compuertas', style=style)
        wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u'Configuración de compuertas', pos = wx.DefaultPosition, size = wx.Size( 650, 271 ), style = wx.DEFAULT_DIALOG_STYLE )
        self.mediador=mediador #mediador al que notifica los cambios...
        self.db= MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')
        self.cursor = self.db.cursor()
        self.font_titulos = wx.Font(15, wx.FONTFAMILY_DEFAULT, wx.NORMAL, wx.NORMAL)
        self.listaKg=[]
        self.lista_tareas=[]
        self.idConfigKg=0
        self.compuerta = Compuerta()
        self.compuerta.set_id_compuerta(idCompuerta) #ID DE LA COMPUERTA PRECONFIGURADO...
        self.crearGUI()
        
    
    def crearGUI(self):
        self.cursor.execute("Select * from CONFIG_APERTURAS_COMPUERTAS Where Dia='" + datetime.datetime.now().strftime('%Y-%m-%d')+"' and Ejecutado=0 and Id_Compuerta=" + str(self.compuerta.get_id_compuerta()))
        resultado = self.cursor.fetchall()
        
        panelTitulo = wx.Panel(self, wx.ID_ANY)
        label_titulo = wx.StaticText(panelTitulo, -1, u'Configuración de la compuerta N'+ str(self.compuerta.get_id_compuerta()))
        label_titulo.SetFont(wx.Font(20, wx.FONTFAMILY_DEFAULT, wx.NORMAL, wx.NORMAL))
        sizerTitulo = wx.BoxSizer(wx.VERTICAL)
        sizerTitulo.Add(label_titulo, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        panelTitulo.SetSizer(sizerTitulo)
        
        panelCalendario = wx.Panel(self, wx.ID_ANY) 
        label_dias_operacion = wx.StaticText(panelCalendario, -1, u'Días de operación')
        label_dias_operacion.SetFont(self.font_titulos)
        self.calendario = wx.calendar.CalendarCtrl(panelCalendario, -1)
        self.Bind(wx.calendar.EVT_CALENDAR_SEL_CHANGED, self.OnCalSelChanged, self.calendario)
        self.calendario.Bind(wx.calendar.EVT_CALENDAR , self.agregarRegistro)
        sizer_calendario = wx.BoxSizer(wx.VERTICAL)
        sizer_calendario.Add(label_dias_operacion)
        sizer_calendario.Add(self.calendario)
        panelCalendario.SetSizer(sizer_calendario)
        
        panelApertura = wx.Panel(self, wx.ID_ANY)
        #panelApertura.SetBackgroundColour('red')
        label_apertura = wx.StaticText(panelApertura, -1, 'Puntos de apertura')
        label_apertura.SetFont(self.font_titulos)
        #self.grilla = wx.grid.Grid(panelApertura, -1, size=(360, 145))
        self.grilla = GridTareas(panelApertura, resultado)

        #chkCopiarConfiguracion = wx.CheckBox(panelApertura, -1, 'Copiar configuracion al resto de compuertas.')
        lblCopiarConfiguracion = wx.StaticText(panelApertura, -1, u'Copiar configuración a las siguientes compuertas:')
        self.listaCompuertas = CheckListBoxComboPopupControl(panelApertura)
        self.listaCompuertas.addItems(["Todas", "Comp1", "Comp2", "Comp3", "Comp4"], False)
        sizerListaComp = wx.BoxSizer(wx.HORIZONTAL)
        sizerListaComp.Add(lblCopiarConfiguracion)
        sizerListaComp.Add(self.listaCompuertas)
        
        sizerApertura = wx.BoxSizer(wx.VERTICAL)
        sizerApertura.Add(label_apertura, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        sizerApertura.Add(self.grilla)
        sizerApertura.Add(sizerListaComp, 0, wx.ALL, 5)
        panelApertura.SetSizer(sizerApertura)
        
        panelBotones = wx.Panel(self, wx.ID_ANY)
        btnAceptar = wx.Button(panelBotones, wx.ID_OK)
        btnAceptar.SetName("btnActualizarTareasProgramadas")
        btnAceptar.Bind(wx.EVT_BUTTON, self.guardarYRecargarTareas)
        btnCancelar = wx.Button(panelBotones, wx.ID_CANCEL)
        sizerBotones= wx.BoxSizer(wx.HORIZONTAL)
        sizerBotones.Add(btnAceptar, 1, wx.ALIGN_RIGHT, 0)
        sizerBotones.Add(btnCancelar, 1,wx.wx.ALIGN_RIGHT, 0)
        panelBotones.SetSizer(sizerBotones)
        
        
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        
        sizer_paneles = wx.BoxSizer(wx.HORIZONTAL)
        sizer_paneles.Add(panelCalendario, 1, wx.EXPAND | wx.ALL, 5) #Ledoy prioridad de crecmiento
        sizer_paneles.Add(panelApertura, 0, wx.EXPAND | wx.ALL, 5)
        
        mainSizer.Add(panelTitulo, 1, wx.EXPAND, 5)
        mainSizer.Add(sizer_paneles, 0, wx.EXPAND | wx.ALL, 5)
        mainSizer.Add(panelBotones, 0, wx.ALIGN_RIGHT, 0)
             
        self.SetSizer(mainSizer)
        mainSizer.Fit(self)
        self.Layout()
        self.Center()
        
        
    def guardarYRecargarTareas(self, event):
        #Si el usuario ha marcado para copiar la configuración a otra compuerta, se procesa este método.
        #<tareaprogramada>Todas</tareaprogramada> #para todas las compuertas.
        wx.BeginBusyCursor()
        seleccionados= self.listaCompuertas.GetValue() #Obtengo la lista de compuertas seleccionadas
        self.getTodasLasConfiguraciones()
        for x in range(0, len(seleccionados)): #verifico si hay compuertas seleccionadas
            compuertas_seleccionadas= self.listaCompuertas.getTextItem()[seleccionados[x]]
            if compuertas_seleccionadas == "Todas": #aplicar a todas
                for x in range(1, len(self.listaCompuertas.getTextItem())): #iniciando en 1, evito Todas...
                    comp = self.listaCompuertas.getTextItem()[x]
                    comp= int(comp[-1:])
                    if comp != self.compuerta.get_id_compuerta(): #Si es el mismo id, no se cargan...     
                        for tarea in self.lista_tareas:
                            self.cursor.execute("Insert into CONFIG_APERTURAS_COMPUERTAS (Id_Compuerta, Dia, Hora, Config_Kg) values("+ str(comp) +",'" + tarea.get_dia() +"', '"+ tarea.get_hora() +"', "+ tarea.get_config_kg() +")")
                            self.db.commit()
                break #salgo inmediatamente
            else: #por cada una, excepto la compuerta configurada para la GUI
                if self.compuerta.get_id_compuerta() != int(compuertas_seleccionadas[-1:]): #solo cargo las tareas si es distinta a la compuerta q estoy configurando...
                    comp_sel= int(compuertas_seleccionadas[-1:])
                    for tarea in self.lista_tareas: #Recorro todas las tareas
                        self.cursor.execute("Insert into CONFIG_APERTURAS_COMPUERTAS (Id_Compuerta, Dia, Hora, Config_Kg) values("+ str(comp_sel) +",'" + tarea.get_dia() +"', '"+ tarea.get_hora() +"', "+ tarea.get_config_kg() +")")
                        self.db.commit()
            
        self.notificarCambios(event)
        wx.EndBusyCursor()
        self.Destroy()
        
    
    def getTodasLasConfiguraciones(self):
        #Rellenar objetos clases de configuraciones..
        for i in range(0, self.grilla.GetNumberRows()):
            print self.grilla.GetCellValue(i, 0)
            tarea = TareaProgramada()
            tarea.set_id_tarea(self.grilla.GetCellValue(i, 0))
            tarea.set_dia(self.grilla.GetCellValue(i, 1))
            tarea.set_hora(self.grilla.GetCellValue(i, 2))
            tarea.set_config_kg(self.grilla.GetCellValue(i, 3))
            print tarea.get_id_tarea()
            self.lista_tareas.append(tarea)
                
        
    
    def notificarCambios(self, evento):
        self.mediador.widgetModificado(self, evento)
    
        
    def cargarTareasPorFecha(self, fecha):
        #Carga las tareas del día, pendiente de ejecución... 
        self.cursor.execute("Select * from CONFIG_APERTURAS_COMPUERTAS Where Dia='" + fecha +"' and Ejecutado=0 and Id_Compuerta="+str(self.compuerta.get_id_compuerta()))
        resultado = self.cursor.fetchall()
        #self.grilla.DeleteCols(0,cols,True); #Elimino las columnas
        self.grilla.cargarDatos(resultado)
                
            
    def OnCalSelChanged(self, event):
        #Si se selecciona un dia pasado, que vuelva al mismo dia...Si es superior no pasa nada
        fecha_seleccionada=event.GetDate().Format("%Y-%m-%d")
        #print fecha_seleccionada.Format("%Y-%m-%d")
        fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")
        #print fecha_actual
        if time.strptime(fecha_seleccionada, "%Y-%m-%d") < time.strptime(fecha_actual, "%Y-%m-%d"):
            date = wx.DateTime_Now()
            self.calendario.SetDate(date)
            dlg = wx.MessageDialog(self,'No puede seleccionar una fecha anterior.', u'Error de selección', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
        else: #Busco si hay fechas por cargar...
            self.cargarTareasPorFecha(fecha_seleccionada)
            
            
    def agregarRegistro(self, event):
        #Este método agrega un registro al final de la grilla
        print "dasd"
        fecha_seleccionada=event.GetEventObject().GetDate().Format("%Y-%m-%d")
        #print fecha_seleccionada.Format("%Y-%m-%d")
        fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")
        hora_actual = datetime.datetime.now()
        aumento_segundos = 60
        aumento_minutos = 0
        hora_con_segundos = datetime.timedelta(minutes=aumento_minutos) + datetime.timedelta(seconds=aumento_segundos) + datetime.timedelta(hours=hora_actual.hour, minutes=hora_actual.minute, seconds=hora_actual.second)
        #print fecha_actual
        if time.strptime(fecha_seleccionada, "%Y-%m-%d") >= time.strptime(fecha_actual, "%Y-%m-%d"):
            fecha= event.GetEventObject().GetDate().Format("%Y/%m/%d")
            self.cursor.execute("Insert into CONFIG_APERTURAS_COMPUERTAS (Id_Compuerta, Dia, Hora, Config_Kg) values("+ str(self.compuerta.get_id_compuerta()) +",'" + fecha_seleccionada +"', '"+ str(hora_con_segundos) +"', 0)")
            ultimo_id=self.db.insert_id()
            self.db.commit()
            self.grilla.AppendRows(1)
            pos = (self.grilla.GetNumberRows()-1)
            self.grilla.SetReadOnly(pos, 0, True) #Col Id no editable
            self.grilla.SetReadOnly(pos, 1, True) #Col Id no editable
            self.grilla.SetCellValue(pos, 0, "%s"%ultimo_id) #Col Id no editable
            self.grilla.SetCellValue(pos, 1, "%s"%fecha)
            self.grilla.SetCellValue(pos, 2, "%s"%hora_con_segundos) #hora por defecto, un minuto mas...
            editor=wx.grid.GridCellChoiceEditor(self.listaKg, False)
            #self.Bind(wx.grid.EVT_GRID_EDITOR_CREATED, self.cargarKilogramosConfigurados)
            self.grilla.SetCellEditor(pos, 3, editor)
            self.grilla.SetCellValue(pos, 3, "0") #Kg por defecto
  
        

'''
app = wx.App()
f = ConfigTareasProgramadas()
f.Show()
app.MainLoop()
'''