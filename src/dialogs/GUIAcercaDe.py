'''
Created on 21/09/2016

@author: diegoariel
'''
import wx
import wx.richtext as RT
import os

class AcercaDe(wx.Dialog):
    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, wx.ID_ANY, 'Acerca de LibAgro')
        self.configurarGUI()
    
    def configurarGUI(self):
        panel = wx.Panel(self, wx.ID_ANY)
        label_acercade = wx.StaticText(panel, -1, "Cliente LibAgro Desktop V1.0")
        label_acercade.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD, False)) #Fuente negrita multiplataforma
        siser_acerca_de = wx.BoxSizer(wx.VERTICAL)
        
        #urlStyle = RT.TextAttrEx()
        #urlStyle.SetTextColour(wx.BLUE)
        #urlStyle.SetFontUnderlined(True)
        
        str_descripcion = "LibAgro, una empresa dedicada a brindar soluciones con "
        
        lblDescripcion = RT.RichTextCtrl(panel, style=wx.VSCROLL|wx.HSCROLL|wx.NO_BORDER)
        lblDescripcion.SetMinSize((300,200))
        lblDescripcion.SetEditable(False)
        lblDescripcion.WriteText("Copyright LibAgro 2016.\nEste programa es ")
        
        lblDescripcion.BeginTextColour((0,0,255))
        lblDescripcion.BeginURL("https://www.gnu.org/philosophy/free-sw.es.html")
        lblDescripcion.WriteText("Software libre")
        lblDescripcion.EndURL()
        lblDescripcion.EndTextColour()
        lblDescripcion.WriteText(" y se distribuye bajo la licencia ")
        
        #lblDescripcion.BeginStyle(urlStyle)
        lblDescripcion.BeginTextColour((0, 0, 255))
        lblDescripcion.BeginURL("http://www.apache.org/licenses/LICENSE-2.0")
        lblDescripcion.WriteText("Apache 2.0")
        lblDescripcion.EndURL()
        #lblDescripcion.EndStyle()
        lblDescripcion.EndTextColour()
        lblDescripcion.Newline()
        lblDescripcion.Newline()
        lblDescripcion.WriteText(str_descripcion)
        lblDescripcion.Newline()
        
        
        #lblDescripcion.BeginStyle(urlStyle)
        lblDescripcion.BeginTextColour((0, 0, 255))
        lblDescripcion.BeginURL("https://es.wikipedia.org/wiki/Tecnolog%C3%ADa_libre")
        lblDescripcion.WriteText("Tecnologias Libres")
        lblDescripcion.EndURL();
        #lblDescripcion.EndStyle();
        lblDescripcion.EndTextColour()
        lblDescripcion.WriteText(" para el sector agreopecuario.")
        lblDescripcion.Bind(wx.EVT_TEXT_URL, self.OnURL)
        lblDescripcion.Newline()        
        if os.name == "posix":
            lblDescripcion.WriteImageFile('./images/LibAgro.png', wx.BITMAP_TYPE_PNG)
        else:
            lblDescripcion.WriteImageFile('.\images/LibAgro.png', wx.BITMAP_TYPE_PNG)
        siser_acerca_de.Add(label_acercade, 0, wx.ALIGN_CENTRE_HORIZONTAL | wx.ALL, 5)
        siser_acerca_de.Add(lblDescripcion, 0, wx.ALIGN_CENTRE_HORIZONTAL | wx.ALL, 5)
        siser_acerca_de.Add(wx.Button(panel, wx.ID_OK), 0, wx.ALIGN_RIGHT | wx.ALL, 5)
        
        panel.SetSizer(siser_acerca_de) 
        siser_acerca_de.Fit(self) #Ajusto al tamanio minimo
        self.Layout() 
        
    def OnURL(self, event):    
        import webbrowser
        webbrowser.open(event.GetString())
        
    
        
'''
app = wx.App()
ventana = AcercaDe()
ventana.CenterOnScreen()
ventana.ShowModal()
#ventana.Show()
ventana.Destroy()
app.MainLoop()
'''