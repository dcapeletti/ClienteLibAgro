# encoding=utf-8
'''
Created on 24 ene. 2018

@author: diegoariel
'''

from wx.grid import *
import wx.grid
#import wx.gridExcepciones.GRIDTABLE_NOTIFY_ROWS_APPENDED
import wx.calendar
import sys
from apt_pkg import config
sys.path.insert(0, sys.path[0]+'/../../')
from src.widgets.widget import DatePickerCtrlV2, TimerCtrl, ListBoxListaAlimentos
import locale
import datetime, time
import MySQLdb

class GUIRepeticionTareaSimple(wx.Dialog):
    def __init__(self, parent, compuerta, ModoEdit=False, tarea=None, title="Nueva tarea programada"):
        wx.Dialog.__init__(self, parent, title=title)
        #locale.setlocale(locale.LC_ALL, '') #http://zetcode.com/wxpython/in18/
        locale.setlocale(locale.LC_ALL, ('es_AR', 'UTF8')) #http://zetcode.com/wxpython/in18/
        self.modo_edit = ModoEdit
        self.tarea_programada = tarea
        self.__bd = MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')
        self.panelEntradaDatos = GUINuevaDosificacionAutomatica(self)
        #self.panelRepeticion = PanelRepeticionTareaSimple(self, None)
        self.panelEntradaDatos.txtCompuerta.SetValue(str(compuerta))
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.panelEntradaDatos)
        #self.sizer.Add(self.panelRepeticion)
        tabPane = wx.Notebook(self, -1)
        #tabPane.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.redibujar)
        self.panelRepeticionTareaSimple = PanelRepeticionTareaSimple(tabPane, None)
        self.panelExcepciones = GUIExcepciones(tabPane, ModoEdit)
        self.panelModificacionesInstancias = GUIModificacionesInstancias(tabPane, ModoEdit)
        tabPane.AddPage(self.panelRepeticionTareaSimple, "Parametros de repeticion")
        if self.modo_edit == True:
            tabPane.AddPage(self.panelExcepciones, "Excepciones")
            tabPane.AddPage(self.panelModificacionesInstancias, "Modificaciones instancias")
            
        self.sizer.Add(tabPane, 0, wx.EXPAND|wx.ALL)
        if self.modo_edit == True:
            self.panelLineaTiempo = PanelLineaTiempo(self)
            self.sizer.Add(self.panelLineaTiempo, 0, wx.EXPAND|wx.ALL)

        btnAceptar = wx.Button(self, wx.ID_OK, "Aceptar")
        btnAceptar.Bind(wx.EVT_BUTTON, self.procesarAceptar)
        btnCancelar = wx.Button(self, wx.ID_CANCEL, "Cancelar")
        sizerBotonera = wx.BoxSizer(wx.HORIZONTAL)
        sizerBotonera.Add(btnCancelar)
        sizerBotonera.Add(btnAceptar)
        
        self.statusBar = wx.StatusBar(self, -1)
        self.statusBar.SetBackgroundColour(wx.YELLOW)
        self.statusBar.SetFont(wx.Font(9, wx.DEFAULT, wx.NORMAL, wx.FONTWEIGHT_BOLD , False))
        self.statusBar.Hide()
        self.sizer.Add(self.statusBar, 0, wx.EXPAND|wx.ALL)
        self.sizer.Add(sizerBotonera, 0, wx.ALIGN_RIGHT)
        
        self.SetSizerAndFit(self.sizer)

    def getBd(self):
        return self.__bd
    
    def GetStatusBar(self):
        return self.statusBar
    
    def procesarAceptar(self, event):
        if self.modo_edit == True: #Si estamos en modo edit, tomamos los valores y actualizamos la BD...
            '''Si una tarea esta configurada "Para siempre" puede tener modificaciones o excepciones 
            HASTA la fecha que sea. ¿qué debería suceder si se cambia la fecha Hasta de una tarea que 
            es menor a la fecha de excepciones o modificaciones que posee? ¿Qué debería pasar con ellas?
            
            Si se cambia la fecha Hasta a una menor que las excepciones o modificaciones de instancias
            que ya tiene configuradas, se las debe ignorar ya que el programa servidor NO LAS TIENE EN CUENTA. 
            El programa servidor solo ejecutará las tareas dónde la fecha actual este dentro del rango de las 
            mismas. Una vez que se cumplio el periodo, las excepciones o modificaciones futuras que esta tenga, no se 
            tendrán en cuenta, excepto que la tarea vuelva a cambiar a una fecha posterior a la fecha actual, 
            sea habilitada o marcada "Para siempre".
            '''
            try:
                bd = MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')
                #bd = self.getBd()
                cur = bd.cursor()
                config_kg = self.panelEntradaDatos.lstListaAlimentos.GetIdSeleccionado()
                fechaIni = self.panelRepeticionTareaSimple.panelDesdeHasta.fechaDesde.GetValue().split("/")
                fechaIni = str(fechaIni[2]) + "-" +str(fechaIni[1]) + "-" + str(fechaIni[0])
                fechaFin = self.panelRepeticionTareaSimple.panelDesdeHasta.fechaHasta.GetValue().split("/")
                fechaFin = str(fechaFin[2]) + "-" +str(fechaFin[1]) + "-" + str(fechaFin[0])
                paraSiempre = self.panelRepeticionTareaSimple.panelDesdeHasta.cmbHasta.GetValue()
                horaEjecucion = self.panelEntradaDatos.ctrlHora.GetValue()
                if paraSiempre == "Para siempre": 
                    paraSiempre = True
                else:
                    paraSiempre=False
                    
                cur.execute("UPDATE CONFIG_TAREAS_PROGRAMADAS SET Config_Kg=%s, " +
                            "Fecha_Inicio=%s, Fecha_Fin=%s, Para_Siempre= %s, Hora_Ejecucion=%s " 
                            "WHERE idCONFIG_TAREAS_PROGRAMADA=%s;", (config_kg, fechaIni, fechaFin, 
                                                                     paraSiempre, horaEjecucion, self.tarea_programada,))
                bd.commit()   
                print "Configuración ", self.tarea_programada, "correctamente actualizada."
                #self.Destroy()
                #RECARGAR LAS TAREAS PROGRAMADAS...
                self.Show(False)
            except MySQLdb.OperationalError as error:
                print error[1]
                dlg = wx.MessageDialog(self,error[1], u'Error en configuración', wx.OK | wx.ICON_ERROR)
                dlg.ShowModal()
                dlg.Destroy()
        else: #MODO NUEVO...
            if self.verificarPanelRepeticionTareaSimple() == False:
                print "Ha ocurrido un error al validar el panel"
            else:
                self.Show(False)
                #Enviar notificacion al mediador
        
        
    def verificarPanelRepeticionTareaSimple(self):
        '''Validación de datos del panel'''
        self.compuerta = self.panelEntradaDatos.txtCompuerta.GetValue()
        self.config_alimento = None
        self.hora = None
        self.para_Siempre = None
        self.fecha_inicio = None
        self.fecha_fin = None
        
        if self.panelEntradaDatos.lstListaAlimentos.GetValue() == "":
            self.GetStatusBar().Show()
            self.GetStatusBar().SetStatusText("Debe seleccionar una configuración de alimento.")
            self.Fit()
            return False
        else:
            self.GetStatusBar().Hide()
            self.config_alimento = self.panelEntradaDatos.lstListaAlimentos.GetIdSeleccionado()
            
        if self.panelRepeticionTareaSimple.panelDesdeHasta.cmbHasta.GetValue() == "Para siempre":  
            self.para_Siempre = True
        else:
            
            self.para_Siempre = False
            
        try:                
            self.hora = [self.panelEntradaDatos.lstListaHoras.GetString(i) for i in range(self.panelEntradaDatos.lstListaHoras.GetCount())]
        except AttributeError as error:
            print error
            self.hora = [self.panelEntradaDatos.ctrlHora.GetValue()]
            
            
        self.fecha_inicio = self.panelRepeticionTareaSimple.panelDesdeHasta.fechaDesde.GetValue().split("/")
        self.fecha_inicio = self.fecha_inicio[2] + "-" + self.fecha_inicio[1] + "-" + self.fecha_inicio[0]
        self.fecha_fin = self.panelRepeticionTareaSimple.panelDesdeHasta.fechaHasta.GetValue().split("/")
        self.fecha_fin = self.fecha_fin[2] + "-" + self.fecha_fin[1] + "-" + self.fecha_fin[0]
        
        print "-----------------------"    
        print "Compuerta:", self.compuerta, "\n", \
        "Config_alimento:", self.config_alimento, "\n", \
        "Hora:", self.hora, "\n", \
        "Desde:", self.fecha_inicio, \
        "Hasta:", self.fecha_fin, "\n", \
        "Para siempre:", self.para_Siempre
         
        try: #Intento 
            bd = MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')
            #bd = self.getBd()
            for valHora in self.hora: #Si existe mas de una hora, creo un registro para cada una de ellas...
                cur = bd.cursor()
                cur.execute("INSERT INTO CONFIG_TAREAS_PROGRAMADAS (Id_Compuerta, Config_Kg, " +
                            "Fecha_Inicio, Fecha_Fin, Para_Siempre, Hora_Ejecucion, Cancelada) "+
                            "VALUES (%s, %s, %s, %s, %s, %s, %s)", (self.compuerta, self.config_alimento,
                            self.fecha_inicio, self.fecha_fin, self.para_Siempre, valHora, 0,))
                bd.commit()
        except MySQLdb.OperationalError as error:
            #print error
            self.GetStatusBar().Show()
            self.GetStatusBar().SetStatusText(error[1])
            self.Fit()
            return False
        
        
        self.Fit()
        return True   


            
class PanelRepeticionTareaSimple(wx.Panel):
    def __init__(self, parent, compuerta=None):
        wx.Panel.__init__(self, parent)
        #self.seleccionModoRepeticion = wx.ComboBox(self, -1) #RadioBox???
        lblList = ['Desde - Hasta', 'Selección dinámica']
        self.seleccionModoRepeticion = wx.RadioBox(self,label = 'Opciones de configuracion', choices = lblList ,
           majorDimension = 1, style = wx.RA_SPECIFY_ROWS)
        self.seleccionModoRepeticion.Bind(wx.EVT_RADIOBOX,self.onRadioBox)
        #self.seleccionModoRepeticion.Append("Desde hasta")
        #self.seleccionModoRepeticion.Append("Selección dinámica")

        sizerModoRepeticion = wx.BoxSizer(wx.HORIZONTAL)
        #sizerModoRepeticion.Add(lblModoRepeticion, 0, wx.ALIGN_CENTER_VERTICAL)
        sizerModoRepeticion.Add(self.seleccionModoRepeticion, 0, wx.ALL, 5)

        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.mainSizer.Add(sizerModoRepeticion)

        self.SetSizerAndFit(self.mainSizer)
        self.crearGUIDesdeHasta()


    def onRadioBox(self, event):
        print event.GetEventObject().GetStringSelection()

        if hasattr(self, 'mainPanel'):
                self.mainPanel.Hide()

        if event.GetEventObject().GetStringSelection() == "Desde - Hasta":
            if hasattr(self, 'panelSeleccionDinamica'):
                self.panelSeleccionDinamica.Hide()
            self.crearGUIDesdeHasta()
        else:
            if hasattr(self, 'panelDesdeHasta'):
                self.panelDesdeHasta.Hide()
            self.crearGUISeleccionDinamica()

        #self.crearGUIExcepciones()
        #self.Fit()
        self.Fit()
        self.GetParent().GetParent().Fit()
        self.GetParent().GetParent().Center()
        self.GetParent().GetParent().Layout()



    def crearGUIDesdeHasta(self):
        if hasattr(self, 'panelDesdeHasta') == False:
            self.panelDesdeHasta = PanelDesdeHasta(self)

            self.mainSizer.Add(self.panelDesdeHasta, 0, wx.EXPAND|wx.ALL, border=4)
            self.Fit()
            #self.Layout()
            self.GetParent().Fit()
        else:
            self.panelDesdeHasta.Show()
            self.Fit()
            #self.Layout()
            self.GetParent().Fit()


    def __ocultarPanel(self, event):
        if event.GetEventObject().GetSelection() > 0:
            self.fechaHasta.Hide()
        else:
            self.fechaHasta.Show()



    def crearGUISeleccionDinamica(self):
        '''La GUI dinámica debe proporcionar:
        * Cargar días y fechas por medio de doble click, ctrl+click tal cual para seleccionar archivos.
        * Se muestran las fechas en una lista inferior o se muestran directamente
        en el calendario de resultados???
        * Si la fecha es anterior a la actual, lanza error.
        '''
        if hasattr(self, 'panelSeleccionDinamica') == False:
            self.panelSeleccionDinamica = wx.Panel(self)
            self.teclaShift = False
            self.fechaPrevia = None
            lblNota = wx.StaticText(self.panelSeleccionDinamica, -1,"Nota: para agregar una fecha haga DOBLE CLICK sobre la misma. \nSi desea agregar un rango de días, presione la tecla Shift\n antes de hacer click en la fecha final.")
            self.calendario1 = wx.calendar.CalendarCtrl(self.panelSeleccionDinamica, -1, size=(350, 200))
            self.calendario1.SetToolTipString("Si mantiene presionada la tecla Shift puede seleccionar un rango de fechas.")
            self.Bind(wx.calendar.EVT_CALENDAR_DAY, self.seleccionFecha, self.calendario1)
            self.calendario1.Bind(wx.EVT_KEY_UP, self.teclaPresionada)
            self.calendario1.Bind(wx.EVT_KEY_DOWN, self.teclaPresionada)
            self.calendario1.Bind(wx.calendar.EVT_CALENDAR, self.agregarFecha)
            self.Bind(wx.calendar.EVT_CALENDAR_SEL_CHANGED, self.OnCalSelChanged, self.calendario1)

            self.listaFechasSeleccionadas = wx.ListBox(self.panelSeleccionDinamica, -1, size=(350, 100))

            sizerSeleccionDinamica = wx.BoxSizer(wx.VERTICAL)
            sizerSeleccionDinamica.Add(lblNota)
            sizerSeleccionDinamica.Add(self.calendario1)
            sizerSeleccionDinamica.Add(self.listaFechasSeleccionadas)

            btnEliminar = wx.Button(self.panelSeleccionDinamica, -1, "Eliminar")
            btnEliminar.Bind(wx.EVT_BUTTON, self.eliminarSeleccionDinamica)
            sizerSeleccionDinamica.Add(btnEliminar)
            self.panelSeleccionDinamica.SetSizerAndFit(sizerSeleccionDinamica)

            self.mainSizer.Add(self.panelSeleccionDinamica)
            self.Fit()
            #self.Layout()
            self.GetParent().Fit()
        else:
            self.panelSeleccionDinamica.Show()
            self.Fit()
            #self.Layout()
            self.GetParent().Fit()


    def eliminarSeleccionDinamica(self, event):
        if self.listaFechasSeleccionadas.GetSelection() >= 0:
            self.listaFechasSeleccionadas.Delete(self.listaFechasSeleccionadas.GetSelection())



    def seleccionFecha(self, event):
        if self.teclaShift == True:
            #print "Tecla ctrl presionada"
            #print self.fechaPrevia
            #self.calendario1.SetDateRange(self.fechaPrevia, self.calendario1.GetDate())
            self.listaFechasSeleccionadas.Append("" + str(self.fechaPrevia.Format("%d de %B de %Y")) + " al " + str(self.calendario1.GetDate().Format("%d %B de %Y")))


    def teclaPresionada(self, event):
        #print event.GetKeyCode()
        self.teclaShift = event.ShiftDown()
        print self.teclaShift
        self.fechaPrevia = self.calendario1.GetDate()
        event.Skip()


    def agregarFecha(self, event):
        fecha_seleccionada=event.GetEventObject().GetDate().Format("%d de %B de %Y")
        #print fecha_seleccionada.Format("%Y-%m-%d")
        #fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")
        self.listaFechasSeleccionadas.Append(fecha_seleccionada)


    def OnCalSelChanged(self, event):
        #Si se selecciona un dia pasado, que vuelva al mismo dia...Si es superior no pasa nada
        fecha_seleccionada=event.GetDate().Format("%Y-%m-%d")
        #print fecha_seleccionada.Format("%Y-%m-%d")
        fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")
        #print fecha_actual
        #print type(event.GetEventObject())
        if time.strptime(fecha_seleccionada, "%Y-%m-%d") < time.strptime(fecha_actual, "%Y-%m-%d"):
            date = wx.DateTime_Now()
            if type(event.GetEventObject()) == wx._controls.DatePickerCtrl:
                event.GetEventObject().SetValue(date)
            else:
                event.GetEventObject().SetDate(date)
            dlg = wx.MessageDialog(self,'No puede seleccionar una fecha anterior.', u'Error de selección', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()


class GUIExcepciones(wx.Panel):
    def __init__(self, parent, modoEdit=False):
        wx.Panel.__init__(self, parent)
        self.modoEdit = modoEdit
        self.crearGUIExcepciones()


    def crearGUIExcepciones(self):
        print "Creando gui excepciones"
        self.mainPanel = wx.Panel(self)
        self.panelDesdeHasta = PanelDesdeHasta(self.mainPanel, False)
        self.panelListaExcepciones = PanelListaFechasModificacionesCancelacionesInstancias(self, ["Desde", "Hasta", "IdExcepcion"])
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(self.panelDesdeHasta, 0, wx.EXPAND|wx.ALL, border=4)
        mainSizer.Add(self.panelListaExcepciones, 0, wx.EXPAND|wx.ALL, border=4)
        self.mainPanel.SetSizerAndFit(mainSizer)
        self.Fit()
        self.GetParent().Fit()

    def agregar(self, event):
        '''Tenemos validación en la BD mediante triggers y tenemos validación
        en la vista.
        Si esta en modo edición, se inserta el registro en la vista y en la BD.
        Caso contrario, solo en la vista. Luego cuando el usuario da Aceptar, se 
        procesa la misma.'''
        print "Agregando excepcion"
        #entrada = "Desde el " + str(self.panelDesdeHasta.fechaDesde.GetValue()) + " al " + str(self.panelDesdeHasta.fechaHasta.GetValue())
        #if not entrada in self.panelListaExcepciones.listaExcepciones.GetStrings():
        #    self.panelListaExcepciones.listaExcepciones.Append(entrada)
        if self.panelDesdeHasta.fechaHasta.GetValue() < self.panelDesdeHasta.fechaDesde.GetValue():
            self.GetParent().GetParent().GetStatusBar().Show()
            self.GetParent().GetParent().GetStatusBar().SetStatusText("La fecha Hasta no puede ser menor a la fecha de Inicio.")
        else:
            self.GetParent().GetParent().GetStatusBar().Hide()
            res = self.panelListaExcepciones.validarRegistroExcepcionesInstancias([self.panelDesdeHasta.fechaDesde.GetValue(), self.panelDesdeHasta.fechaHasta.GetValue()])
            if res == True: #Cormpuebo si el registro no esta duplicado.
                if self.modoEdit == True: #modo edicion, envío los cambios en la BD
                    try: 
                        bd = self.GetParent().GetParent().getBd()
                        cur = bd.cursor()
                        tarea_programada = self.GetParent().GetParent().tarea_programada
                        fechaIni = self.panelDesdeHasta.fechaDesde.GetValue().split("/")
                        fechaIni = str(fechaIni[2]) + "-" +str(fechaIni[1]) + "-" + str(fechaIni[0])
                        fechaFin = self.panelDesdeHasta.fechaHasta.GetValue().split("/")
                        fechaFin = str(fechaFin[2]) + "-" +str(fechaFin[1]) + "-" + str(fechaFin[0])
                        cur.execute("INSERT INTO EXCEPCIONES_INSTANCIAS_TAREAS "
                                    "(Id_Config_Tarea, Fecha_Inicio, Fecha_Fin, Cancelada) "
                                    "VALUES (%s, %s, %s, %s)", (tarea_programada, fechaIni, fechaFin, 0))
                        bd.commit()
                        res = self.panelListaExcepciones.agregarRegistro([self.panelDesdeHasta.fechaDesde.GetValue(), self.panelDesdeHasta.fechaHasta.GetValue(), cur.lastrowid])
                        self.GetParent().GetParent().GetStatusBar().Hide()
                    except MySQLdb.OperationalError as error: 
                        print error[1]
                        self.GetParent().GetParent().GetStatusBar().Show()
                        self.GetParent().GetParent().GetStatusBar().SetStatusText(error[1])
                else: #No esta en modo edicion, lo agrego a la vista
                    self.panelListaExcepciones.agregarRegistro([self.panelDesdeHasta.fechaDesde.GetValue(), self.panelDesdeHasta.fechaHasta.GetValue()])
            else:
                self.GetParent().GetParent().GetStatusBar().Show()
                self.GetParent().GetParent().GetStatusBar().SetStatusText("Ya existe un registro idéntico como el que intenta ingresar.")

        self.Fit()
        self.GetParent().Fit()
        self.GetParent().GetParent().Fit()
            


    def modificar(self, event):
        '''Si la vista esta en modo edición, envío los cambios en la BD.'''
        print "Modificando excepcion"
        res = self.panelListaExcepciones.validarRegistroExcepcionesInstancias([self.panelDesdeHasta.fechaDesde.GetValue(), self.panelDesdeHasta.fechaHasta.GetValue()])
        if res == True:
            if self.modoEdit == True: #Si se agregó correctamente a la vista, lo agrego en la BD
                try:
                    bd = self.GetParent().GetParent().getBd()
                    cur = bd.cursor()
                    #tarea_programada = self.GetParent().GetParent().tarea_programada
                    id_excepcion = self.panelListaExcepciones.getGrilla().GetCellValue(self.panelListaExcepciones.getGrilla().GetGridCursorRow(), 2)
                    fechaIni = self.panelDesdeHasta.fechaDesde.GetValue().split("/")
                    fechaIni = str(fechaIni[2]) + "-" +str(fechaIni[1]) + "-" + str(fechaIni[0])
                    fechaFin = self.panelDesdeHasta.fechaHasta.GetValue().split("/")
                    fechaFin = str(fechaFin[2]) + "-" +str(fechaFin[1]) + "-" + str(fechaFin[0])
                    cur.execute("UPDATE EXCEPCIONES_INSTANCIAS_TAREAS " 
                    "SET Fecha_Inicio=%s, Fecha_Fin=%s "
                    "WHERE idEXCEPCIONES_INSTANCIA_TAREA=%s;", (fechaIni, fechaFin, id_excepcion,))
                    bd.commit()
                    self.panelListaExcepciones.actualizarRegistro([self.panelDesdeHasta.fechaDesde.GetValue(), self.panelDesdeHasta.fechaHasta.GetValue()])
                    self.GetParent().GetParent().GetStatusBar().Hide()
                    print "Instancia ", id_excepcion, " actualizada..."
                except MySQLdb.OperationalError as error:
                    print error[1]
                    self.GetParent().GetParent().GetStatusBar().Show()
                    self.GetParent().GetParent().GetStatusBar().SetStatusText(error[1])
            else: #Cuando no esta en modo edición, actualizo el registro en la vista
                self.panelListaExcepciones.actualizarRegistro([self.panelDesdeHasta.fechaDesde.GetValue(), self.panelDesdeHasta.fechaHasta.GetValue()])
        else:
            self.GetParent().GetParent().GetStatusBar().Show()
            self.GetParent().GetParent().GetStatusBar().SetStatusText("Existe un registro repetido con los valores que intenta ingresar.")
        
        self.Fit()
        self.GetParent().Fit()
        self.GetParent().GetParent().Fit()



    def eliminar(self, event):
        '''Si esta en modo edición, elimino el registro de la BD y luego de la vista...'''
        print "Eliminar excepcion"
        if self.panelListaExcepciones.getGrilla().GetGridCursorRow() >= 0:
            if self.modoEdit == True: 
                try:
                    bd = self.GetParent().GetParent().getBd()
                    cur = bd.cursor()
                    id_mod_instancia = self.panelListaExcepciones.getGrilla().GetCellValue(self.panelListaExcepciones.getGrilla().GetGridCursorRow(), 2)
                    cur.execute("UPDATE EXCEPCIONES_INSTANCIAS_TAREAS SET Cancelada=1 WHERE idEXCEPCIONES_INSTANCIA_TAREA=%s", (id_mod_instancia,))
                    bd.commit()
                    self.panelListaExcepciones.getGrilla().DeleteRows(self.panelListaExcepciones.getGrilla().GetGridCursorRow())
                    self.GetParent().GetParent().GetStatusBar().Hide()
                    print "Instancia ", id_mod_instancia, " eliminada..."
                except MySQLdb.OperationalError as error:
                    print error[1]
                    self.GetParent().GetParent().GetStatusBar().Show()
                    self.GetParent().GetParent().GetStatusBar().SetStatusText(error[1])
            else: #Cuando no esta en modo edicion, se lo elimina directamente de la vista
                self.panelListaExcepciones.getGrilla().DeleteRows(self.panelListaExcepciones.getGrilla().GetGridCursorRow())            
            
        self.Fit()
        self.GetParent().Fit()
        self.GetParent().GetParent().Fit()


    def actualizarVista(self, event):
        fechaDesde = self.panelListaExcepciones.getGrilla().GetCellValue(event.GetRow(), 0)
        if len(fechaDesde) > 0:
            fecha = map(int, fechaDesde.split("/"))
            fechaDesde = wx.DateTimeFromDMY(fecha[0], fecha[1]-1, fecha[2])
            fechaHasta = self.panelListaExcepciones.getGrilla().GetCellValue(event.GetRow(), 1)
            fecha = map(int, fechaHasta.split("/"))
            fechaHasta = wx.DateTimeFromDMY(fecha[0], fecha[1]-1, fecha[2])
            self.panelDesdeHasta.fechaDesde.SetValue(fechaDesde)
            self.panelDesdeHasta.fechaHasta.SetValue(fechaHasta)






class GUINuevaDosificacionAutomatica(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.crearPanelDatosBasico()


    def crearPanelDatosBasico(self):
        self.panelDatosBasico = wx.Panel(self)
        lblCompuerta=wx.StaticText(self.panelDatosBasico, -1, "Compuerta:")
        self.txtCompuerta = wx.TextCtrl(self.panelDatosBasico, -1)
        self.txtCompuerta.Disable()
        lblConfigAlimento = wx.StaticText(self.panelDatosBasico, -1, "Config. Alimento:")
        self.lstListaAlimentos = ListBoxListaAlimentos(self.panelDatosBasico)  #wx.ComboBox(self.panelDatosBasico, -1)
        btnEditarAlimentos = wx.Button(self.panelDatosBasico, -1, "Editar")
        lblHoraDosificacion = wx.StaticText(self.panelDatosBasico, -1, "Hora de dosificacion:")
        self.ctrlHora = TimerCtrl(self.panelDatosBasico)
        self.ctrlHora.setValue(datetime.datetime.now().time().strftime("%H:%M:%S"))
        if self.GetParent().modo_edit == False:
            self.btnAgregarHora = wx.Button(self.panelDatosBasico, -1, "Agregar otra hora")
            self.btnAgregarHora.Bind(wx.EVT_BUTTON, self.crearPanelHora)

        self.sizerDatosBasicos = wx.GridBagSizer(3,4)
        self.sizerDatosBasicos.Add(lblCompuerta, pos=(0, 0),flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        self.sizerDatosBasicos.Add(self.txtCompuerta, pos=(0, 1),flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        self.sizerDatosBasicos.Add(lblConfigAlimento, pos=(0, 2), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        self.sizerDatosBasicos.Add(self.lstListaAlimentos, pos=(0, 3), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        self.sizerDatosBasicos.Add(btnEditarAlimentos, pos=(0, 4), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        self.sizerDatosBasicos.Add(lblHoraDosificacion, pos=(1, 0), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        self.sizerDatosBasicos.Add(self.ctrlHora, pos=(1, 1), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        if self.GetParent().modo_edit == False:
            self.sizerDatosBasicos.Add(self.btnAgregarHora, pos=(1, 2), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)

        self.panelDatosBasico.SetSizerAndFit(self.sizerDatosBasicos)


    def crearPanelhora(self):
        self.panelHorasDosificacion = wx.Panel(self)
        self.lstListaHoras = wx.ListBox(self.panelHorasDosificacion, -1, size=(200, -1))
        self.lstListaHoras.Append(self.ctrlHora.GetValue())
        self.ctrlHora.SetFocus()
        self.btnNuevaHora = wx.Button(self.panelHorasDosificacion, -1, "Agregar hora")
        self.btnNuevaHora.Bind(wx.EVT_BUTTON, self.agregarPuntodeApertura)
        #self.btnEditarHora = wx.Button(self.panelHorasDosificacion, -1, "Editar hora")
        #self.btnEditarHora.Bind(wx.EVT_BUTTON, self.editarPuntoApertura)
        self.btnEliminarHora = wx.Button(self.panelHorasDosificacion, -1, "Eliminar hora")
        self.btnEliminarHora.Bind(wx.EVT_BUTTON, self.eliminarPuntoApertura)

        sizerListaHoras = wx.BoxSizer(wx.HORIZONTAL)
        sizerListaHoras.Add(self.lstListaHoras, 0, wx.EXPAND|wx.ALL)

        sizerBotones = wx.BoxSizer(wx.VERTICAL)
        sizerBotones.Add(self.btnNuevaHora)
        #sizerBotones.Add(self.btnEditarHora)
        sizerBotones.Add(self.btnEliminarHora)

        sizerListaHoras.Add(sizerBotones)
        self.panelHorasDosificacion.SetSizerAndFit(sizerListaHoras)
        itm = self.sizerDatosBasicos.FindItemAtPosition((1, 2))
        self.sizerDatosBasicos.Detach(itm.GetWindow())
        self.sizerDatosBasicos.Add(self.panelHorasDosificacion, pos=(1, 2), span=(1, 4), flag=wx.TOP|wx.ALL|wx.EXPAND, border=4)
        self.panelDatosBasico.SetSizerAndFit(self.sizerDatosBasicos)
        self.Fit()
        self.GetParent().Fit()


    def agregarPuntodeApertura(self, event):
        if not self.ctrlHora.GetValue() in self.lstListaHoras.GetStrings():
            self.lstListaHoras.Append(self.ctrlHora.GetValue())
            listaOrdenada = self.lstListaHoras.GetStrings()
            listaOrdenada.sort()
            self.lstListaHoras.Clear()
            self.lstListaHoras.AppendItems(listaOrdenada)


    def eliminarPuntoApertura(self, event):
        if self.lstListaHoras.GetSelection() >= 0:
            self.ctrlHora.setValue(self.lstListaHoras.GetString(self.lstListaHoras.GetSelection()))
            self.lstListaHoras.Delete(self.lstListaHoras.GetSelection())


    def crearPanelHora(self, event):
        self.crearPanelhora()


class PanelDesdeHasta(wx.Panel):
    def __init__(self, parent, comboHasta=True):
        wx.Panel.__init__(self, parent)
        self.crearPanelDesdeHasta(comboHasta)

    def crearPanelDesdeHasta(self, valor):
        self.panelDesdeHasta = wx.Panel(self)
        lblDesde = wx.StaticText(self.panelDesdeHasta, -1, "Desde")
        self.fechaDesde = DatePickerCtrlV2(self.panelDesdeHasta)
        self.Bind(wx.calendar.EVT_CALENDAR_SEL_CHANGED, self.OnCalSelChanged, self.fechaDesde.getCalendarCtrl())
        self.cmbHasta = wx.ComboBox(self.panelDesdeHasta, -1)
        self.cmbHasta.Bind(wx.EVT_COMBOBOX, self.__ocultarPanel)
        self.cmbHasta.Append("Hasta")
        self.cmbHasta.Append("Para siempre")
        self.cmbHasta.Select(0)

        self.fechaHasta = DatePickerCtrlV2(self.panelDesdeHasta)

        sizerDistribucion = wx.BoxSizer(wx.HORIZONTAL)
        sizerDistribucion.Add(lblDesde, 0, wx.ALIGN_CENTER_VERTICAL)
        sizerDistribucion.Add(self.fechaDesde)
        sizerDistribucion.Add(self.cmbHasta, 0, wx.ALIGN_CENTER_VERTICAL)
        sizerDistribucion.Add(self.fechaHasta)
        #self.panelMensaje.Hide()

        self.panelDesdeHasta.SetSizerAndFit(sizerDistribucion)

        self.Fit()
        self.GetParent().Fit()
        self.__activarCtrlcmbHasta(valor)


    def OnCalSelChanged(self, event):
        fecha_seleccionada=event.GetDate().Format("%Y-%m-%d")
        #print fecha_seleccionada.Format("%Y-%m-%d")
        fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")
        #print fecha_actual
        #print type(event.GetEventObject())
        if time.strptime(fecha_seleccionada, "%Y-%m-%d") < time.strptime(fecha_actual, "%Y-%m-%d"):
            date = wx.DateTime_Now()
            if type(event.GetEventObject()) == wx._controls.DatePickerCtrl:
                event.GetEventObject().SetValue(date)
            else:
                event.GetEventObject().SetDate(date)
            dlg = wx.MessageDialog(self,'No puede seleccionar una fecha anterior.', u'Error de selección', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
        #print self.fechaHasta.GetValue()
        if time.strptime(self.fechaDesde.GetValue(),"%d/%m/%Y") > time.strptime(self.fechaHasta.GetValue(),"%d/%m/%Y"): #Si la fechadesde es > de fechaHasta, se igualan
            fecha = map(int, self.fechaDesde.GetValue().split("/"))
            fechaDesde = wx.DateTimeFromDMY(fecha[0], fecha[1]-1, fecha[2])
            self.fechaHasta.SetValue(fechaDesde)

            #self.fechaDesde.SetValue(event.GetDate())
        #else:
        #    print "Menor"



    def __ocultarPanel(self, event):
        if event.GetEventObject().GetSelection() > 0:
            self.fechaHasta.Hide()
        else:
            self.fechaHasta.Show()


    def __activarCtrlcmbHasta(self, valor):
        self.cmbHasta.Enable(valor)




class PanelListaFechasModificacionesCancelacionesInstancias(wx.Panel):
    '''Esta clase crea un panel que muestra una lista con datos.
    Tiene 3 botones (Agregar, Modificar, Eliminar), dónde cada clase que cree este panel,
    debe reescribir la funcionalidad para cada uno de ellos.'''
    def __init__(self, parent, lista_Columnas):
        wx.Panel.__init__(self, parent)
        self.crearPanel(lista_Columnas)


    def crearPanel(self, lista_cols):
        self.mainPanel = wx.Panel(self, -1)
        #self.listaExcepciones = wx.ListBox(self.mainPanel, -1, size=(500, -1))

        self.__gridExcepciones = Grid(self.mainPanel, -1, size=(580,150))
        self.__gridExcepciones.EnableEditing(False)
        self.__gridExcepciones.SetToolTipString("Presione la tecla Supr para eliminar un registro.")

        # creamos las filas y columnas
        self.__gridExcepciones.CreateGrid(0,len(lista_cols))

        # definimos los campos que contiene el gridExcepciones
        for col in lista_cols:
            self.__gridExcepciones.SetColLabelValue(lista_cols.index(col), col)


        # definimos los tamanos
        self.__gridExcepciones.SetRowLabelSize(50)
        self.__gridExcepciones.SetColSize(0,150)
        self.__gridExcepciones.SetColSize(1,150)
        self.__gridExcepciones.Bind(wx.grid.EVT_GRID_SELECT_CELL, self.GetParent().actualizarVista)
        self.__gridExcepciones.Bind(wx.grid.EVT_GRID_SELECT_CELL, self.onSingleSelect)


        btnAgregarExcepcion = wx.Button(self.mainPanel, -1, "Agregar")
        btnAgregarExcepcion.Bind(wx.EVT_BUTTON, self.GetParent().agregar)
        #btnAgregarExcepcion.Bind(wx.EVT_BUTTON, self.agregarExcepcion)
        btnModificarExcepcion = wx.Button(self.mainPanel, -1, "Modificar")
        btnModificarExcepcion.Bind(wx.EVT_BUTTON, self.GetParent().modificar)
        #btnModificarExcepcion.Bind(wx.EVT_BUTTON, self.cambiarExcepcion)
        btnEliminaExcepcion = wx.Button(self.mainPanel, -1, "Eliminar")
        btnEliminaExcepcion.Bind(wx.EVT_BUTTON, self.GetParent().eliminar)
        #btnEliminaExcepcion.Bind(wx.EVT_BUTTON, self.eliminarExcepcion)

        sizerBotonesExcepciones = wx.BoxSizer(wx.HORIZONTAL)
        sizerBotonesExcepciones.Add(btnAgregarExcepcion)
        sizerBotonesExcepciones.Add(btnModificarExcepcion)
        sizerBotonesExcepciones.Add(btnEliminaExcepcion)

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        #mainSizer.Add(self.listaExcepciones, 0, wx.EXPAND|wx.ALL, border=4)
        mainSizer.Add(self.__gridExcepciones, 0, wx.EXPAND|wx.ALL, border=4)
        mainSizer.Add(sizerBotonesExcepciones, border=4)

        self.mainPanel.SetSizerAndFit(mainSizer)

        #self.mainSizer.Add(self.mainPanel)
        self.Fit()
            #self.Layout()
        self.GetParent().Fit()

    def validarRegistroExcepcionesInstancias(self, registro):
        for x in range(0, self.__gridExcepciones.GetNumberRows()):
            val =[]
            for i in range(0, self.__gridExcepciones.GetNumberCols()):
                val.append(self.__gridExcepciones.GetCellValue(x, i))
            try:
                #No se permiten agregar registros iguales o donde la fecha y hora sera igual...
                if val == registro or val[0] == registro[0] and val[1] == registro[1]:
                    return False #Hay un registro identico, no permito que se agregue
            except: #Si no existe el registro, continuo
                pass
            
        return True
    
    
    def validarRegistroModificacionesInstancias(self, registro):
        for x in range(0, self.__gridExcepciones.GetNumberRows()):
            val =[]
            for i in range(0, self.__gridExcepciones.GetNumberCols()):
                val.append(self.__gridExcepciones.GetCellValue(x, i))
            try:
                #No se permiten agregar registros iguales o donde la fecha y hora sera igual...
                if val == registro or val[0] == registro[0] and val[2] == registro[2] and val[3]==registro[3]:
                    return False
            except: #Si no existe el registro, continuo
                pass
            
        return True

    
    def agregarRegistro(self, registro):
        self.__gridExcepciones.AppendRows(1)
        for pos in range(0, len(registro)):
            self.__gridExcepciones.SetCellValue(self.__gridExcepciones.GetNumberRows()-1, pos,"%s" %registro[pos])
        
    
    
    def actualizarRegistro(self, registro):
        '''Se debe realizar una verificacion antes de la actualización...'''            
        for pos in range(0, len(registro)):
            self.__gridExcepciones.SetCellValue(self.currentlySelectedCell[0] , pos,"%s" %registro[pos])
            
    

    def getGrilla(self):
        return self.__gridExcepciones
    
    
    def onSingleSelect(self, event):
        self.currentlySelectedCell = (event.GetRow(),
                                      event.GetCol())
        event.Skip()
    
    



class PanelLineaTiempo(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, size=(-1, 50))
        self.SetBackgroundColour("white")
        self.Bind(wx.EVT_PAINT, self.crearLineaTiempo)
        #self.crearLineaTiempo()

    def crearLineaTiempo(self, event):
        print "Dibujando"
        self.dc = wx.PaintDC(self)
        rect = self.GetClientRect()
        # "Set a red brush to draw a rectangle"
        #wx.Button(self, -1, "dsd")
        self.dc.SetBrush(wx.GREEN_BRUSH)
        self.dc.DrawRectangle(10, 10, rect[2]-20, 30)
        self.dc.SetBrush(wx.Brush("black"))
        self.dc.DrawRectangle(30, -10, rect[2]-100, 30)
        self.dc.DrawLine(0, 0, 50, 50)

        #self.SetSize((dc.GetCharWidth(), dc.GetCharHeight()))

    def crearImagen(self):
        print self.GetSize()[0], self.GetSize()[1]
        self.SetSize((-1, 80))
        #self.GetParent().Fit()


#//Hola
class GUIModificacionesInstancias(wx.Panel):
    def __init__(self, parent, modoEdit=False):
        wx.Panel.__init__(self, parent)
        self.modoEdit = modoEdit
        self.crearGUIModificacionesInstancias()

    def crearGUIModificacionesInstancias(self):
        self.panelModificacionesInstancias = wx.Panel(self)
        lblHoraModificacion = wx.StaticText(self.panelModificacionesInstancias, -1, "Hora de ejecución:")
        self.controlHora = TimerCtrl(self.panelModificacionesInstancias)
        self.controlHora.setValue(datetime.datetime.now().time().strftime("%H:%M:%S"))
        lblConfigAlimento = wx.StaticText(self.panelModificacionesInstancias, -1, "Configuración del alimento:")
        self.lstListaConfigAlimento = ListBoxListaAlimentos(self.panelModificacionesInstancias) #wx.ComboBox(self.panelModificacionesInstancias, -1)
        #lblDesde = wx.StaticText(self.panelModificacionesInstancias, -1, "Desde:")
        #self.txtFechaDesde =  wx.calendar.CalendarCtrl(self.panelSeleccionDinamica, -1)
        self.panelDesdeHasta = PanelDesdeHasta(self.panelModificacionesInstancias, False)
        self.panelListaModificaciones = PanelListaFechasModificacionesCancelacionesInstancias(self, ["Hora", "Config_KG", "Desde", "Hasta", "ConfigId"])

        sizerPanelModificacionesInstancias = wx.GridBagSizer(3, 3)
        sizerPanelModificacionesInstancias.Add(lblHoraModificacion, pos=(0, 0), flag=wx.TOP|wx.ALL|wx.EXPAND, border=4)
        sizerPanelModificacionesInstancias.Add(self.controlHora, pos=(0, 1), flag=wx.TOP|wx.ALL|wx.EXPAND, border=4)
        sizerPanelModificacionesInstancias.Add(lblConfigAlimento, pos=(0, 2), flag=wx.TOP|wx.ALL|wx.EXPAND, border=4)
        sizerPanelModificacionesInstancias.Add(self.lstListaConfigAlimento, pos=(0, 3), flag=wx.TOP|wx.ALL|wx.EXPAND, border=4)
        sizerPanelModificacionesInstancias.Add(self.panelDesdeHasta, pos=(1, 0), span=(1, 3), flag=wx.TOP|wx.ALL|wx.EXPAND, border=4)
        sizerPanelModificacionesInstancias.Add(self.panelListaModificaciones, pos=(2, 0), span=(1, 4), flag=wx.TOP|wx.ALL|wx.EXPAND, border=4)
        self.panelModificacionesInstancias.SetSizerAndFit(sizerPanelModificacionesInstancias)
        self.Fit()
        self.GetParent().Fit()
        #Agregar el calendario seleccion dinamica para el establecimiento de fechas

    def agregar(self, event):
        '''Cuando la vista es para nuevo regitro, se agrega únicamente a la vista.
        El botón aceptar luego procesa la vista agregando los registros a la BD.
        Cuando la vista esta en modo edición, al agregar un registro, se agrega el 
        mismo a la BD porque se conoce su codigo de tarea. 
        '''
        print "Agregando instancia"
        if self.lstListaConfigAlimento.GetValue() != "":
            res = self.panelListaModificaciones.validarRegistroModificacionesInstancias([self.controlHora.GetValue(), self.lstListaConfigAlimento.GetValue(), self.panelDesdeHasta.fechaDesde.GetValue(), self.panelDesdeHasta.fechaHasta.GetValue()])
            if res == True:
                if self.modoEdit == True: #Si se agregó correctamente a la vista, lo agrego en la BD
                    try:
                        bd = self.GetParent().GetParent().getBd()
                        tarea_programada = self.GetParent().GetParent().tarea_programada
                        cur = bd.cursor()
                        fechaIni = self.panelDesdeHasta.fechaDesde.GetValue().split("/")
                        fechaIni = str(fechaIni[2]) + "-" +str(fechaIni[1]) + "-" + str(fechaIni[0])
                        fechaFin = self.panelDesdeHasta.fechaHasta.GetValue().split("/")
                        fechaFin = str(fechaFin[2]) + "-" +str(fechaFin[1]) + "-" + str(fechaFin[0])
                        configKg = self.lstListaConfigAlimento.GetIdSeleccionado()
                        cur.execute ("INSERT INTO MODIFICACION_INSTANCIAS_TAREAS "+
                                        "(Id_Config_Tarea, Fecha_Inicio, Fecha_Fin, Hora_Ejecucion, Config_Kg) "
                                         " VALUES (%s, %s, %s, %s, %s)",
                                         (tarea_programada, fechaIni,
                                          fechaFin, self.controlHora.GetValue(), configKg))
                        bd.commit()
                        self.panelListaModificaciones.agregarRegistro([self.controlHora.GetValue(), self.lstListaConfigAlimento.GetValue(), self.panelDesdeHasta.fechaDesde.GetValue(), self.panelDesdeHasta.fechaHasta.GetValue(), cur.lastrowid])
                        self.GetParent().GetParent().GetStatusBar().Hide()
                        print "Modificación de instancia ", cur.lastrowid, "agregada correctamente."
                    except MySQLdb.OperationalError as error: 
                        print error[1] #La fecha de inicio no puede ser menor al presente.
                        self.GetParent().GetParent().GetStatusBar().Show()
                        self.GetParent().GetParent().GetStatusBar().SetStatusText(error[1])
                else: #en el else, es para nuevo registro NO MODO EDICIÓN. 
                    self.panelListaModificaciones.agregarRegistro([self.controlHora.GetValue(), self.lstListaConfigAlimento.GetValue(), self.panelDesdeHasta.fechaDesde.GetValue(), self.panelDesdeHasta.fechaHasta.GetValue()])
            else:
                self.GetParent().GetParent().GetStatusBar().Show()
                self.GetParent().GetParent().GetStatusBar().SetStatusText("Existe un registro idéntico al que intenta agregar.")
        else:
            self.GetParent().GetParent().GetStatusBar().Show()
            self.GetParent().GetParent().GetStatusBar().SetStatusText("Debe seleccionar una configuración de alimento.")
        self.Fit()
        self.GetParent().Fit()
        self.GetParent().GetParent().Fit()


    def modificar(self, event):
        '''Si la vista está en modo edición, se guardan los cambios en la BD
        y se actualiza la vista. Caso contrario, únicamente se actualiza la vista.'''
        print "Modificando instancia"
        if self.lstListaConfigAlimento.GetValue() != "":
            res = self.panelListaModificaciones.validarRegistroModificacionesInstancias([self.controlHora.GetValue(), self.lstListaConfigAlimento.GetValue(), self.panelDesdeHasta.fechaDesde.GetValue(), self.panelDesdeHasta.fechaHasta.GetValue()])
            if res == True: #Verifico si existe un registro similar en la vista
                if self.modoEdit == True: #Si se agregó correctamente a la vista, lo agrego en la BD
                    try:
                        bd = self.GetParent().GetParent().getBd()
                        id_mod_instancia = self.panelListaModificaciones.getGrilla().GetCellValue(self.panelListaModificaciones.getGrilla().GetGridCursorRow(), 4)
                        cur = bd.cursor()
                        fechaIni = self.panelDesdeHasta.fechaDesde.GetValue().split("/")
                        fechaIni = str(fechaIni[2]) + "-" +str(fechaIni[1]) + "-" + str(fechaIni[0])
                        fechaFin = self.panelDesdeHasta.fechaHasta.GetValue().split("/")
                        fechaFin = str(fechaFin[2]) + "-" +str(fechaFin[1]) + "-" + str(fechaFin[0])
                        configKg = self.lstListaConfigAlimento.GetIdSeleccionado()
                        cur.execute ("UPDATE MODIFICACION_INSTANCIAS_TAREAS "+
                                        "set Fecha_Inicio=%s, Fecha_Fin=%s, Hora_Ejecucion=%s, Config_Kg=%s "
                                         "WHERE idMODIFICACION_INSTANCIA_TAREA=%s",
                                    (fechaIni, fechaFin, self.controlHora.GetValue(), configKg, id_mod_instancia,))
                        bd.commit()
                        self.panelListaModificaciones.actualizarRegistro([self.controlHora.GetValue(), self.lstListaConfigAlimento.GetValue(), self.panelDesdeHasta.fechaDesde.GetValue(), self.panelDesdeHasta.fechaHasta.GetValue()])
                        self.GetParent().GetParent().GetStatusBar().Hide()
                    except MySQLdb.OperationalError as error: 
                        print error[1] #La fecha de inicio no puede ser menor al presente.
                        self.GetParent().GetParent().GetStatusBar().Show()
                        self.GetParent().GetParent().GetStatusBar().SetStatusText(error[1])
                else: #REALIZAR VALIDACIONES PARA CUANDO NO SE ESTÁ EN MODO EDICION 
                    self.panelListaModificaciones.actualizarRegistro([self.controlHora.GetValue(), self.lstListaConfigAlimento.GetValue(), self.panelDesdeHasta.fechaDesde.GetValue(), self.panelDesdeHasta.fechaHasta.GetValue()])
            else:
                self.GetParent().GetParent().GetStatusBar().Show()
                self.GetParent().GetParent().GetStatusBar().SetStatusText("Existe un registro idéntico al que intenta agregar.")
        else:
            self.GetParent().GetParent().GetStatusBar().Show()
            self.GetParent().GetParent().GetStatusBar().SetStatusText("Debe seleccionar una configuración de alimento.")
        self.Fit()
        self.GetParent().Fit()
        self.GetParent().GetParent().Fit()
        


    def eliminar(self, event):
        '''Si la vista está en modo edición, cambio el registro en la BD y lo elimino
        de la vista...Caso contrario únicamente lo elimino de la vista.'''
        print "Eliminar instancia"
        if self.panelListaModificaciones.getGrilla().GetGridCursorRow() >= 0:
            if self.modoEdit == True: #Modo edicion, elimino el regitro de la BD y actualizo la vista...
                try:
                    bd = self.GetParent().GetParent().getBd()
                    cur = bd.cursor()
                    id_mod_instancia = self.panelListaModificaciones.getGrilla().GetCellValue(self.panelListaModificaciones.getGrilla().GetGridCursorRow(), 4)
                    cur.execute("UPDATE MODIFICACION_INSTANCIAS_TAREAS SET Cancelada=1 WHERE idMODIFICACION_INSTANCIA_TAREA=%s", (id_mod_instancia,))
                    bd.commit()
                    self.panelListaModificaciones.getGrilla().DeleteRows(self.panelListaModificaciones.getGrilla().GetGridCursorRow(), 1)
                    self.GetParent().GetParent().GetStatusBar().Hide()
                except MySQLdb.OperationalError as error: 
                        print error[1] #La fecha de inicio no puede ser menor al presente.
                        self.GetParent().GetParent().GetStatusBar().Show()
                        self.GetParent().GetParent().GetStatusBar().SetStatusText(error[1])
            else: #
                self.panelListaModificaciones.getGrilla().DeleteRows(self.panelListaModificaciones.getGrilla().GetGridCursorRow())


    def actualizarVista(self, event):
        fechaDesde = self.panelListaModificaciones.getGrilla().GetCellValue(event.GetRow(), 2)
        if len(fechaDesde) > 0:
            self.controlHora.setValue(self.panelListaModificaciones.getGrilla().GetCellValue(event.GetRow(), 0))
            self.lstListaConfigAlimento.SetValue(self.panelListaModificaciones.getGrilla().GetCellValue(event.GetRow(), 1))
            fecha = map(int, fechaDesde.split("/"))
            fechaDesde = wx.DateTimeFromDMY(fecha[0], fecha[1]-1, fecha[2])
            fechaHasta = self.panelListaModificaciones.getGrilla().GetCellValue(event.GetRow(), 3)
            fecha = map(int, fechaHasta.split("/"))
            fechaHasta = wx.DateTimeFromDMY(fecha[0], fecha[1]-1, fecha[2])
            self.panelDesdeHasta.fechaDesde.SetValue(fechaDesde)
            self.panelDesdeHasta.fechaHasta.SetValue(fechaHasta)




class GUIRepeticionTareaSimpleEditMode(GUIRepeticionTareaSimple):
    '''En modo edición, se recibe un Id de tarea configurada en la BD, se crean 
    los paneles de Excepciones y/o de Modificaciones.'''
    def __init__(self, parent, tarea, title=u"Edición de tarea programada"):
        GUIRepeticionTareaSimple.__init__(self, parent, None, True, tarea)
        self.Title = title
        #GUIRepeticionTareaSimple.Title= "ds"
        #self.tarea_programada = tarea
        #seleccionar el alimento
        self.cargarDatosPanelTareaSimple()
        self.cargarDatosPanelExcepciones()
        self.cargarDatosPanelModificaciones()

    def cargarDatosPanelTareaSimple(self):
        try:
            cur = self.getBd().cursor()
            cur.execute("SELECT CONFIG_TAREAS_PROGRAMADAS.idCONFIG_TAREAS_PROGRAMADA, "+
                "CONFIG_TAREAS_PROGRAMADAS.Id_Compuerta, CONFIG_TAREAS_PROGRAMADAS.Hora_Ejecucion, " +
                "CONFIG_TAREAS_PROGRAMADAS.Fecha_Inicio, CONFIG_TAREAS_PROGRAMADAS.Fecha_Fin, " +
                "CONFIG_TAREAS_PROGRAMADAS.Para_Siempre, " +
                "CONCAT(CONFIG_PARAMETROS_KILOS_ALIMENTOS.Kg, ' kg de ', ALIMENTOS.Nombre) as 'Config_Alimento'" +
                "FROM CONFIG_TAREAS_PROGRAMADAS inner join CONFIG_PARAMETROS_KILOS_ALIMENTOS on " +
                "CONFIG_TAREAS_PROGRAMADAS.Config_Kg = CONFIG_PARAMETROS_KILOS_ALIMENTOS.idCONFIG_PARAMETROS_KILOS " +
                "INNER JOIN ALIMENTOS ON CONFIG_PARAMETROS_KILOS_ALIMENTOS.Tipo_alimento= ALIMENTOS.id_ALIMENTO " +
                "where CONFIG_TAREAS_PROGRAMADAS.idCONFIG_TAREAS_PROGRAMADA=%s", (self.tarea_programada,))
    
            #self.panelEntradaDatos.lstListaAlimentos.Select(self.panelEntradaDatos.lstListaAlimentos.FindString(str("5")))
            res = cur.fetchall()
            compuerta = res[0][1]
            config_alimento = res[0][6]
            self.panelEntradaDatos.txtCompuerta.SetValue(str(compuerta))
            self.panelEntradaDatos.lstListaAlimentos.Select(self.panelEntradaDatos.lstListaAlimentos.FindString(str(config_alimento)))
            self.panelEntradaDatos.ctrlHora.setValue(str(res[0][2]))
            fecha = map(int, str(res[0][3]).split("-")) #Devuelve año-mes-dia
            fechaDesde = wx.DateTimeFromDMY(fecha[2], fecha[1]-1, fecha[0])
            self.panelRepeticionTareaSimple.panelDesdeHasta.fechaDesde.SetValue(fechaDesde)
            self.panelRepeticionTareaSimple.panelDesdeHasta.fechaDesde.Disable()
            if res[0][5] == True: #Para_siempre
                self.panelRepeticionTareaSimple.panelDesdeHasta.cmbHasta.Select(1)
                self.panelRepeticionTareaSimple.panelDesdeHasta.fechaHasta.Hide()
            else:
                fecha = map(int, str(res[0][4]).split("-")) #Devuelve año-mes-dia
                fechaHasta = wx.DateTimeFromDMY(fecha[2], fecha[1]-1, fecha[0])
                self.panelRepeticionTareaSimple.panelDesdeHasta.fechaHasta.SetValue(fechaHasta)
        except IndexError as error:
            print error, ".La tarea programada no existe."


    def cargarDatosPanelExcepciones(self):
        cur = self.getBd().cursor()
        cur.execute("SELECT EXCEPCIONES_INSTANCIAS_TAREAS.idEXCEPCIONES_INSTANCIA_TAREA, "+
                    "EXCEPCIONES_INSTANCIAS_TAREAS.Fecha_Inicio, "+
                    "EXCEPCIONES_INSTANCIAS_TAREAS.Fecha_Fin " +
                    "FROM EXCEPCIONES_INSTANCIAS_TAREAS " +
                    "where EXCEPCIONES_INSTANCIAS_TAREAS.Id_Config_Tarea=%s "+
                    "and EXCEPCIONES_INSTANCIAS_TAREAS.Cancelada=0 "+
                    "order by EXCEPCIONES_INSTANCIAS_TAREAS.idEXCEPCIONES_INSTANCIA_TAREA", (self.tarea_programada,))

        res = cur.fetchall()

        for val in res:
            fechaIni = val[1].strftime("%d/%m/%Y")
            fechaFin = val[2].strftime("%d/%m/%Y")
            self.panelExcepciones.panelListaExcepciones.agregarRegistro([fechaIni, fechaFin, str(val[0])])
            #self.panelExcepciones.panelListaExcepciones.getGrilla().SetRowLabelValue(res.index(val), str(val[0]))
            #self.panelExcepciones.panelListaExcepciones.getGrilla().HideCol(2)
            

    def cargarDatosPanelModificaciones(self):
        cur = self.getBd().cursor()
        cur.execute("SELECT MODIFICACION_INSTANCIAS_TAREAS.idMODIFICACION_INSTANCIA_TAREA, " +
                    "MODIFICACION_INSTANCIAS_TAREAS.Hora_Ejecucion, " + 
                    "MODIFICACION_INSTANCIAS_TAREAS.Fecha_Inicio, " +
                    "MODIFICACION_INSTANCIAS_TAREAS.Fecha_Fin, " +   
                    "CONCAT(CONFIG_PARAMETROS_KILOS_ALIMENTOS.Kg, ' kg de ', ALIMENTOS.Nombre) as 'Config_Alimento' "+
                    "FROM MODIFICACION_INSTANCIAS_TAREAS " +
                    "INNER JOIN CONFIG_PARAMETROS_KILOS_ALIMENTOS " +
                    "ON MODIFICACION_INSTANCIAS_TAREAS.Config_Kg = CONFIG_PARAMETROS_KILOS_ALIMENTOS.idCONFIG_PARAMETROS_KILOS "+
                    "INNER JOIN ALIMENTOS ON CONFIG_PARAMETROS_KILOS_ALIMENTOS.Tipo_alimento=ALIMENTOS.id_ALIMENTO "+
                    "WHERE MODIFICACION_INSTANCIAS_TAREAS.Id_Config_Tarea=%s "+
                    "and MODIFICACION_INSTANCIAS_TAREAS.Cancelada=False "+
                    "order by MODIFICACION_INSTANCIAS_TAREAS.idMODIFICACION_INSTANCIA_TAREA", (self.tarea_programada,))
        
        res = cur.fetchall()
        for val in res:
            if int(str(val[1])[0:str(val[1]).find(":")]) in range(0, 9): #Si la hora esta entre las 0 y 9, agrego un cero delante
                hora = str("0"+str(val[1]))
            else:
                hora= val[1]
                
            config_kg=val[4]
            fechaIni = val[2].strftime("%d/%m/%Y")
            fechaFin = val[3].strftime("%d/%m/%Y")
            #str(int(str(val[0])[0:str(val[0]).find("-")]))
            self.panelModificacionesInstancias.panelListaModificaciones.agregarRegistro([hora, config_kg, fechaIni, fechaFin, str(val[0])])
            #self.panelModificacionesInstancias.panelListaModificaciones.getGrilla().SetRowLabelValue(res.index(val), str(val[0]))
            #self.panelModificacionesInstancias.panelListaModificaciones.getGrilla().HideCol(4)
        
        #print self.panelModificacionesInstancias.panelListaModificaciones.getGrilla().GetCellValue(1, 4)
        
        
        
'''
app = wx.App()
ventana = GUIRepeticionTareaSimple(None, 3)
ventana.ShowModal()
ventana.Destroy()
app.MainLoop()
'''