# coding= ISO-8859-1
'''
Created on 3 dic. 2017

@author: diegoariel

Este cuadro de di�logo permite configurar la hora del servidor...
La hora se puede establecer de 3 maneras:

1) Si el servidor dispone de conexi�n a internet o a una intranet que tiene 
un servidor NTP, se muestra la opcion actualizar por la red, caso contrario no.
Estaoci�n se habilita din�micamente, si el servidor tiene conexi�n a internet.
Si se elije esta opci�n, el servidor lo hace autom�ticamente el servidor al 
arrancar la m�quina. Pero por medio de un boton, tambi�n es posible hacer que el 
servidor actualice la hora del servidor cuando se requiera. 
La GUI debe permitir en este caso, ingresar el servidor de red desde d�nde desea 
actualizar la hora.

2) Si el servidor no dispone de conexi�n a internet, pero est� en red lan, 
es posible copiar la configuraci�n de fecha y hora de la m�quina que ejecuta 
el cliente o manualmente. Dicha m�quina debe tener la hora y fecha correctamente 
configurada.
La GUI debe tener un boton que permita enviar la fecha y hora del cliente al 
servidor.
Cuando el cliente se conecta al servidor, se podr�an revizar estos datos y si 
no concuerdan, preguntar si desea actualizar dichos datos. 
�Qu� pasa si se conecta mas de un cliente?...�Permisos por IP, o por usuario?

3) Si no se dispone de conexi�n a internet ni el cliente tiene el horario 
correctaente configurado, es posible establecer la fecha y hora manualmente.
�Qu� pasa si se conecta mas de un cliente?...�Permisos por IP, o por usuario?

Esta configuraci�n se debe establecer por el usuario. Se debe guardar en un archivo
de base de datos que el servidor pueda leer, sea el punto 1, 2 o 3.

En la interfaz del programa cliente, se debe mostrar los datos de la fecha y hora
del servidor.
'''

import wx, datetime
from src.widgets import widget
from src.widgets.widget import Widget
import MySQLdb
import time
from datetime import datetime


class GUIConfigurarFechaYHora(wx.Dialog, Widget):
    def __init__(self, parent, mediador):
            size=(470, 250)
            #wx.Frame.__init__(self, None, wx.ID_ANY, u'ConfiguraciÃ³n de alertas', style=style)
            wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = 'Configuracion de fecha y hora', pos = wx.DefaultPosition, size = wx.Size( 650, 271 ), style = wx.DEFAULT_DIALOG_STYLE )
            self.tamanoCuadroTexto=25
            self.mediador = mediador
            self.db= MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')
            self.cursor = self.db.cursor()
            self.idConfigInternet=None
            self.idConfigServidorActivoNTP=None
            self.crearGUI()
    
    def crearGUI(self):
        self.mainSizer = wx.BoxSizer(wx.VERTICAL) 
        lblList = ['Hora desde internet', 'Hora de mi ordenador', 'Establecer hora manualmente']     
        self.rbox = wx.RadioBox(self,label = 'Opciones de configuracion', choices = lblList ,
           majorDimension = 1, style = wx.RA_SPECIFY_ROWS)
        self.rbox.Bind(wx.EVT_RADIOBOX,self.onRadioBox)
        self.mainSizer.Add(self.rbox)
        self.SetSizerAndFit(self.mainSizer)
        '''Si el servidor tiene conexi�n a internet, panelHoraInternet se habilita
        autom�ticamente. Caso contrario, solo se muestran las otras opciones.'''
        self.panelHoraInternet = wx.Panel(self, -1)
        self.txtServidorRed = wx.ComboBox(self.panelHoraInternet, -1, size=(200, -1))
        self.txtServidorRed.Bind(wx.EVT_COMBOBOX, self.comboBoxServidorNTPChange)
        #self.txtServidorRed.AppendItems(["pool.ntp.org"])
        self.txtServidorRed.Select(0)
        self.btnAgregarServidorNTP = wx.Button(self.panelHoraInternet, -1, "Agregar servidor")
        self.btnAgregarServidorNTP.Bind(wx.EVT_BUTTON, self.agregarServidorNTP)
        lblServidorRed = wx.StaticText(self.panelHoraInternet, -1, "Ingrese el servidor de red:")
        self.chkActualizarAutomaticamente = wx.CheckBox(self.panelHoraInternet, -1, "Actualizar automaticamente")
        #self.btnCambiarHora = wx.Button(self.panelHoraInternet, -1, "Actualizar hora")
        sizerHoraInternet = wx.GridBagSizer(2,2)
        sizerHoraInternet.Add(lblServidorRed, pos=(0, 0), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizerHoraInternet.Add(self.txtServidorRed, pos=(0, 1),flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizerHoraInternet.Add(self.btnAgregarServidorNTP, pos=(0, 2),flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizerHoraInternet.Add(self.chkActualizarAutomaticamente, pos=(1, 0), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        #sizerHoraInternet.Add(self.btnCambiarHora, pos=(2,0), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        self.panelHoraInternet.SetSizer(sizerHoraInternet)
        
        self.mainSizer.Add(self.panelHoraInternet, 0, wx.EXPAND, 5)
        self.panelHoraInternet.Hide()
        
        self.panelHoraOrdenador = wx.Panel(self, -1)
        self.lblFecha = wx.StaticText(self.panelHoraOrdenador, -1, "Fecha y hora actual: " + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.actualizarTiempo, self.timer)
        #btnEstablecerHora = wx.Button(self.panelHoraOrdenador, -1, "Establecer hora")
        sizerHoraOrdenador = wx.BoxSizer(wx.VERTICAL)
        sizerHoraOrdenador.Add(self.lblFecha, 0, wx.ALL|wx.ALIGN_LEFT, 5)
        #sizerHoraOrdenador.Add(btnEstablecerHora, 0, wx.ALL|wx.ALIGN_LEFT, 5)
        self.panelHoraOrdenador.SetSizer(sizerHoraOrdenador)
        self.panelHoraOrdenador.Hide()
        self.mainSizer.Add(self.panelHoraOrdenador, 0, wx.EXPAND, 5)
        
        self.panelHoraManual = wx.Panel(self, -1)
        lblIngreseFecha = wx.StaticText(self.panelHoraManual, -1, "Ingrese la fecha:")
        self.fechaManual = widget.DatePickerCtrlV2(self.panelHoraManual)
        lblHoraManual = wx.StaticText(self.panelHoraManual, -1, "Ingrese la hora:")
        self.controlHora = widget.TimerCtrl(self.panelHoraManual)
        #btnAceptar = wx.Button(self.panelHoraManual, -1, "Guardar")
        sizerHoraManual = wx.GridBagSizer(2,2)
        sizerHoraManual.Add(lblIngreseFecha, pos=(0, 0), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizerHoraManual.Add(self.fechaManual, pos=(0, 1), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizerHoraManual.Add(lblHoraManual, pos=(1, 0), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizerHoraManual.Add(self.controlHora, pos=(1, 1), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        #sizerHoraManual.Add(btnAceptar, pos=(2, 0), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        
        self.panelHoraManual.SetSizer(sizerHoraManual)
        self.panelHoraManual.Hide()
        self.mainSizer.Add(self.panelHoraManual, 0, wx.EXPAND, 5)
        
        self.panelBotonera = wx.Panel(self, -1)
        self.btnAceptar = wx.Button(self.panelBotonera, wx.ID_OK, "Aceptar")
        self.btnAceptar.SetDefault()
        self.btnAceptar.Bind(wx.EVT_BUTTON, self.procesarHora)
        self.btnCancelar = wx.Button(self.panelBotonera, wx.ID_CANCEL, "Cancelar")
        sizerBotonera = wx.BoxSizer(wx.HORIZONTAL)
        sizerBotonera.Add(self.btnCancelar, 0, wx.ALL|wx.EXPAND|wx.ALIGN_RIGHT, 5)
        sizerBotonera.Add(self.btnAceptar, 0, wx.ALL|wx.EXPAND|wx.ALIGN_RIGHT, 5)
        self.panelBotonera.SetSizer(sizerBotonera)
        
        self.mainSizer.Add(self.panelBotonera, 0, wx.ALIGN_RIGHT, 5)
    
    def onRadioBox(self,e): 
        print self.rbox.GetStringSelection(),' is clicked from Radio Box'
        if self.rbox.GetStringSelection() == "Hora desde internet":
            if self.txtServidorRed.GetCount() == 0:
                self.cargarDatosPanelInternet()
            self.panelHoraOrdenador.Hide()
            self.panelHoraManual.Hide()
            self.panelHoraInternet.Show()
            self.timer.Stop()
            self.Fit()
            self.Layout()
        elif self.rbox.GetStringSelection() == "Hora de mi ordenador":
            self.panelHoraInternet.Hide()
            self.panelHoraManual.Hide()
            self.panelHoraOrdenador.Show()
            self.timer.Start(1000)
            self.Fit()
            self.Layout()
        elif self.rbox.GetStringSelection() == "Establecer hora manualmente":
            self.panelHoraOrdenador.Hide()
            self.panelHoraInternet.Hide()
            self.panelHoraManual.Show()
            self.timer.Stop()
            self.Fit()
            self.Layout()
            
    
    def actualizarTiempo(self, event):
        Fecha = datetime.now()
        self.lblFecha.SetLabel("Fecha y hora actual: " + Fecha.strftime("%d/%m/%Y %H:%M:%S"))
        print Fecha.strftime("%d/%m/%Y %H:%M:%S")
        
        
    def procesarHora(self, event):
        if self.panelHoraInternet.IsShown():
            '''Se guarda en la BD el servidor y se envia el comando para que se realice la actualizacion
            Se guarda opcionalmente la posibilidad de actualizar automaticamente
            '''
            print "Panel hora internet"
            cur = self.db.cursor()
            #cur.execute("insert into CONFIGURACION_DATETIME (ServidorNTP, Actualizar_Automaticamente, Activa) values(%s, %s, %s) ", (self.txtServidorRed.Value, self.chkActualizarAutomaticamente.Value, 1))
            #cur.execute("update CONFIGURACION_DATETIME set ServidorNTP ='ddf'  Where Activa=1")
            cur.execute("update CONFIGURACION_DATETIME set ServidorNTP =%s, Actualizar_Automaticamente=%s, Activa=1 Where idCONFIGURACION_DATETIME=%s", 
                        (''+self.txtServidorRed.Value+'', self.chkActualizarAutomaticamente.Value, + self.idConfigInternet))
            #self.db.autocommit(True)
            if self.idConfigServidorActivoNTP != self.idConfigInternet:
                cur.execute("update CONFIGURACION_DATETIME set Activa=0 Where idCONFIGURACION_DATETIME=%s", (self.idConfigServidorActivoNTP,))
            cur.connection.commit()
            #cur.execute()
            self.notificarCambios(event)
            self.Show(False)
        if self.panelHoraOrdenador.IsShown():
            print "Panel hora ordenador"
            self.notificarCambios(event)
            self.Show(False)
        if self.panelHoraManual.IsShown():
            print "Panel hora manual"
            self.notificarCambios(event)
            self.Show(False)
            
    def cargarDatosPanelInternet(self):
        '''Se cargan todos los servidores NTP a la lista combobox, 
        pero el que se muestra en pantalla es el �nico registro activo de la BD.
        '''
        cur = self.db.cursor()
        cur.execute("Select idCONFIGURACION_DATETIME, ServidorNTP from CONFIGURACION_DATETIME")
        
        for lista_servidores in cur:
            self.txtServidorRed.Append(lista_servidores[1], lista_servidores[0])
        #Solo cargo la configuraci�n activa.
        cur.execute("Select * from CONFIGURACION_DATETIME Where Activa=1")
         
        for config_activa in cur:
            self.txtServidorRed.Select(self.txtServidorRed.GetItems().index(config_activa[1])) #Selecciono el servidor activo
            self.chkActualizarAutomaticamente.SetValue(config_activa[2])
            self.idConfigInternet = config_activa[0]
            self.idConfigServidorActivoNTP = self.idConfigInternet
            
        #self.txtServidorRed.Select(0)
        
        
    def comboBoxServidorNTPChange(self, event):
        self.idConfigInternet = self.txtServidorRed.GetClientData(self.txtServidorRed.GetSelection())
        #self.idConfigServidorActivoNTP = self.idConfigInternet
        
    def agregarServidorNTP(self, event):
        dlg = wx.TextEntryDialog(self,
                "Ingresar el nombre o el IP del servidor",
                "Nuevo servidor NTP", style=wx.OK | wx.CANCEL )
        if dlg.ShowModal() == wx.ID_OK:
            self.nuevoServerSMTP =dlg.GetValue()
            cur = self.db.cursor()
            cur.execute("insert into CONFIGURACION_DATETIME (ServidorNTP) values(%s)", (self.nuevoServerSMTP,))
            self.idConfigInternet = cur.lastrowid #la configuracion de servidor que se muestra en pantalla es la insertada por el usuario
            self.txtServidorRed.Value = self.nuevoServerSMTP
        dlg.Destroy()
        
    
    def getFechaHoraManual(self):
        fecha = self.fechaManual.GetValue()
        hora = self.controlHora.GetValue()
        return fecha + " " + hora

    
    def notificarCambios(self, evento):
        #Se tiene que enviar notificaciones del evento al mediador...
        if self.panelHoraInternet.IsShown():
            self.mediador.widgetModificado(self, evento, "Actualizar_Fecha_Hora")
        if self.panelHoraOrdenador.IsShown():
            self.mediador.widgetModificado(self, evento, "Actualizacion_Ordenador")
        if self.panelHoraManual.IsShown():
            self.mediador.widgetModificado(self, evento, "Actualizacion_Manual")
        

'''
app = wx.App()
frame = GUIConfigurarFechaYHora(None, None)
frame.ShowModal()
frame.Destroy()
#app.MainLoop()
'''

