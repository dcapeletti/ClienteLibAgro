# encoding=utf-8
'''
Created on 12 mar. 2018

@author: diegoariel

En esta vista, se creará una clase para un panel de Config de compuerta
y otro panel (en una clase distinta) para tareas programadas. Cada panel
es independiente por lo que puede dibujarse como quiera.

'''

import wx
import wx.grid
from wx.grid import *
import locale
import MySQLdb
import datetime
from datetime import date
from src.dialogs.GUITareasProgramadasSimple import GUIRepeticionTareaSimpleEditMode,\
    GUIRepeticionTareaSimple
from src.widgets.widget import Widget, DatePickerCtrlV2
import wx.calendar

class GUIConfigCompuerta(wx.Dialog, Widget):
    def __init__(self, parent, compuerta, notebookTab=None, title=None, mediador=None):
        wx.Dialog.__init__(self, parent)
        self.Title = u"Configuración general de compuerta " + str(compuerta)
        self.mediador = mediador
        notebook = wx.Notebook(self, -1)
        
        locale.setlocale(locale.LC_ALL, ('es_AR', 'UTF8')) #http://zetcode.com/wxpython/in18/       
        self.__bd = MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')
        
        self.panelConfigCompuerta = PanelConfigCompuerta(notebook)
        self.panelConfigTareasProgramadas = PanelConfigTareasProgramadas(notebook)
        
        notebook.AddPage(self.panelConfigCompuerta, "Configuración de compuerta")
        notebook.AddPage(self.panelConfigTareasProgramadas, "Configuración de tareas programadas")
        
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(notebook, 0, wx.EXPAND|wx.ALL)
        
        if notebookTab != None:
            notebook.ChangeSelection(notebookTab)
            #También es posible ocultar el resto de paneles...
            
        self.panelConfigTareasProgramadas.SetCompuerta(compuerta)
        self.panelConfigTareasProgramadas.filtrarTareasCompuerta(None)
        
        btnAceptar = wx.Button(self, wx.ID_OK, "Aceptar")
        btnCancelar = wx.Button(self, wx.ID_CANCEL, "Cancelar")
        btnAceptar.SetName("btnActualizarTareasProgramadas")
        btnAceptar.Bind(wx.EVT_BUTTON, self.procesarDatos)
        
        sizerBotonera = wx.BoxSizer(wx.HORIZONTAL)
        sizerBotonera.Add(btnCancelar)
        sizerBotonera.Add(btnAceptar)
        
        mainSizer.Add(sizerBotonera, 0, wx.ALIGN_RIGHT)
        self.SetSizerAndFit(mainSizer)
        #self.SetSize((400, 400))
        
    def getBd(self):
        return self.__bd
    
    def procesarDatos(self, event):
        self.notificarCambios(event)
        wx.EndBusyCursor()
        self.Destroy()
    
    def notificarCambios(self, evento):
        self.mediador.widgetModificado(self, evento)
        
    
class PanelConfigTareasProgramadas(wx.Panel):
    '''Este panel permite al usuario, agregar nuevas tareas programadas, 
    editar las mismas, filtrar las mismas, ver las tareas a ejecutarse
    en la fecha actual, etc.
    '''
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        lblFiltro = wx.StaticText(self, -1, "Seleccione un filtro:")
        self.__cmbListaFiltros=wx.ComboBox(self, -1)
        self.__cmbListaFiltros.Bind(wx.EVT_COMBOBOX, self.seleccionarPanel)
        self.__cmbListaFiltros.Append("Tareas del día de hoy", 0)
        self.__cmbListaFiltros.Append("Tareas canceladas", 1)
        self.__cmbListaFiltros.Append("Tareas completadas", 2)
        self.__cmbListaFiltros.Append("Tareas no completadas", 3)
        self.__cmbListaFiltros.Append("Todas las tareas", 4)
        self.__cmbListaFiltros.Select(0)
        self.__cmbListaFiltros.SetEditable(False)
        lblEnCompuerta = wx.StaticText(self, -1, "En la compuerta:")
        self.__cmbListaCompuertas = wx.ComboBox(self, -1)
        self.__cmbListaCompuertas.AppendItems(["Todas", "1", "2", "3", "4"]) #Hay que programar el modelo de compuerta
        self.__cmbListaCompuertas.Select(0)
        self.__cmbListaCompuertas.Bind(wx.EVT_COMBOBOX, self.filtrarTareasCompuerta)
        
        sizerFiltro = wx.BoxSizer(wx.HORIZONTAL)
        sizerFiltro.Add(lblFiltro, 0, wx.ALIGN_CENTER_VERTICAL, 5)
        sizerFiltro.Add(self.__cmbListaFiltros, 0, wx.ALIGN_CENTER_VERTICAL, 5)
        sizerFiltro.Add(lblEnCompuerta, 0, wx.ALIGN_CENTER_VERTICAL, 5)
        sizerFiltro.Add(self.__cmbListaCompuertas, 0, wx.ALIGN_CENTER_VERTICAL, 5)
        
        self.sizerDataFiltro = wx.BoxSizer(wx.VERTICAL)
        
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.mainSizer.Add(sizerFiltro)
        self.mainSizer.Add(self.sizerDataFiltro)
        #self.mainSizer.Add(self.crearPanelBotonera())
        
        self.seleccionarPanel(None)
        self.SetSizerAndFit(self.mainSizer)
        
    def SetCompuerta(self, comp):
        self.__cmbListaCompuertas.SetValue(str(comp))
        
    
    def GetCompuerta(self):
        return self.__cmbListaCompuertas.GetValue()    
    
    
    def filtrarTareasCompuerta(self, event):
        '''Dependiendo del panel instalado, se aplica un filtro u otro.
        Si están todos instalados, se aplica el filtro para todos.
        '''
        #print event.GetEventObject().GetValue()
        if self.panelInstalado == self.panelTareasFechaActual:
            lista_tareas = []
            if self.__cmbListaCompuertas.GetValue() == "Todas":
                #lista_tareas = self.cargarTareasDelDia(None)
                lista_tareas = self.cargarTareasDelDiaV2(None)
            else:
                #lista_tareas = self.cargarTareasDelDia(int(self.__cmbListaCompuertas.GetValue()))
                lista_tareas = self.cargarTareasDelDiaV2(int(self.__cmbListaCompuertas.GetValue()))
                
            if self.grid_fecha_actual.GetNumberRows() > 0:
                self.grid_fecha_actual.DeleteRows(0, self.grid_fecha_actual.GetNumberRows(), True)
            for val in lista_tareas:
                self.grid_fecha_actual.AppendRows(1)
                self.grid_fecha_actual.SetCellValue(lista_tareas.index(val), 0, str(val['id_tarea']))
                self.grid_fecha_actual.SetCellValue(lista_tareas.index(val), 1, str(val['compuerta']))
                self.grid_fecha_actual.SetCellValue(lista_tareas.index(val), 2, str(val['hora']))
                self.grid_fecha_actual.SetCellValue(lista_tareas.index(val), 3, str(val['config_kg']))
        elif self.panelInstalado == self.panelTareasEjecutadas:
            '''{'id':res[0], 'config_tarea':res[1], 'compuerta':res[2], 'fecha':res[3], 
                               'hora_ejecucion':res[4]}'''
            if type(event.GetEventObject()) == wx._controls.DatePickerCtrl:
                fecha = event.GetDate().Format("%Y-%m-%d")
                lista_tareas = self.cargarTareasEjecutadas(self.GetCompuerta(), fecha)
            else:
                lista_tareas = self.cargarTareasEjecutadas(self.GetCompuerta(), None)
            
            if self.grilla_tareas_completadas.GetNumberRows() > 0:
                self.grilla_tareas_completadas.DeleteRows(0, self.grilla_tareas_completadas.GetNumberRows(), True)
            for val in lista_tareas:
                self.grilla_tareas_completadas.AppendRows(1)
                self.grilla_tareas_completadas.SetCellValue(lista_tareas.index(val), 0, str(val['id']))
                self.grilla_tareas_completadas.SetCellValue(lista_tareas.index(val), 1, str(val['compuerta']))
                self.grilla_tareas_completadas.SetCellValue(lista_tareas.index(val), 2, str(val['fecha']))
                self.grilla_tareas_completadas.SetCellValue(lista_tareas.index(val), 3, str(val['hora_ejecucion']))
        
            
        
    def seleccionarPanel(self, event):
        val= self.__cmbListaFiltros.GetClientData(self.__cmbListaFiltros.FindString(self.__cmbListaFiltros.Value))
        self.panelInstalado = None
        if val == 0:
            try:
                self.panelTareasCanceladas.Hide()
                #self.panelTareasFechaActual.Hide()
                self.panelTareasEjecutadas.Hide()
            except:
                pass
            self.sizerDataFiltro.Add(self.crearPanelTareasFechaActual())
            self.panelInstalado = self.panelTareasFechaActual
        elif val == 1:
            try:
                self.panelTareasFechaActual.Hide()
                self.panelTareasEjecutadas.Hide()
            except:
                pass
            self.sizerDataFiltro.Add(self.crearPanelTareasCanceladas())
            self.panelInstalado = self.panelTareasCanceladas
        elif val == 2:
            try:
                self.panelTareasFechaActual.Hide()
                self.panelTareasCanceladas.Hide()
            except:
                pass
            self.sizerDataFiltro.Add(self.crearPanelTareasEjecutadas())
            self.panelInstalado = self.panelTareasEjecutadas
        elif val == 3:
            try:
                self.panelTareasCanceladas.Hide()
                self.panelTareasEjecutadas.Hide()
                self.panelTareasFechaActual.Hide()
            except:
                pass
            self.sizerDataFiltro.Add(self.crearPanelTareasNoEjecutadas())
            self.panelInstalado = self.panelTareasNoEjecutadas
            
        #self.mainSizer.Add()    
        self.Fit()
        self.GetParent().Fit()
        self.GetParent().GetParent().Fit()
        
            
            
    def crearPanelTodasLasTareasProgramadas(self):
        self.panelTodasLasTareasProgramadas = wx.Panel(self, -1)
        btn = wx.Button(self.panelTodasLasTareasProgramadas, -1, "Panel todas las tareas")
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(btn)
        
        self.panelTodasLasTareasProgramadas.SetSizerAndFit(sizer)
        return self.panelTodasLasTareasProgramadas
            
        
        
        
    def crearPanelTareasFechaActual(self):
        '''En este panel, se creará una grilla que muestra las tareas de la fecha actual 
        en orden por horario de ejecución.
        El usuario puede fitrar por compuerta si lo desea.
        Además, el usuario puede cancelar una tarea que se va a ejecutar, editarla o agregar nuevas tareas.'''
        self.panelTareasFechaActual = wx.Panel(self, -1)
        self.grid_fecha_actual = Grid(self.panelTareasFechaActual, -1, size=(550, 150))
        self.grid_fecha_actual.EnableEditing(False)
        
        #Lista de columnas
        lista_cols = ["Id_Tarea", "Compuerta", "Hora", "Config_Alimento"]
        
        # creamos las filas y columnas
        self.grid_fecha_actual.CreateGrid(0,len(lista_cols))

        # definimos los campos que contiene el gridExcepciones
        for col in lista_cols:
            self.grid_fecha_actual.SetColLabelValue(lista_cols.index(col), col)
        
        self.grid_fecha_actual.SetColSize(2, 140)
        self.grid_fecha_actual.SetColSize(3, 200)

        lista_tareas=[]
        if self.__cmbListaCompuertas.GetValue() != "Todas":
            #lista_tareas = self.cargarTareasDelDia(self.__cmbListaCompuertas.GetValue())
            lista_tareas = self.cargarTareasDelDiaV2(self.__cmbListaCompuertas.GetValue())
        else:
            #lista_tareas = self.cargarTareasDelDia(None)
            lista_tareas = self.cargarTareasDelDiaV2(None)
        
        '''{'id_tarea': val[1], 'compuerta': val[2], 'fecha': val[3], 'hora': val[4], 'config_kg': val[5]}'''
        for val in lista_tareas:
            self.grid_fecha_actual.AppendRows(1)
            self.grid_fecha_actual.SetCellValue(lista_tareas.index(val), 0, str(val['id_tarea']))
            self.grid_fecha_actual.SetCellValue(lista_tareas.index(val), 1, str(val['compuerta']))
            self.grid_fecha_actual.SetCellValue(lista_tareas.index(val), 2, str(val['hora']))
            self.grid_fecha_actual.SetCellValue(lista_tareas.index(val), 3, str(val['config_kg']))
        
        #self.grid_fecha_actual.SetCellBackgroundColour(2, 2, (255, 0, 0))
        #self.grid_fecha_actual.ForceRefresh()
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.grid_fecha_actual)
        
        btnAceptar = wx.Button(self.panelTareasFechaActual, -1, "Crear nueva tarea")
        btnAceptar.Bind(wx.EVT_BUTTON, self.crearNuevaTarea)
        btnEditar = wx.Button(self.panelTareasFechaActual, -1, "Editar tarea")
        btnEditar.Bind(wx.EVT_BUTTON, self.editarTarea)
        btnEliminar = wx.Button(self.panelTareasFechaActual, -1, "Eliminar tarea")
        btnEliminar.Bind(wx.EVT_BUTTON, self.eliminarTarea)
        
        sizerBotonera = wx.BoxSizer(wx.HORIZONTAL)
        sizerBotonera.Add(btnAceptar)
        sizerBotonera.Add(btnEditar)
        sizerBotonera.Add(btnEliminar)        
        
        sizer.Add(sizerBotonera, 0, wx.EXPAND, 5)
                       
        self.panelTareasFechaActual.SetSizerAndFit(sizer)
        return self.panelTareasFechaActual
    
    
    def crearPanelTareasCanceladas(self):
        '''Crea un panel con una grilla para mostrar las tareas que fueron canceladas.
        Con un boton puede reanudar la tarea, siempre y cuando la misma no este en el pasado.'''
        self.panelTareasCanceladas = wx.Panel(self, -1)
        self.grid_tareas_canceladas = Grid(self.panelTareasCanceladas, -1, size=(550, 150))
        self.grid_tareas_canceladas.EnableEditing(False)
        
        #Lista de columnas
        lista_cols = ["Id Tarea", "Compuerta", "Fecha inicio", "Fecha fin", "Hora", "Para siempre", "Config_Alimento", "Fecha cancelación"]
        
        # creamos las filas y columnas
        self.grid_tareas_canceladas.CreateGrid(0,len(lista_cols))
        
        # definimos los campos que contiene el grid
        for col in lista_cols:
            self.grid_tareas_canceladas.SetColLabelValue(lista_cols.index(col), col)
        
        #self.grid_tareas_canceladas.SetColSize(2, 140)
        self.grid_tareas_canceladas.SetColSize(6, 150)
        self.grid_tareas_canceladas.SetColSize(7, 150)
        
        lista_tareas = self.cargarTareasCanceladas(None)
        
        for val in lista_tareas:
            self.grid_tareas_canceladas.AppendRows(1)
            self.grid_tareas_canceladas.SetCellValue(lista_tareas.index(val), 0, str(val['id']))
            self.grid_tareas_canceladas.SetCellValue(lista_tareas.index(val), 1, str(val['compuerta']))
            self.grid_tareas_canceladas.SetCellValue(lista_tareas.index(val), 2, str(val['fecha_ini']))
            self.grid_tareas_canceladas.SetCellValue(lista_tareas.index(val), 3, str(val['fecha_fin']))
            self.grid_tareas_canceladas.SetCellValue(lista_tareas.index(val), 4, str(val['hora']))
            self.grid_tareas_canceladas.SetCellValue(lista_tareas.index(val), 5, str(val['para_siempre']))
            self.grid_tareas_canceladas.SetCellValue(lista_tareas.index(val), 6, str(val['config_kg']))
            self.grid_tareas_canceladas.SetCellValue(lista_tareas.index(val), 7, str(val['fecha_cancelacion']))
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.grid_tareas_canceladas)
        
        btnReanudarTarea = wx.Button(self.panelTareasCanceladas, -1, "Reanudar tarea")
        btnReanudarTarea.SetName("btnActualizarTareasProgramadas")
        btnReanudarTarea.Bind(wx.EVT_BUTTON, self.reanudarTarea)
        
        sizer.Add(btnReanudarTarea)
        
        self.panelTareasCanceladas.SetSizerAndFit(sizer)
        return self.panelTareasCanceladas
    
    
    def crearPanelTareasEjecutadas(self):
        '''En este panel, se listan las tareas que fueron ejecutadas con éxito.
        '''
        self.panelTareasEjecutadas = wx.Panel(self, -1)
        self.grilla_tareas_completadas = Grid(self.panelTareasEjecutadas, -1, size=(550, 150))
        self.grilla_tareas_completadas.EnableEditing(False)
        
        #Lista de columnas
        lista_cols = ["Id Tarea", "Compuerta", "Fecha ejecución", "Hora", "Config_Alimento"]
        
        # creamos las filas y columnas
        self.grilla_tareas_completadas.CreateGrid(0,len(lista_cols))
        
        # definimos los campos que contiene el grid
        for col in lista_cols:
            self.grilla_tareas_completadas.SetColLabelValue(lista_cols.index(col), col)
        
        #self.grid_tareas_canceladas.SetColSize(2, 140)
        self.grilla_tareas_completadas.SetColSize(2, 150)
        self.grilla_tareas_completadas.SetColSize(4, 150)
        
        lblFiltroFecha = wx.StaticText(self.panelTareasEjecutadas, -1, "Seleccione una fecha:")
        txtFiltroFecha = DatePickerCtrlV2(self.panelTareasEjecutadas)
        txtFiltroFecha.Bind(wx.calendar.EVT_CALENDAR_SEL_CHANGED, self.filtrarTareasCompuerta)
        
        sizerFiltroFecha = wx.BoxSizer(wx.HORIZONTAL)
        sizerFiltroFecha.Add(lblFiltroFecha, 0, wx.ALIGN_CENTER_VERTICAL)
        sizerFiltroFecha.Add(txtFiltroFecha)
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(sizerFiltroFecha)
        sizer.Add(self.grilla_tareas_completadas)
        
        lista_tareas_ejecutadas=[]
        if self.GetCompuerta() == "Todas":
            lista_tareas_ejecutadas = self.cargarTareasEjecutadas(None, None)
        else:        
            lista_tareas_ejecutadas = self.cargarTareasEjecutadas(self.GetCompuerta(), None)
        '''    
        {'id':res[0], 'compuerta':res[2], 'fecha':res[3], 'hora_ejecucion':res[4], 
                               'config_kg': res[5]}
        '''
        
        for val in lista_tareas_ejecutadas:
            self.grilla_tareas_completadas.AppendRows(1)
            self.grilla_tareas_completadas.SetCellValue(lista_tareas_ejecutadas.index(val), 0, str(val['id']))
            self.grilla_tareas_completadas.SetCellValue(lista_tareas_ejecutadas.index(val), 1, str(val['compuerta']))
            self.grilla_tareas_completadas.SetCellValue(lista_tareas_ejecutadas.index(val), 2, str(val['fecha']))
            self.grilla_tareas_completadas.SetCellValue(lista_tareas_ejecutadas.index(val), 3, str(val['hora_ejecucion']))
            self.grilla_tareas_completadas.SetCellValue(lista_tareas_ejecutadas.index(val), 4, str(val['config_kg']))
        
        self.panelTareasEjecutadas.SetSizerAndFit(sizer)
        
        return self.panelTareasEjecutadas
    
    
    def crearPanelTareasNoEjecutadas(self):
        '''En este panel, se listan las tareas que no pudieron ser ejecutadas por algún motivo:
        Corte de suministro eléctrico antes de la ejecución, problemas con el Software, etc.         
        Estas tareas se notifican por email al/los usuarios (si está configurado) 
        cuando el suministro eléctrico o la red comienzan a responder. 
        
        ¿Cómo se calculan las tareas no ejecutadas?
        Se podrían obtener de la diferencia de:
        Tareas no ejecutadas = Tareas programadas - tareas ejecutadas.
        
        ¿Cuándo se envía el aviso de las tareas no ejecutadas?
        Puede haber un servicio que hace un chequeo cada 5 minuto del día (288 chequeos diarios). 
        Cuando la máquina restaura la energía eléctrica, se hace el chequeo y se envían las notificaciones
        al email configurado. De todas formas si no hay un email configurado, queda un registro en la BD 
        con las tareas no ejecutadas. Si el aviso no fue notificado vía email, el programa cliente debe 
        avisar de dicha situación.
        '''
        self.panelTareasNoEjecutadas = wx.Panel(self, -1)
        self.grid_tareas_no_ejecutadas = Grid(self.panelTareasNoEjecutadas, -1, size=(550, 150))
        self.grid_tareas_no_ejecutadas.EnableEditing(False)
        
        #Lista de columnas
        lista_cols = ["Id Tarea", "Compuerta", "Fecha ejecución", "Hora", "Config_Alimento", "Notificada"]
        
        # creamos las filas y columnas
        self.grid_tareas_no_ejecutadas.CreateGrid(0,len(lista_cols))
        
        # definimos los campos que contiene el grid
        for col in lista_cols:
            self.grid_tareas_no_ejecutadas.SetColLabelValue(lista_cols.index(col), col)
        
        #self.grid_tareas_canceladas.SetColSize(2, 140)
        self.grid_tareas_no_ejecutadas.SetColSize(2, 150)
        self.grid_tareas_no_ejecutadas.SetColSize(4, 150)
        
        btnConfigurarNotificacionesEmail = wx.Button(self.panelTareasNoEjecutadas, -1, "Enviar notificaciones por email")
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.grid_tareas_no_ejecutadas)
        sizer.Add(btnConfigurarNotificacionesEmail)
        
        self.panelTareasNoEjecutadas.SetSizerAndFit(sizer)
        
        return self.panelTareasNoEjecutadas
        
    
    
    def cargarTareasDelDia(self, compuerta=None):
        '''Busco las tareas del dia por consulta. Busco las modificaciones.
        '''
        tiempo_inicio = datetime.datetime.now()
        if compuerta == None:
            print "Cargando las tareas del dia", datetime.date.today(), 'para todas las compuertas...'
        else:
            print "Cargando las tareas del dia", datetime.date.today(), 'para la compuerta', compuerta
        self.__tareas_compuertas=[]
        del self.__tareas_compuertas[0:]
        bd = self.GetParent().GetParent().getBd() 
        if not bd.open:
            bd = MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')
        cursor = bd.cursor()
        cursorProc = bd.cursor()
        if compuerta != None: #Verifico si el parámetro está vacío
                        cursor.execute("SELECT idCONFIG_TAREAS_PROGRAMADA, Id_Compuerta, Hora_Ejecucion, Config_Kg, " + 
                           "concat(CONFIG_PARAMETROS_KILOS_ALIMENTOS.Kg, ' Kg de ', ALIMENTOS.Nombre) as 'Alimento'" +
                           "FROM ListaTareasProgramadasFechaActual inner join CONFIG_PARAMETROS_KILOS_ALIMENTOS ON " + 
                           "ListaTareasProgramadasFechaActual.Config_Kg = CONFIG_PARAMETROS_KILOS_ALIMENTOS.idCONFIG_PARAMETROS_KILOS " + 
                           "inner join ALIMENTOS ON CONFIG_PARAMETROS_KILOS_ALIMENTOS.Tipo_alimento = ALIMENTOS.id_ALIMENTO "+
                           "where ListaTareasProgramadasFechaActual.Id_Compuerta=%s order by Hora_Ejecucion", (compuerta,))
        else:
            cursor.execute("SELECT idCONFIG_TAREAS_PROGRAMADA, Id_Compuerta, Hora_Ejecucion, Config_Kg, " + 
                           "concat(CONFIG_PARAMETROS_KILOS_ALIMENTOS.Kg, ' Kg de ', ALIMENTOS.Nombre) as 'Alimento'" +
                           "FROM ListaTareasProgramadasFechaActual inner join CONFIG_PARAMETROS_KILOS_ALIMENTOS ON " + 
                           "ListaTareasProgramadasFechaActual.Config_Kg = CONFIG_PARAMETROS_KILOS_ALIMENTOS.idCONFIG_PARAMETROS_KILOS " + 
                           "inner join ALIMENTOS ON CONFIG_PARAMETROS_KILOS_ALIMENTOS.Tipo_alimento = ALIMENTOS.id_ALIMENTO " +
                           "order by Hora_Ejecucion")
        resultado_tareas_dia = cursor.fetchall()
        
        id_config_tareas=0
        contador_tareas =0
        lista_tareas = [] #Lista de las tareas resultantes
        cursorProc.callproc('CONSULTA_MODIFICACION_TAREAS', [str(datetime.date.today())]) #llama al procedimiento que crea las tablas temporales
        
        for res in resultado_tareas_dia:
            if id_config_tareas != res[0]: #Filtro cada tarea y cargo su configuracion
                cur_tarea_sin_moficicaciones = bd.cursor()
                tarea = {'id':0, 'compuerta':0, 'hora':0, 'config_kg':0}
                hora = str(res[2]) #La hora es de tipo datetime.timedelta
                if len(str(hora))==7: #Es un horario entre las 0 y las 9, le agrego el 0 delante para que quede 05, 09, 06, etc
                    hora = "0"+ str(hora)
                print "   Tienes la tarea", res[0], 'con hora de ejecucion', hora
                tarea['id'] = int(res[0])
                tarea['compuerta'] = int(res[1])    
                tarea['hora'] = datetime.datetime(datetime.date.today().year, datetime.date.today().month, datetime.date.today().day, int(hora[0:2]), int(hora[3:5]), int(hora[6:8]))
                tarea['config_kg'] = str(res[4])
                cur_tarea_sin_moficicaciones.execute("Select * from CONFIG_PARAMETROS_KILOS_ALIMENTOS Where idCONFIG_PARAMETROS_KILOS=%s", (res[2],))
                res_cur_tarea_sin_moficicaciones = cur_tarea_sin_moficicaciones.fetchall()
                for val_res_cur_tarea_sin_moficicaciones in res_cur_tarea_sin_moficicaciones: #Cargo la config_kg de la tarea original
                    pass
                    #tarea.setPosicion(val_res_cur_tarea_sin_moficicaciones[4]) 
                    #tarea.setDuracion(val_res_cur_tarea_sin_moficicaciones[3])
                cursor.execute('Select * from ListaTareasModificadas Where idCONFIG_TAREAS_PROGRAMADA=%s', (res[0],))
                resultado_modificaciones = cursor.fetchall()
                for res_modificaciones in resultado_modificaciones: #Busco las modificaciones para la tarea
                    print '        Tarea', res_modificaciones[0], 'con modificacion', res_modificaciones[1]
                    cursorModificacion = bd.cursor()
                    cursorModificacion.execute("Select MODIFICACION_INSTANCIAS_TAREAS.Hora_Ejecucion, "+
                    "CONFIG_PARAMETROS_KILOS_ALIMENTOS.Cierre, CONFIG_PARAMETROS_KILOS_ALIMENTOS.Apertura, "+  
                    "concat(CONFIG_PARAMETROS_KILOS_ALIMENTOS.Kg, ' Kg de ', ALIMENTOS.Nombre)  as 'Alimento'" +                         
                    "from ListaTareasModificadas INNER JOIN MODIFICACION_INSTANCIAS_TAREAS "+
                    "on ListaTareasModificadas.IdModTarea = MODIFICACION_INSTANCIAS_TAREAS.idMODIFICACION_INSTANCIA_TAREA " +
                    "inner join CONFIG_PARAMETROS_KILOS_ALIMENTOS ON MODIFICACION_INSTANCIAS_TAREAS.Config_Kg = "+
                    "CONFIG_PARAMETROS_KILOS_ALIMENTOS.idCONFIG_PARAMETROS_KILOS "+
                    "inner join ALIMENTOS ON CONFIG_PARAMETROS_KILOS_ALIMENTOS.Tipo_alimento = ALIMENTOS.id_ALIMENTO " + 
                    "Where ListaTareasModificadas.idCONFIG_TAREAS_PROGRAMADA=%s "+
                    "order by MODIFICACION_INSTANCIAS_TAREAS.Hora_Ejecucion", (res_modificaciones[0],))
                    res_consulta_modificaciones =cursorModificacion.fetchall()
                    for val in res_consulta_modificaciones: #Sobreescribo las modificiones
                        hora = str(val[0]) #La hora es de tipo datetime.timedelta
                        if len(str(hora))==7: #Es un horario entre las 0 y las 9, le agrego el 0 delante para que quede 05, 09, 06, etc
                            hora = "0"+ str(hora)
                        #print hora
                        tarea['hora'] = datetime.datetime(datetime.date.today().year, datetime.date.today().month, datetime.date.today().day, int(hora[0:2]), int(hora[3:5]), int(hora[6:8]))
                        tarea['config_kg'] = str(val[3])
                        #tarea.setPosicion(val[2])
                        #tarea.setDuracion(val[1])
                if tarea['hora'] > datetime.datetime.now(): #Agrego a la lista de tareas a ejecutar solo quellas tareas cuya hora de inicio es mayor a la hora actual
                    self.__tareas_compuertas.append(tarea)
                contador_tareas +=1
            
            id_config_tareas = res[0]
        
        tiempo_fin = datetime.datetime.now()
        print tiempo_fin - tiempo_inicio    
        print 'TOTAL:', contador_tareas, "tareas para el dia", str(datetime.date.today())
        print "Solo", len(self.__tareas_compuertas), 'tarea/s sera ejecutada por diferencia horaria'
        print "\n"
        if len(self.__tareas_compuertas) > 0: print "LISTA DE TAREAS A EJECUTAR PARA EL", datetime.date.today()
        for x in self.__tareas_compuertas:
            print 'Tarea', x['id'], 'en compuerta', x['compuerta'], 'hora de ejecucion' , x['hora'], "\n" #'posicion', x.getPosicion() ,'duracion', x.getDuracion(), 'segundos'
        
        bd.close()
        return self.__tareas_compuertas
    
    
    def cargarTareasDelDiaV2(self, compuerta=None):
        '''Como el servidor explota las tareas del día al cambiar de día o al realizar una modificación de 
        las mismas, busco directamente las tareas en la tabla BOM_TAREAS_PROGRAMADAS evitando usar la función
        cargarTareasDelDia.
        '''
        bd = self.GetParent().GetParent().getBd() 
        self.__tareas_compuertas=[]
        if not bd.open:
            bd = MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')
        cursor = bd.cursor()
        
        if compuerta == None:
            print "Cargando las tareas del dia", datetime.date.today(), 'para todas las compuertas...'
            cursor.execute("Select BOM_TAREAS_PROGRAMADAS.Id_Tarea, BOM_TAREAS_PROGRAMADAS.Compuerta, "+
                           "BOM_TAREAS_PROGRAMADAS.Fecha, BOM_TAREAS_PROGRAMADAS.Hora_Ejecucion, "+
                           "concat(CONFIG_PARAMETROS_KILOS_ALIMENTOS.Kg, ' Kg de ', ALIMENTOS.Nombre) As 'Alimento' "+
                           "from BOM_TAREAS_PROGRAMADAS " 
                           "inner join CONFIG_PARAMETROS_KILOS_ALIMENTOS on BOM_TAREAS_PROGRAMADAS.Config_Kg = "+
                           "CONFIG_PARAMETROS_KILOS_ALIMENTOS.idCONFIG_PARAMETROS_KILOS inner join ALIMENTOS on "+
                           "CONFIG_PARAMETROS_KILOS_ALIMENTOS.Tipo_alimento = ALIMENTOS.id_ALIMENTO " + 
                           "where Fecha=%s", (datetime.date.today(), ))
        else:
            print "Cargando las tareas del dia", datetime.date.today(), 'para la compuerta', compuerta
            cursor.execute("Select BOM_TAREAS_PROGRAMADAS.Id_Tarea, BOM_TAREAS_PROGRAMADAS.Compuerta, "+
                           "BOM_TAREAS_PROGRAMADAS.Fecha, BOM_TAREAS_PROGRAMADAS.Hora_Ejecucion, "+
                           "concat(CONFIG_PARAMETROS_KILOS_ALIMENTOS.Kg, ' Kg de ', ALIMENTOS.Nombre) As 'Alimento' "+
                           "from BOM_TAREAS_PROGRAMADAS " 
                           "inner join CONFIG_PARAMETROS_KILOS_ALIMENTOS on BOM_TAREAS_PROGRAMADAS.Config_Kg = "+
                           "CONFIG_PARAMETROS_KILOS_ALIMENTOS.idCONFIG_PARAMETROS_KILOS inner join ALIMENTOS on "+
                           "CONFIG_PARAMETROS_KILOS_ALIMENTOS.Tipo_alimento = ALIMENTOS.id_ALIMENTO " + 
                           "where Fecha=%s and Compuerta=%s", (datetime.date.today(), compuerta, ))
        
        resultado = cursor.fetchall()
        
        for val in resultado:
            hora = str(val[3])
            if len(str(hora))==7: #Es un horario entre las 0 y las 9, le agrego el 0 delante para que quede 05, 09, 06, etc
                hora = "0"+ str(hora)
            
            print hora
            fecha = datetime.datetime(datetime.date.today().year, datetime.date.today().month, datetime.date.today().day, int(hora[0:2]), int(hora[3:5]), int(hora[6:8]))
            if fecha > datetime.datetime.now(): #Agrego a la lista de tareas a ejecutar solo quellas tareas cuya hora de inicio es mayor a la hora actual
                tarea = {'id_tarea': val[0], 'compuerta': val[1], 'fecha': val[2], 'hora': val[3], 'config_kg': val[4]}
                self.__tareas_compuertas.append(tarea)
            
        return self.__tareas_compuertas 


    def cargarTareasCanceladas(self, compuerta=None):
        lista_tareas_canceladas=[]
        bd = MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')
        cur = bd.cursor()
        cur.execute("SELECT CONFIG_TAREAS_PROGRAMADAS.idCONFIG_TAREAS_PROGRAMADA, CONFIG_TAREAS_PROGRAMADAS.Id_Compuerta, " +
                    "CONFIG_TAREAS_PROGRAMADAS.Config_Kg, CONFIG_TAREAS_PROGRAMADAS.Fecha_Inicio, " +
                    "CONFIG_TAREAS_PROGRAMADAS.Fecha_Fin, CONFIG_TAREAS_PROGRAMADAS.Para_Siempre, " +
                    "CONFIG_TAREAS_PROGRAMADAS.Hora_Ejecucion, CONFIG_TAREAS_PROGRAMADAS.Fecha_Cancelacion, " +
                    "concat(CONFIG_PARAMETROS_KILOS_ALIMENTOS.Kg, ' Kg de ', ALIMENTOS.Nombre) as 'Alimento' " +
                    "FROM CONFIG_TAREAS_PROGRAMADAS " + 
                    "inner join CONFIG_PARAMETROS_KILOS_ALIMENTOS ON CONFIG_TAREAS_PROGRAMADAS.Config_Kg= " +
                    "CONFIG_PARAMETROS_KILOS_ALIMENTOS.idCONFIG_PARAMETROS_KILOS INNER JOIN ALIMENTOS " +
                    "ON CONFIG_PARAMETROS_KILOS_ALIMENTOS.Tipo_alimento = ALIMENTOS.id_ALIMENTO " +
                    "Where Cancelada=True and (Fecha_Fin > curdate() or Para_Siempre=True)")
        
        resultado = cur.fetchall()
        
        for val in resultado:
            tarea = {'id':val[0], 'compuerta':val[1], 'config_kg':val[8], 'fecha_ini':val[3],
                     'fecha_fin':val[4], 'hora':val[6],'para_siempre':val[5], 'fecha_cancelacion':val[7]}
            lista_tareas_canceladas.append(tarea)
            
        return lista_tareas_canceladas
    
    
    def cargarTareasEjecutadas(self, compuerta=None, fecha=None):
        '''Se obtienen las tareas que fueron ejecutadas con éxito.
        Las tareas que están marcadas para siempre, que fueron canceladas en el momento anterior o que 
        no aparecen por algún motivo (corte de energía eléctrica), no aparecen en esta lista ya que no fueron 
        ejecutadas con éxito.
        '''
        
        '''
        Select *
        from TAREAS_EJECUTADAS
        INNER join CONFIG_TAREAS_PROGRAMADAS ON TAREAS_EJECUTADAS.Id_Config_Tarea =
        CONFIG_TAREAS_PROGRAMADAS.idCONFIG_TAREAS_PROGRAMADA
        LEFT join MODIFICACION_INSTANCIAS_TAREAS ON CONFIG_TAREAS_PROGRAMADAS.idCONFIG_TAREAS_PROGRAMADA=
        MODIFICACION_INSTANCIAS_TAREAS.Id_Config_Tarea
        order by TAREAS_EJECUTADAS.Fecha, TAREAS_EJECUTADAS.Hora_Ejecucion
        '''
        bd = MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')
        cur = bd.cursor()
        if compuerta == "Todas":
            compuerta = None
            
        if compuerta == None or compuerta == "Todas" and fecha == None:
            cur.execute("Select TAREAS_EJECUTADAS.idTAREAS_EJECUTADAS, TAREAS_EJECUTADAS.Id_Config_Tarea, "+
                        "CONFIG_TAREAS_PROGRAMADAS.Id_Compuerta, TAREAS_EJECUTADAS.Fecha, "+ 
                        "TAREAS_EJECUTADAS.Hora_Ejecucion, "+
                        "CONFIG_TAREAS_PROGRAMADAS.Config_Kg, " +
                        "MODIFICACION_INSTANCIAS_TAREAS.Config_Kg as 'Config_kg_modificacion'"+
                        "from TAREAS_EJECUTADAS "+
                        "INNER join CONFIG_TAREAS_PROGRAMADAS ON TAREAS_EJECUTADAS.Id_Config_Tarea = "+
                        "CONFIG_TAREAS_PROGRAMADAS.idCONFIG_TAREAS_PROGRAMADA "+
                        "LEFT join MODIFICACION_INSTANCIAS_TAREAS ON CONFIG_TAREAS_PROGRAMADAS.idCONFIG_TAREAS_PROGRAMADA= "+
                        "MODIFICACION_INSTANCIAS_TAREAS.Id_Config_Tarea "+
                        "where MODIFICACION_INSTANCIAS_TAREAS.Hora_Ejecucion is Null or "+ 
                        "TAREAS_EJECUTADAS.Hora_Ejecucion = MODIFICACION_INSTANCIAS_TAREAS.Hora_Ejecucion "+
                        "order by TAREAS_EJECUTADAS.Fecha, TAREAS_EJECUTADAS.Hora_Ejecucion")
        elif compuerta != None and fecha != None:
            cur.execute("Select TAREAS_EJECUTADAS.idTAREAS_EJECUTADAS, TAREAS_EJECUTADAS.Id_Config_Tarea, "+
                        "CONFIG_TAREAS_PROGRAMADAS.Id_Compuerta, TAREAS_EJECUTADAS.Fecha, "+ 
                        "TAREAS_EJECUTADAS.Hora_Ejecucion, "+
                        "CONFIG_TAREAS_PROGRAMADAS.Config_Kg, " +
                        "MODIFICACION_INSTANCIAS_TAREAS.Config_Kg as 'Config_kg_modificacion'"+
                        "from TAREAS_EJECUTADAS "+
                        "INNER join CONFIG_TAREAS_PROGRAMADAS ON TAREAS_EJECUTADAS.Id_Config_Tarea = "+
                        "CONFIG_TAREAS_PROGRAMADAS.idCONFIG_TAREAS_PROGRAMADA "+
                        "LEFT join MODIFICACION_INSTANCIAS_TAREAS ON CONFIG_TAREAS_PROGRAMADAS.idCONFIG_TAREAS_PROGRAMADA= "+
                        "MODIFICACION_INSTANCIAS_TAREAS.Id_Config_Tarea "+
                        "where (MODIFICACION_INSTANCIAS_TAREAS.Hora_Ejecucion is Null or "+ 
                        "TAREAS_EJECUTADAS.Hora_Ejecucion = MODIFICACION_INSTANCIAS_TAREAS.Hora_Ejecucion) and "+ 
                        "CONFIG_TAREAS_PROGRAMADAS.Id_Compuerta=%s and TAREAS_EJECUTADAS.Fecha=%s "+
                        "order by TAREAS_EJECUTADAS.Fecha, TAREAS_EJECUTADAS.Hora_Ejecucion", (compuerta, fecha, ))
        elif compuerta != None:
            cur.execute("Select TAREAS_EJECUTADAS.idTAREAS_EJECUTADAS, TAREAS_EJECUTADAS.Id_Config_Tarea, "+
                        "CONFIG_TAREAS_PROGRAMADAS.Id_Compuerta, TAREAS_EJECUTADAS.Fecha, "+ 
                        "TAREAS_EJECUTADAS.Hora_Ejecucion, "+
                        "CONFIG_TAREAS_PROGRAMADAS.Config_Kg, " +
                        "MODIFICACION_INSTANCIAS_TAREAS.Config_Kg as 'Config_kg_modificacion'"+
                        "from TAREAS_EJECUTADAS "+
                        "INNER join CONFIG_TAREAS_PROGRAMADAS ON TAREAS_EJECUTADAS.Id_Config_Tarea = "+
                        "CONFIG_TAREAS_PROGRAMADAS.idCONFIG_TAREAS_PROGRAMADA "+
                        "LEFT join MODIFICACION_INSTANCIAS_TAREAS ON CONFIG_TAREAS_PROGRAMADAS.idCONFIG_TAREAS_PROGRAMADA= "+
                        "MODIFICACION_INSTANCIAS_TAREAS.Id_Config_Tarea "+
                        "where ((MODIFICACION_INSTANCIAS_TAREAS.Hora_Ejecucion is Null or "+ 
                        "TAREAS_EJECUTADAS.Hora_Ejecucion = MODIFICACION_INSTANCIAS_TAREAS.Hora_Ejecucion) and "+ 
                        "CONFIG_TAREAS_PROGRAMADAS.Id_Compuerta=%s )"+
                        "order by TAREAS_EJECUTADAS.Fecha, TAREAS_EJECUTADAS.Hora_Ejecucion", (compuerta, ))
        elif fecha != None:
            cur.execute("Select TAREAS_EJECUTADAS.idTAREAS_EJECUTADAS, TAREAS_EJECUTADAS.Id_Config_Tarea, "+
                        "CONFIG_TAREAS_PROGRAMADAS.Id_Compuerta, TAREAS_EJECUTADAS.Fecha, "+ 
                        "TAREAS_EJECUTADAS.Hora_Ejecucion, "+
                        "CONFIG_TAREAS_PROGRAMADAS.Config_Kg, " +
                        "MODIFICACION_INSTANCIAS_TAREAS.Config_Kg as 'Config_kg_modificacion'"+
                        "from TAREAS_EJECUTADAS "+
                        "INNER join CONFIG_TAREAS_PROGRAMADAS ON TAREAS_EJECUTADAS.Id_Config_Tarea = "+
                        "CONFIG_TAREAS_PROGRAMADAS.idCONFIG_TAREAS_PROGRAMADA "+
                        "LEFT join MODIFICACION_INSTANCIAS_TAREAS ON CONFIG_TAREAS_PROGRAMADAS.idCONFIG_TAREAS_PROGRAMADA= "+
                        "MODIFICACION_INSTANCIAS_TAREAS.Id_Config_Tarea "+
                        "where MODIFICACION_INSTANCIAS_TAREAS.Hora_Ejecucion is Null or "+ 
                        "TAREAS_EJECUTADAS.Hora_Ejecucion = MODIFICACION_INSTANCIAS_TAREAS.Hora_Ejecucion and "+ 
                        "TAREAS_EJECUTADAS.Fecha=%s "+
                        "order by TAREAS_EJECUTADAS.Fecha, TAREAS_EJECUTADAS.Hora_Ejecucion", (fecha, ))
            
        result = cur.fetchall()
        lista_tareas_ejecutadas = []
                
        for res in result:
            tarea_ejecutada = {'id':res[0], 'compuerta':res[2], 'fecha':res[3], 'hora_ejecucion':res[4], 
                               'config_kg': res[5]}
            curConfigKg = bd.cursor()
            if res[6] != None: #Tiene una modificacion de config_kg 
                curConfigKg.execute("Select CONCAT(CONFIG_PARAMETROS_KILOS_ALIMENTOS.Kg, ' Kg de ', ALIMENTOS.Nombre) as 'Alimento' " +
                                    "from CONFIG_PARAMETROS_KILOS_ALIMENTOS inner join ALIMENTOS " +
                                    "ON CONFIG_PARAMETROS_KILOS_ALIMENTOS.Tipo_alimento = ALIMENTOS.id_ALIMENTO "+
                                    "Where CONFIG_PARAMETROS_KILOS_ALIMENTOS.idCONFIG_PARAMETROS_KILOS=%s", (res[6], ))
                rescurConfigKg = curConfigKg.fetchall() 
                tarea_ejecutada['config_kg'] = rescurConfigKg[0][0]
            else:
                curConfigKg.execute("Select CONCAT(CONFIG_PARAMETROS_KILOS_ALIMENTOS.Kg, ' Kg de ', ALIMENTOS.Nombre) as 'Alimento' " +
                                    "from CONFIG_PARAMETROS_KILOS_ALIMENTOS inner join ALIMENTOS " +
                                    "ON CONFIG_PARAMETROS_KILOS_ALIMENTOS.Tipo_alimento = ALIMENTOS.id_ALIMENTO "+
                                    "Where CONFIG_PARAMETROS_KILOS_ALIMENTOS.idCONFIG_PARAMETROS_KILOS=%s", (res[5], ))
                rescurConfigKg = curConfigKg.fetchall() 
                tarea_ejecutada['config_kg'] = rescurConfigKg[0][0]

            lista_tareas_ejecutadas.append(tarea_ejecutada)
            
        return lista_tareas_ejecutadas
        
    
    def crearNuevaTarea(self, event):
        compuerta = self.__cmbListaCompuertas.GetValue() 
        guiNuevaTarea = GUIRepeticionTareaSimple(self, compuerta)
        guiNuevaTarea.ShowModal()
        guiNuevaTarea.Destroy()
        self.filtrarTareasCompuerta(None)
        
    
    def editarTarea(self, event):
        tarea = self.grid_fecha_actual.GetCellValue(self.grid_fecha_actual.GetGridCursorRow(), 0)
        guiEdicionTarea = GUIRepeticionTareaSimpleEditMode(self, tarea=tarea)
        guiEdicionTarea.ShowModal()
        guiEdicionTarea.Destroy()
        self.filtrarTareasCompuerta(None)
        
    
    def eliminarTarea(self, event):
        tarea = self.grid_fecha_actual.GetCellValue(self.grid_fecha_actual.GetGridCursorRow(), 0)
        dialogo = wx.MessageDialog(self, "¿Deseas eliminar la tarea programada?", "Eliminar tarea", wx.YES_NO | wx.ICON_QUESTION)
        result = dialogo.ShowModal() == wx.ID_YES #En una sola linea muestro el dialogo en modal y obtengo su resultado
        dialogo.Destroy()
        
        if result == True:
            bd = MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')
            cursor = bd.cursor()
            cursor.execute("UPDATE CONFIG_TAREAS_PROGRAMADAS SET Cancelada=1 WHERE idCONFIG_TAREAS_PROGRAMADA=%s;", (tarea,))
            cursor.connection.commit()
            bd.close()
            self.grid_fecha_actual.DeleteRows(self.grid_fecha_actual.GetGridCursorRow(), 1)
            
    
    def reanudarTarea(self, event):
        tarea = self.grid_tareas_canceladas.GetCellValue(self.grid_tareas_canceladas.GetGridCursorRow(), 0)
        dialogo = wx.MessageDialog(self, "¿Deseas reanudar la tarea programada?", "Reanudar tarea", wx.YES_NO | wx.ICON_QUESTION)
        result = dialogo.ShowModal() == wx.ID_YES #En una sola linea muestro el dialogo en modal y obtengo su resultado
        dialogo.Destroy()
        
        if result == True:
            bd = MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')
            cursor = bd.cursor()
            cursor.execute("UPDATE CONFIG_TAREAS_PROGRAMADAS SET Cancelada=0 WHERE idCONFIG_TAREAS_PROGRAMADA=%s;", (tarea,))
            cursor.connection.commit()
            bd.close()
            self.grid_tareas_canceladas.DeleteRows(self.grid_tareas_canceladas.GetGridCursorRow(), 1)
            #self.notificarCambios(event)
            
                        
        

class PanelConfigCompuerta(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        btn = wx.Button(self, -1, "Hola nena")
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(btn, 0, wx.ALL|wx.EXPAND)
        self.SetSizerAndFit(sizer)

        
        
        
        
'''
app = wx.App()
ventana = GUIConfigCompuerta(None, compuerta=1, notebookTab=1)
#ventana = GUIExcepciones(None)
ventana.CenterOnScreen()
ventana.ShowModal()
#ventana.Show()
ventana.Destroy()
app.MainLoop()
'''
