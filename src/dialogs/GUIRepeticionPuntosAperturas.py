# coding=utf-8
'''
Created on 30/06/2017

@author: diegoariel
Este calendario está inspirado en el cuadro de repeticiones de tareas de Evolution...
'''

import wx
import wx.calendar
from src.widgets import widget
from src import widgets
from wx.lib.pubsub import pub 

class RepeticionPuntosAperturas(wx.Dialog):
    def __init__(self, parent):
        size=(680,600)
        super(RepeticionPuntosAperturas, self).__init__(parent, wx.ID_ANY, u'Repetición de los puntos de aperturas', size=size)
        self.panelRepeticionTarea = wx.Panel(self, wx.ID_ANY)
        lblRepeticion = wx.StaticText(self.panelRepeticionTarea, -1, u'Repetición')
        chk_se_repite = wx.CheckBox(self.panelRepeticionTarea, -1, "Esta tarea se repite")
        chk_se_repite.Bind(wx.EVT_CHECKBOX, self.habilitarPanel)
        lblCada = wx.StaticText(self.panelRepeticionTarea, -1, "Cada")
        spinboxCada = wx.SpinCtrl(self.panelRepeticionTarea, -1, value="1", size=(50, -1), max=1000) #Un año máximo repetición...
        self.cmbCada = wx.ComboBox(self.panelRepeticionTarea, -1, size=(100,-1), style=wx.CB_READONLY)
        self.cmbCada.Bind(wx.EVT_COMBOBOX, self.seleccionCada)
        self.cmbTiempo = wx.ComboBox(self.panelRepeticionTarea, -1, size=(100,-1), style=wx.CB_READONLY)
        self.cmbTiempo.Bind(wx.EVT_COMBOBOX, self.seleccionTiempo)
        self.spinboxHasta = wx.SpinCtrl(self.panelRepeticionTarea, -1, value="1", size=(50, -1), max=1000) #Un año máximo repetición...
        self.calendarioHasta = widgets.widget.DatePickerCtrlV2(self.panelRepeticionTarea)
        self.lblRepeticiones = wx.StaticText(self.panelRepeticionTarea, -1, "Repeticiones")
        
        self.panelSeleccionSemanas = wx.Panel(self.panelRepeticionTarea, -1)
        self.chkDom = wx.CheckBox(self.panelSeleccionSemanas, 1, "Dom")
        self.chkLun = wx.CheckBox(self.panelSeleccionSemanas, 1, "Lun")
        self.chkMar = wx.CheckBox(self.panelSeleccionSemanas, 1, "Mar")
        self.chkMier = wx.CheckBox(self.panelSeleccionSemanas, 1, "Mie")
        self.chkJue = wx.CheckBox(self.panelSeleccionSemanas, 1, "Jue")
        self.chkvie = wx.CheckBox(self.panelSeleccionSemanas, 1, "Vie")
        self.chkSab = wx.CheckBox(self.panelSeleccionSemanas, 1, "Sab")
        
        
        sizerSeleccionSemanas = wx.BoxSizer(wx.HORIZONTAL)
        sizerSeleccionSemanas.Add(self.chkDom)
        sizerSeleccionSemanas.Add(self.chkLun)
        sizerSeleccionSemanas.Add(self.chkMar)
        sizerSeleccionSemanas.Add(self.chkMier)
        sizerSeleccionSemanas.Add(self.chkJue)
        sizerSeleccionSemanas.Add(self.chkvie)
        sizerSeleccionSemanas.Add(self.chkSab)
        self.panelSeleccionSemanas.SetSizer(sizerSeleccionSemanas)
        self.panelSeleccionSemanas.Show(False)
        
        
        self.panelMeses = wx.Panel(self.panelRepeticionTarea, -1)
        lblEnel = wx.StaticText(self.panelMeses, -1, "en el")
        self.cmbEnel = wx.ComboBox(self.panelMeses, -1, size=(100,-1), style=wx.CB_READONLY)
        self.cmbEnel.AppendItems(["Primer", "Segundo", "Tercer", "Cuarto", "Quinto", u"Último", "1", "Otra fecha"])
        self.cmbEnel.Bind(wx.EVT_COMBOBOX, self.seleccionEnel)
        self.cmbDia = wx.ComboBox(self.panelMeses, -1, size=(100,-1), style=wx.CB_READONLY)
        self.cmbDia.AppendItems(["Dia", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", u"Sábado", "Domingo"])
        self.cmbDia.Bind(wx.EVT_COMBOBOX, self.seleccionDia)      
        
        
        sizerMeses = wx.BoxSizer(wx.HORIZONTAL)
        sizerMeses.Add(lblEnel, flag=wx.ALIGN_CENTER_VERTICAL)
        sizerMeses.Add(self.cmbEnel)
        sizerMeses.Add(self.cmbDia)
        self.panelMeses.SetSizer(sizerMeses)
        self.panelMeses.Show(False)        
          
        
        self.sizerConfigRepeticion = wx.BoxSizer(wx.HORIZONTAL)
        self.sizerConfigRepeticion.Add(lblCada, 0, wx.ALIGN_CENTRE_VERTICAL, 0)
        self.sizerConfigRepeticion.Add(spinboxCada, 0, wx.ALIGN_CENTRE_VERTICAL, 0)
        self.sizerConfigRepeticion.Add(self.cmbCada, 0, wx.ALIGN_CENTRE_VERTICAL, 0)
        self.sizerConfigRepeticion.Add(self.panelSeleccionSemanas, 0, wx.ALIGN_CENTRE_VERTICAL)
        self.sizerConfigRepeticion.Add(self.panelMeses, 0, wx.ALIGN_CENTRE_VERTICAL)
        self.sizerConfigRepeticion.Add(self.cmbTiempo, 0, wx.ALIGN_CENTRE_VERTICAL)
        self.sizerConfigRepeticion.Add(self.spinboxHasta, 0, wx.ALIGN_CENTRE_VERTICAL)   
        self.sizerConfigRepeticion.Add(self.calendarioHasta, 0, wx.ALIGN_CENTRE_VERTICAL)
        self.sizerConfigRepeticion.Add(self.lblRepeticiones, 0, wx.ALIGN_CENTRE_VERTICAL)
        
        self.mainPanel = wx.Panel(self.panelRepeticionTarea)
        lblExepciones = wx.StaticText(self.panelRepeticionTarea, -1, "Excepciones")
        self.listaExcepciones = wx.ListBox(self.mainPanel, -1, size=(100, 100))
        btnAgregarExcepcion = wx.Button(self.mainPanel, -1, "Agregar")
        btnAgregarExcepcion.Bind(wx.EVT_BUTTON, self.agregarExcepcion)
        btnModificarExcepcion = wx.Button(self.mainPanel, -1, "Modificar")
        btnModificarExcepcion.Bind(wx.EVT_BUTTON, self.cambiarExcepcion)
        btnEliminaExcepcion = wx.Button(self.mainPanel, -1, "Eliminar")
        btnEliminaExcepcion.Bind(wx.EVT_BUTTON, self.eliminarExcepcion)
        sizerExcepciones = wx.BoxSizer(wx.HORIZONTAL)
        sizerExcepciones.Add(self.listaExcepciones, 1, wx.EXPAND)
        sizerBotonesExcepciones = wx.BoxSizer(wx.VERTICAL)
        sizerBotonesExcepciones.Add(btnAgregarExcepcion)
        sizerBotonesExcepciones.Add(btnModificarExcepcion)
        sizerBotonesExcepciones.Add(btnEliminaExcepcion)
        sizerExcepciones.Add(sizerBotonesExcepciones)
        self.mainPanel.SetSizer(sizerExcepciones)
        
        
        self.panelVistaPrevia = wx.Panel(self.panelRepeticionTarea)
        lblVistaPrevia = wx.StaticText(self.panelRepeticionTarea, -1, "Vista previa")
        lblVistaPrevia.SetForegroundColour(wx.BLACK)
        lblVistaPrevia.SetFont(wx.Font(12, wx.NORMAL, wx.ITALIC, wx.BOLD))
        self.calendario1 = wx.calendar.CalendarCtrl(self.panelVistaPrevia, -1)
        self.calendario2 = wx.calendar.CalendarCtrl(self.panelVistaPrevia, -1)
        self.calendario3 = wx.calendar.CalendarCtrl(self.panelVistaPrevia, -1)
        sizerVistaPrevia = wx.BoxSizer(wx.HORIZONTAL)
        sizerVistaPrevia.Add(self.calendario1)
        sizerVistaPrevia.Add(self.calendario2)
        sizerVistaPrevia.Add(self.calendario3)
        self.panelVistaPrevia.SetSizer(sizerVistaPrevia)
        
        
        self.sizerRepeticionTarea = wx.BoxSizer(wx.VERTICAL)
        self.sizerRepeticionTarea.Add(lblRepeticion)
        self.sizerRepeticionTarea.Add(chk_se_repite)
        self.sizerRepeticionTarea.Add(self.sizerConfigRepeticion)
        self.sizerRepeticionTarea.Add(lblExepciones)
        self.sizerRepeticionTarea.Add(self.mainPanel, 0, wx.EXPAND, 5)
        self.sizerRepeticionTarea.Add(lblVistaPrevia, 0, wx.EXPAND, 10)
        self.sizerRepeticionTarea.Add(self.panelVistaPrevia, 0, wx.ALIGN_CENTER_HORIZONTAL, 10)
        self.panelRepeticionTarea.SetSizer(self.sizerRepeticionTarea)
        self.sizerConfigRepeticion.ShowItems(False)
        
        
        self.cmbCada.AppendItems(["Dia(s)", "Semana(s)", "Meses(s)", u'Año(s)'])
        self.cmbCada.SetSelection(0)
        self.cmbTiempo.AppendItems(["Durante", "Hasta", "Para siempre"])
        self.cmbTiempo.SetSelection(0)
       
        self.Layout()
        #self.sizerRepeticionTarea.Fit(self)
        
    def seleccionCada(self, event):
        if self.cmbCada.GetSelection() == 0: #Dias
            print "Dias"
            self.panelSeleccionSemanas.Show(False)
            self.panelMeses.Show(False)
            #self.SetSizerAndFit(self.sizerRepeticionTarea)
            #self.panelRepeticionTarea.Layout()
            #self.sizerConfigRepeticion.Fit(self)
            self.panelRepeticionTarea.SetSizerAndFit(self.sizerRepeticionTarea)
            self.Fit()
            self.Layout()
        if self.cmbCada.GetSelection() == 1: #Semanas
            #Hacer que aparezca la seleccion de semanas
            print "Semanas" 
            self.panelMeses.Show(False)
            self.panelSeleccionSemanas.Show(True)
            self.panelRepeticionTarea.SetSizerAndFit(self.sizerRepeticionTarea)
            #self.sizerConfigRepeticion.Fit(self)
            #self.panelRepeticionTarea.Layout()
            self.Fit()
            self.Layout()
        if self.cmbCada.GetSelection() ==2:#Meses
            print  "meses"
            self.panelSeleccionSemanas.Show(False)
            self.panelMeses.Show(True)
            self.panelRepeticionTarea.SetSizerAndFit(self.sizerRepeticionTarea)
            self.Fit()   
            self.Layout()
        if self.cmbCada.GetSelection() ==3: #Años   
            print  "Años"
            self.panelSeleccionSemanas.Hide()
            self.panelMeses.Hide()
            self.panelRepeticionTarea.SetSizerAndFit(self.sizerRepeticionTarea)
            self.Fit()   
            self.Layout() 
          
            
    def seleccionTiempo(self, event):
        if self.cmbTiempo.GetSelection() == 0:#Meses
            self.spinboxHasta.Show()
            self.calendarioHasta.Hide()
            self.lblRepeticiones.Show()
        if self.cmbTiempo.GetSelection() == 1:
            self.spinboxHasta.Hide()
            self.calendarioHasta.Show()
            self.lblRepeticiones.Hide()
        if self.cmbTiempo.GetSelection() == 2:
            self.spinboxHasta.Hide()
            self.calendarioHasta.Hide()
            self.lblRepeticiones.Hide()
        
        self.sizerConfigRepeticion.Layout()    
        self.panelRepeticionTarea.SetSizerAndFit(self.sizerRepeticionTarea)
        self.Fit()   
        self.Layout()
    
            
    def habilitarPanel(self, event):
        if event.IsChecked():
            print "True"
            self.sizerConfigRepeticion.ShowItems(True)
            self.calendarioHasta.Hide()
        else:
            self.sizerConfigRepeticion.ShowItems(False)
        
        self.panelMeses.Show(False)
        self.panelSeleccionSemanas.Show(False)
        self.panelRepeticionTarea.SetSizerAndFit(self.sizerRepeticionTarea)
        self.Fit()   
        self.Layout() 
            
            
    def seleccionEnel(self, event):
        cmbOrigen = event.GetEventObject()
        if cmbOrigen.GetValue() == "Otra fecha":
            dlg = wx.TextEntryDialog(self,
                "Ingrese la fecha del 1 al 31",
                "Otra fecha", style=wx.OK | wx.CANCEL )
            if dlg.ShowModal() == wx.ID_OK:
                try:
                    if int(dlg.GetValue()) and int(dlg.GetValue()) <=31:
                        print dlg.GetValue()
                        cmbOrigen.AppendItems([dlg.GetValue()])
                        cmbOrigen.SetSelection(cmbOrigen.GetCount()-1)
                    else:
                        self.seleccionEnel(event)
                except ValueError:
                    print "Error de valor"
                    self.seleccionEnel(event)
            dlg.Destroy()
        try:
            if event.GetEventObject().GetValue().isnumeric():
                self.cmbDia.SetSelection(0)
            else:
                self.cmbDia.SetSelection(1)
        except:
            print "Error en el valor"
            
    
    
    def seleccionDia(self, event):
        if event.GetEventObject().GetValue() == u"Dia":
            self.cmbEnel.SetSelection(6)   

    
    def agregarExcepcion(self, event):
        guiRepeticiones = GUIExcepciones(self)
        guiRepeticiones.CenterOnParent()
        guiRepeticiones.ShowModal()
        guiRepeticiones.Destroy()
        
    
    def cambiarExcepcion(self, event):
        if self.listaExcepciones.GetCount() > 0:
            guiRepeticiones = GUIExcepciones(self)
            fechaEditar = self.listaExcepciones.GetString(self.listaExcepciones.GetSelection())
            fechaEditar=fechaEditar.split("/")
            print fechaEditar
            guiRepeticiones.editarFecha(fechaEditar)
            guiRepeticiones.CenterOnParent()
            guiRepeticiones.ShowModal()
            guiRepeticiones.Destroy()
        
        
    def eliminarExcepcion(self, event):
        if self.listaExcepciones.GetCount() > 0:
            self.listaExcepciones.Delete(self.listaExcepciones.GetSelection())
            
    
    def actualizarVistaPrevia(self):
        self.calendario1.SetDateRange(wx.DateTime(8, 12, 2017, 0, 0, 0, 0), wx.DateTime(15, 12, 2017, 0, 0, 0, 0))





class GUIExcepciones(wx.Dialog):
    #Ventana excepciones
    def __init__(self, parent):
        size=(200, 200)
        super(GUIExcepciones, self).__init__(parent, wx.ID_ANY, u'Excepciones', size=size)
        self.modoEdicion = False #Por defecto no esta en modo edicion
        self.crearGUI()
        
        
    def crearGUI(self):
        #self.panelPrincipal = wx.Panel(self, wx.ID_ANY)
        import locale
        locale.nl_langinfo(locale.D_FMT)
        self.calendarioSeleccionDia = widget.DatePickerCtrlV3(self)
        btnAceptar = wx.Button(self, wx.ID_OK, "Aceptar")
        btnAceptar.SetDefault()
        btnAceptar.Bind(wx.EVT_BUTTON, self.getFechaSeleccionada)
        btnCancelar = wx.Button(self, wx.ID_CANCEL, "Cancelar")
        sizerPrincipal = wx.BoxSizer(wx.VERTICAL)
        botonera = wx.BoxSizer(wx.HORIZONTAL)
        
        botonera.Add(btnCancelar)
        botonera.Add(btnAceptar)
                
        sizerPrincipal.Add(self.calendarioSeleccionDia, 0, wx.EXPAND)
        sizerPrincipal.Add(botonera, 0, wx.ALIGN_CENTER)
        #self.SetSizer(sizerPrincipal)
        #self.panelPrincipal.SetSizerAndFit(sizerPrincipal)
        self.SetSizerAndFit(sizerPrincipal)
        
    
    def getFechaSeleccionada(self, event):
        #Agrego la fecha seleccionada en el dialogo padre
        fechaSeleccionada = self.calendarioSeleccionDia.GetValue()
        #print x.GetDay(), x.GetMonth()+1, x.GetYear()
        #print str(x.GetDay()) + "/" + str(x.GetMonth()+1) + "/"+ str(x.GetYear())
        #return str(x.GetDay()) + "/" + str(x.GetMonth()+1) + "/"+ str(x.GetYear())
        if fechaSeleccionada == "":
            return
        if self.modoEdicion == True:
            self.Parent.listaExcepciones.Delete(self.Parent.listaExcepciones.GetSelection())
            #self.Parent.listaExcepciones.Append(str(x.GetDay()) + "/" + str(x.GetMonth()+1) + "/"+ str(x.GetYear()))
            self.Parent.listaExcepciones.Append(fechaSeleccionada)
        else:
            #self.Parent.listaExcepciones.Append(str(x.GetDay()) + "/" + str(x.GetMonth()+1) + "/"+ str(x.GetYear()))
            self.Parent.listaExcepciones.Append(fechaSeleccionada)
        self.Destroy()
        
    
    def editarFecha(self, fecha):
        self.modoEdicion = True
        val = wx.DateTimeFromDMY(int(fecha[0]), int(fecha[1]), int(fecha[2]))
        self.calendarioSeleccionDia.SetValue(val)
        



app = wx.App()
ventana = RepeticionPuntosAperturas(None)
#ventana = GUIExcepciones(None)
ventana.CenterOnScreen()
ventana.ShowModal()
#ventana.Show()
ventana.Destroy()
app.MainLoop()



