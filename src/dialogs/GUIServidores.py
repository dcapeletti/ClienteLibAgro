'''
Created on 22/09/2016

@author: diegoariel
'''
import wx, MySQLdb, wx.lib.intctrl, wx.grid
import service_bd

class GUIServidor(wx.Dialog):
    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, wx.ID_ANY, "Lista de servidores")
        self.bd = service_bd.ConfiguracionCliente()
        # realizamos la consulta sql
        result_lista_servers = self.bd.imprimirServidores()
        # Definimos el panel_grilla principal
        panel_grilla = wx.Panel(self, -1)
        vbox = wx.BoxSizer(wx.VERTICAL)

        # Anadimos el grid
        self.grid = wx.grid.Grid(panel_grilla, -1, size=(670,250))
        self.grid.EnableEditing(True)
        self.grid.SetToolTipString("Presione la tecla Supr para eliminar un registro.")

        # creamos las filas y columnas
        self.grid.CreateGrid(len(result_lista_servers),4)

        # definimos los campos que contiene el grid
        self.grid.SetColLabelValue(0, 'Id')
        self.grid.SetColLabelValue(1, 'Servidor')
        self.grid.SetColLabelValue(2, 'Puerto')
        self.grid.SetColLabelValue(3, 'Descripcion')

        # definimos los tamanos
        self.grid.SetRowLabelSize(50)
        self.grid.SetColSize(0,30)
        self.grid.SetColSize(1,150) 
        self.grid.SetColSize(2,80)
        self.grid.SetColSize(3,300)

        gridTableSizer = wx.BoxSizer(wx.HORIZONTAL)
        gridTableSizer.Add(self.grid, wx.ALIGN_CENTER | wx.ALL,0)
        self.Bind(wx.grid.EVT_GRID_CELL_CHANGE, self.CellContentsChanged)
        self.grid.Bind(wx.grid.EVT_GRID_SELECT_CELL, self.informarFila)
        self.grid.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown) #Manejo el tab, enter y del        

        # Anadimos los campos de la base de datos a las columnas

        for i in range(0,len(result_lista_servers)):
            # anadimos las columnas
            self.grid.SetCellValue(i,0,"%s" % result_lista_servers[i][0])
            self.grid.SetCellValue(i,1,"%s" % result_lista_servers[i][1])
            self.grid.SetCellValue(i,2,"%s" % result_lista_servers[i][2])
            self.grid.SetCellValue(i,3,"%s" % result_lista_servers[i][3])
            self.grid.SetReadOnly(i, 0, True) #col Id no editable           
 
        #Add buttons
        BtnSizer = wx.BoxSizer(wx.HORIZONTAL)
        closeBtn = wx.Button(panel_grilla, wx.ID_OK)
        #closeBtn.Bind(wx.EVT_BUTTON, self.onClose) #para sistemas Windows no tiene que llevar este evento, funciona igualmente...
        BtnSizer.Add(closeBtn, 1, wx.ALL, 5)
        
        vbox.Add(gridTableSizer, 0, wx.ALIGN_CENTER | wx.ALL, 5)
        vbox.Add(BtnSizer, 0, wx.ALIGN_RIGHT | wx.ALL, 5)
        panel_grilla.SetSizer(vbox)
        vbox.Fit(self)
        self.Layout()
        

    def CellContentsChanged(self, event):
        col = event.GetCol()
        row = event.GetRow()
        val = self.grid.GetCellValue(row, col) #obtiene el valor de la celda que ha cambiado...
        if val == "":
            val = "null"
        ColLabel = self.grid.GetColLabelValue(col)    
        self.bd.getBD().cursor().execute("UPDATE servidor SET "+ColLabel+"=? WHERE Id =?", (val, self.grid.GetCellValue(row, 0))) # protects against injection of dangerous 'val'
        self.bd.getBD().commit()
        self.grid.SetCellValue(row, col, val) #Si esta en la ultima fila, agrego una fila e inserto un registro...
        if row == (self.grid.GetNumberRows()-1):
            self.grid.AppendRows(1)
            self.bd.getBD().cursor().execute("Insert into servidor(Servidor, Puerto, Descripcion) values ('', '', '')")
            newId = int(self.grid.GetCellValue(row, 0))+1
            self.grid.SetCellValue(row+1, 0, "%s" % newId)
            self.grid.SetReadOnly(row+1, 0, True) #Col Id no editable
        
            
    
    def informarFila(self, event):
        self.fila= event.GetRow()
        self.currentlySelectedCell = (event.GetRow(),event.GetCol())
        event.Skip()


    def OnKeyDown(self, evt):
        if evt.GetKeyCode() == 127: #Tecla Supr
            self.bd.getBD().cursor().execute("Delete from servidor Where Id=?", (self.grid.GetCellValue(self.fila, 0),))
            self.bd.getBD().commit()
            self.grid.DeleteRows(self.fila)
            evt.Skip()
            return
        
        if evt.GetKeyCode() != wx.WXK_RETURN: #Tecla enter
            evt.Skip()
            return
        
        if evt.ControlDown():   # the edit control needs this key
            evt.Skip()
            return
        

        self.grid.DisableCellEditControl()
        success = self.grid.MoveCursorRight(evt.ShiftDown())

        if not success:
            newRow = self.grid.GetGridCursorRow() + 1

            if newRow < self.grid.GetTable().GetNumberRows():
                self.grid.SetGridCursor(newRow, 0)
                self.grid.MakeCellVisible(newRow, 0)
            else:
                # this would be a good place to add a new row if your app
                # needs to do that
                pass
