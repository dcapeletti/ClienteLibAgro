# encoding=utf-8
'''
Created on 21/09/2016

@author: diegoariel

Crear los paneles y poner en cada objeto como padre.
Crear lo sizer.
Agregar los objetos a los sizer.
Establecer los sizer a los paneles creados en el punto 1.

El sizer padre de todos que se establece al Frame, debe contener a los paneles.
'''

import wx
import service_bd
from variablesutiles import AdminVariables

class GUILogin(wx.Dialog):
    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, wx.ID_ANY, 'Ingreso al sistema')
        self.crearGUI()
        
    def crearGUI(self):
        panel = wx.Panel(self, wx.ID_ANY)
        lblServidor = wx.StaticText(panel, -1, u'Máquina:')
        self.lista_servidores = wx.ComboBox(panel, -1)
        lblUsuario=wx.StaticText(panel, -1, 'Usuario:')
        txtUsuario = wx.TextCtrl(panel, -1, size=(185, 25))
        lblContrasena = wx.StaticText(panel, -1, u'Cntraseña:')
        txtContrasena = wx.TextCtrl(panel, -1, size=(185, 25), style=wx.TE_PASSWORD)
        btn_Cambiar_server = wx.Button(panel, wx.ID_ANY, u'Agregar máquina')
        btn_Cambiar_server.Bind(wx.EVT_BUTTON, self.cambiarServer)
        self.btn_Aceptar=wx.Button(panel, wx.ID_OK)
        self.btn_Aceptar.SetDefault()
        btn_Cancelar=wx.Button(panel, wx.ID_CANCEL)
        
        sizer = wx.GridBagSizer(3,3) # Es uno de los sizer mas completos, pero el mas complejo
        sizer.Add(lblServidor, pos=(0, 0), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizer.Add(self.lista_servidores, pos=(0, 1), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizer.Add(btn_Cambiar_server, pos=(0, 2), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizer.Add(lblUsuario, pos=(1,0), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizer.Add(txtUsuario, pos=(1,1), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizer.Add(lblContrasena, pos=(2,0), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizer.Add(txtContrasena, pos=(2,1), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        
       
        sizer.Add(btn_Cancelar, pos=(3,1), flag=wx.TOP|wx.RIGHT|wx.ALIGN_RIGHT, border=4)
        sizer.Add(self.btn_Aceptar, pos=(3,2), span=(1, 1), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        
        
        panel.SetSizer(sizer)
        
        #self.SetSizer(sizer)
        sizer.Fit(self)
        self.Layout()
        self.base_dato = service_bd.ConfiguracionCliente()
        self.cargarListaServidores()
        self.lista_servidores.Select(self.lista_servidores.Count-2)
        
    def cambiarServer(self, event):
        print "Cambiando servidor"
        import GUIServidores
        gui_admin_servers = GUIServidores.GUIServidor(None)
        gui_admin_servers.CenterOnScreen()
        gui_admin_servers.ShowModal()
        gui_admin_servers.Destroy()#Cuando se cierra, limpio la lista    
        self.cargarListaServidores() #cargo nuevamente
        self.lista_servidores.Select(self.lista_servidores.Count-2)
        #dlg.Destroy()
        
    
    def cargarListaServidores(self):
        self.lista_servidores.Clear()
        lista_servers= self.base_dato.imprimirServidores()
        for server in lista_servers:
            self.lista_servidores.AppendItems([server[1]])
            
    
            
    def getServidor(self):
        return self.lista_servidores.GetValue()
        
        
'''        
app = wx.App()
ventana = GUILogin(None)
ventana.CenterOnScreen()
ventana.ShowModal()
#ventana.Show()
ventana.Destroy()
app.MainLoop()
'''