# encoding=utf-8

'''
Created on 31/10/2016

@author: diegoariel

En este cuadro de dialogo se crea una configuración que determina la cantidad aproximada de alimento (Kg) 
que sale en una determinada cantidad de tiempo.

'''
import wx
from wx import BoxSizer
import MySQLdb  
from variablesutiles import AdminVariables
        
class ConfigParametroAlimento(wx.Dialog):
    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=-1, title=u"Parametrización Kg de alimentos")
        self.db= MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')
        self.cursor = self.db.cursor()
        
        #panel = ConfiguracionParametroAlimento(self)
        self.panel_controles=wx.Panel(self, -1)
        lbl_tipo_alimento=wx.StaticText(self.panel_controles, -1, "Tipo de alimento:")
        self.lst_tipo_alimento= wx.ComboBox(self.panel_controles, -1)
        self.lst_tipo_alimento.SetToolTipString("Si no existe el tipo de alimento, ingrese su nombre correctamente.")
        lbl_kg=wx.StaticText(self.panel_controles, -1, "Kg:")
        self.txt_kg=wx.TextCtrl(self.panel_controles, -1)
        lbl_tiempo_cierre=wx.StaticText(self.panel_controles, -1, "Tiempo de cierre:") #Especifica en cuantos seg. quiere que se produzca el cierre. La parte decimal es nanosegundos.
        self.txt_tiempo_cierre=wx.TextCtrl(self.panel_controles, -1)
        self.txt_tiempo_cierre.SetToolTipString(u"Especifique que tiempo de cierre en segundos. Si requiere mas presición utilice la parte decimal para milisegundos. Ejemplo 2.28")
        lbl_angulo_apertura=wx.StaticText(self.panel_controles, -1, "Apertura:")
        self.control_angulo_apertura=wx.Slider(self.panel_controles, wx.ID_ANY, 5, 5, 10, wx.DefaultPosition, wx.Size( 60,90 ), wx.SL_INVERSE|wx.SL_LABELS|wx.SL_VERTICAL )
        lbl_detalles=wx.StaticText(self.panel_controles, -1, "Detalles:")
        self.txt_detalles=wx.TextCtrl(self.panel_controles, -1, pos=wx.DefaultPosition, size=(180,50), style= wx.TE_MULTILINE | wx.SUNKEN_BORDER)
        btn_aceptar=wx.Button(self.panel_controles, wx.ID_OK)
        btn_aceptar.SetDefault()
        btn_aceptar.Bind(wx.EVT_BUTTON, self.guardarRegistroAlimento)
        btn_cancelar=wx.Button(self.panel_controles, wx.ID_CANCEL)
        btn_tiempo_ultima_apertura= wx.Button(self.panel_controles, -1, u"Leer última apertura")
        btn_tiempo_ultima_apertura.Bind(wx.EVT_BUTTON, self.obtenerUltimaApertura)
        
        cajaAgrupador = wx.StaticBox(self.panel_controles, -1, u'Prueba de compuerta')
        lbl_compuerta=wx.StaticText(self.panel_controles, -1, "Seleccione la compuerta:")
        self.lst_compuerta=wx.ComboBox(self.panel_controles, -1)
        btn_probar=wx.Button(self.panel_controles, -1, u"Probar configuración") #envia el mensaje para que se ejecute la acción.
        btn_probar.Bind(wx.EVT_BUTTON, self.enviarComandoPrueba)
        sizer_prueba=wx.BoxSizer(wx.VERTICAL)
        sizer_prueba.Add(lbl_compuerta, -1)
        sizer_prueba.Add(self.lst_compuerta, -1)
        sizer_prueba.Add(btn_probar, -1)
        sizerCajaAgrupadora = wx.StaticBoxSizer(cajaAgrupador)
        sizerCajaAgrupadora.Add(sizer_prueba, 0, wx.EXPAND|wx.ALL, 5)
        
    
        sizer_tiempo_cierre = wx.BoxSizer(wx.HORIZONTAL)
        sizer_tiempo_cierre.Add(lbl_tiempo_cierre, -1)
        sizer_tiempo_cierre.Add(self.txt_tiempo_cierre, -1, wx.LEFT, 2)
        sizer_tiempo_cierre.Add(btn_tiempo_ultima_apertura, -1)       
        
        sizer_controles=wx.GridBagSizer(4, 2) #sizer de los controles a la izqueirda
        sizer_controles.Add(lbl_tipo_alimento, pos=(0, 0), flag=wx.ALIGN_CENTRE_VERTICAL|wx.LEFT|wx.BOTTOM, border=4)
        sizer_controles.Add(self.lst_tipo_alimento, pos=(0, 1), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizer_controles.Add(lbl_angulo_apertura, pos=(1,0), flag=wx.ALIGN_CENTRE_VERTICAL|wx.LEFT|wx.BOTTOM, border=4)
        sizer_controles.Add(self.control_angulo_apertura, pos=(1,1), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        #sizer_controles.Add(lbl_tiempo_cierre, pos=(2,0), flag=wx.ALIGN_CENTRE_VERTICAL|wx.LEFT|wx.BOTTOM, border=4)
        #sizer_controles.Add(self.txt_tiempo_cierre, pos=(2,1), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        #sizer_controles.Add(btn_tiempo_ultima_apertura, pos=(2,2), span=wx.GBSpan(2,2), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)      
        sizer_controles.Add(sizer_tiempo_cierre, pos=(2,0), span=wx.GBSpan(2,2), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizer_controles.Add(lbl_kg, pos=(4, 0), flag=wx.ALIGN_CENTRE_VERTICAL|wx.LEFT|wx.BOTTOM, border=4)
        sizer_controles.Add(self.txt_kg, pos=(4, 1), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        sizer_controles.Add(lbl_detalles, pos=(5,0), flag=wx.ALIGN_CENTRE_VERTICAL|wx.LEFT|wx.BOTTOM, border=4)
        sizer_controles.Add(self.txt_detalles, pos=(5,1), flag=wx.TOP|wx.LEFT|wx.BOTTOM, border=4)
        
        sizer_botones=wx.BoxSizer(wx.HORIZONTAL)
        sizer_botones.Add(btn_aceptar)
        sizer_botones.Add(btn_cancelar)
        
        sizer_controles_mas_prueba=BoxSizer(wx.HORIZONTAL)
        sizer_controles_mas_prueba.Add(sizer_controles, 0, wx.ALL, 5)
        sizer_controles_mas_prueba.Add(sizerCajaAgrupadora)
        
        main_sizer2=wx.BoxSizer(wx.VERTICAL)
        main_sizer2.Add(sizer_controles_mas_prueba)
        main_sizer2.Add(sizer_botones, 0, wx.ALIGN_RIGHT, 5)
                
        self.panel_controles.SetSizer(main_sizer2)
        main_sizer2.Fit(self)
        self.cargarTipoAlimentos()
        self.cargarListaCompuertas()
        self.Layout()
        self.Centre()
        
        
    def obtenerUltimaApertura(self, event):
        adminvariables= AdminVariables()
        val = adminvariables.getConexionBd().getTiempoUltimaApertura()
        self.txt_tiempo_cierre.SetValue(val)
        
        
    def cargarTipoAlimentos(self):
        #Esto debe ser parte del servivio de BD
        self.cursor.execute("Select Nombre from ALIMENTOS")
        resultado = self.cursor.fetchall()
        for x in range(0, len(resultado)):
            #print resultado[x][0]
            self.lst_tipo_alimento.AppendItems([resultado[x][0]])
            
    
    def cargarListaCompuertas(self):
        #Esto debe ser parte del servivio de BD
        self.cursor.execute("Select Id_compuerta from COMPUERTAS")
        resultado = self.cursor.fetchall()
        for x in range(0, len(resultado)):
            #print resultado[x][0]
            self.lst_compuerta.AppendItems(str(resultado[x][0]))
            
    
    def enviarComandoPrueba(self, event):
        if self.control_angulo_apertura.Value > 5 and self.txt_tiempo_cierre.GetValue() != "" and self.lst_compuerta.GetValue() != "": #Si es 5, la compuerta esta cerrada...
            print "Enviando el comando..." #Hay que enviar el mensaje al mediador...
        else:
            print "Hay que rellenar campos..."
            dlg = wx.MessageDialog(self,u'Dede completar los campos Tiempo de cierre, seleccionar una compuerta de la lista y elevar el ángulo de apertura de la misma.', u'Faltan datos', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
            
            
    def guardarTipoAlimento(self, alimento):
        #Guardar este registro debe ser parte del servivio de BD
        if alimento == "": return
        if alimento in  self.lst_tipo_alimento.GetItems():
            pass #si el alimento se encuentra en la lista no lo guardo...
        else:
            self.cursor.execute("Insert into ALIMENTOS(Nombre) values('"+alimento+"')")
            self.db.commit()
            
    
    def guardarRegistroAlimento(self, event):
        if self.control_angulo_apertura.Value > 5 and self.txt_tiempo_cierre.GetValue() != "" and self.lst_tipo_alimento.GetValue() != "" and self.txt_kg.GetValue() !="": #Si es 5, la compuerta esta cerrada...
            print "Guardado el registro de alimento..."
            self.guardarTipoAlimento(self.lst_tipo_alimento.GetValue())
            #Guardar este registro debe ser parte del servivio de BD.
            self.cursor.execute("Select id_ALIMENTO from ALIMENTOS Where Nombre='"+self.lst_tipo_alimento.GetValue()+"'")
            id_tipo_alimento = self.cursor.fetchall()
            #print id_tipo_alimento[0][0]
            sql = "Insert into CONFIG_PARAMETROS_KILOS_ALIMENTOS(Tipo_alimento, Kg, Cierre, Apertura, Detalles) values("+str(id_tipo_alimento[0][0])+", "+str(self.txt_kg.GetValue())+", "+str(self.txt_tiempo_cierre.GetValue())+", "+str(self.control_angulo_apertura.GetValue())+", '"+self.txt_detalles.GetValue()+"')"
            print sql
            self.cursor.execute(sql)
            self.db.commit()
            self.Destroy()
        else:
            dlg = wx.MessageDialog(self, u'Debe completar la siguiente información para guardar el registro: Tipo de alimento, Apertura, Tiempo de cierre y Kg.', u'Faltan datos', wx.OK|wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
            
        
        
'''
app=wx.App()
frame = ConfigParametroAlimento(None)
frame.Show()
app.MainLoop()
'''

    