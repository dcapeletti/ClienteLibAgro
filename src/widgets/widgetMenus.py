# encoding=utf-8
'''
Created on 19/10/2015

@author: diegoariel
'''

import wx
from src.widgets.widget import Widget
from variablesutiles import AdminVariables
from chatnetworking import defaulthost, ChatConnect

#------------------------------------------------------------------------------------

class MenuBar(wx.MenuBar, Widget):
    def __init__(self, parent, mediador):
        wx.MenuBar.__init__(self)
        self.frame = parent #Es el Frame contenedor
        self.mediador = mediador
        self.adminvariables = AdminVariables() #Es un singleton que contiene variables útiles
        
        self.mnuServidor = wx.Menu() 
        #1, 2 son Id de los menues
        mnuCambiarServidor = self.mnuServidor.Append(1, '&Establecer servidor...',
                'Establecer o cambiar el nombre de servidor.')
        mnuConectarServidor = self.mnuServidor.Append(2, '&Conectar al servidor', 'Edita un archivo')
        
        self.frame.Bind(wx.EVT_MENU, self.setHostServer, mnuCambiarServidor)
        self.frame.Bind(wx.EVT_MENU, self.conectar, mnuConectarServidor)
        self.Append(self.mnuServidor, '&Servidor')
        
        self.addMenuAyuda()
        #self.connected=False
        
    
    def setDefaultHost(self, event):
        self.adminvariables.setHostServer(defaulthost)
        self.notificarCambios(event)
        
        
    def setHostServer(self, event):
        print "Cambiando el nombre del servidor..."
        dlg = wx.TextEntryDialog(self,
                "Ingresar el nombre o el IP del servidor",
                "Nuevo servidor de comedor electrónico", style=wx.OK | wx.CANCEL )
        if dlg.ShowModal() == wx.ID_OK:
            self.adminvariables.setHostServer(dlg.GetValue())
        dlg.Destroy()
        self.notificarCambios(event)
        
    
    def abrirAcercaDe(self, event):
        self.notificarCambios(event)
        
        
    def conectar(self, event):
        self.notificarCambios(event)
        
    def agregarTodosLosMenus(self):
        if self.FindItemById(3) == None: #Verifico si no se ha cargado alguna ves el menu Tareas programadas...Así evito cargarlos a todos de nuevo.
            self.addMenuTareasProgramadas()
            self.addMenuConfiguracion()   
            self.Remove(1) #Elimino el menú ayuda
            self.addMenuAyuda() #Lo agrego en la ultima posicion
        
    def abrirDialogoTareasProgramadas(self, event):
        self.notificarCambios(event)     
        
    
    def addMenuTareasProgramadas(self):
        #Agrega un nuevo menu para tareas programadas.
        self.mnuTareasProgramadas = wx.Menu()
        mnuCambiarTareasProgramadas =  self.mnuTareasProgramadas.Append(3, '&Tareas programadas...',
                u'Permite establecer tareas para que las máquinas ejecuten..')
        
        self.frame.Bind(wx.EVT_MENU, self.abrirDialogoTareasProgramadas, mnuCambiarTareasProgramadas)
        self.Append(self.mnuTareasProgramadas, "Tareas programadas")
        
    def addMenuConfiguracion(self):
        #Agrega un nuevo menu para tareas programadas.
        self.mnuConfiguracion = wx.Menu()
        mnuItemCofiguracionNotificaciones =  self.mnuConfiguracion.Append(4, u'&Configuración de notificaciones...',
                'Permite configurar las notificaciones de eventos del sistema.')
        #Agrego el menu para configuracion del peso del alimento...
        mnuItemConfigParametrosAlimentos = self.mnuConfiguracion.Append(6, u'Configuración peso alimento...', 'Permite configurar el peso del alimento.')
        mnuItemConfigFechaYHora = self.mnuConfiguracion.Append(7, u'Configuración fecha y hora', 
                                        'Permite configurar la fecha y hora del servidor...')
        
        
        self.frame.Bind(wx.EVT_MENU, self.abrirDialogoConfiguracion, mnuItemCofiguracionNotificaciones)
        self.frame.Bind(wx.EVT_MENU, self.abrirDialogoConfigPeso, mnuItemConfigParametrosAlimentos)
        self.frame.Bind(wx.EVT_MENU, self.abrirDialogoConfigFechaYhora, mnuItemConfigFechaYHora)
        self.Append(self.mnuConfiguracion, u"Configuración")
        
    def addMenuAyuda(self):
        self.mnuAyuda = wx.Menu() 

        mnuItemAyuda = self.mnuAyuda.Append(5, '&Acerca de...', 'Acerca de LibAgro')
        
        self.frame.Bind(wx.EVT_MENU, self.abrirAcercaDe, mnuItemAyuda)
        self.Append(self.mnuAyuda, '&Ayuda')

        
    def abrirDialogoConfiguracion(self, event):
        self.notificarCambios(event)
        
        
    def abrirDialogoConfigPeso(self, event):
        self.notificarCambios(event)
        
    
    def abrirDialogoConfigFechaYhora(self, event):
        self.notificarCambios(event)
        
    
    def notificarCambios(self, evento):
        #Se tiene que enviar notificaciones del evento al mediador...
        self.mediador.widgetModificado(self, evento)
   
   
#------------------------------------------------------------------------------------
