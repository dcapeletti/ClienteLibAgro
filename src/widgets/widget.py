# encoding=utf-8
'''
Created on 17/10/2015

@author: diegoariel

En este módulo se deberá implementar el patrón MEDIADOR entre los objetos que forman
parte de la vista (Interfaz gráfica). Entre estos objetos pueden estar los menúes, botoneras,
etc.
'''
import wx
from EnhancedStatusBar import *
import wx.lib.agw.peakmeter as PM
import service_bd
from variablesutiles import AdminVariables
import MySQLdb
import locale
#from mediador import DirectorView

xmax = 500
ymax = 500
MAIN_WINDOW_DEFAULT_SIZE = (xmax,ymax)

eventDict = {}

class Widget():

    def notificarCambios(self, evento):
        raise NotImplemented('Este método es abstracto. Debe sobrescribirlo.')

#-----------------------------------------------------------------------------------
class App(wx.App):

    def OnInit(self):
        self.__cargarEventos()
        return True

    def __cargarEventos(self):
        """
        Carga los eventos en un diccionario para luego buscar que evento se ha lanzado...
        """
        for name in dir(wx):
            if name.startswith('EVT_'):
                evt = getattr(wx, name)
                if isinstance(evt, wx.PyEventBinder):
                    eventDict[evt.typeId] = name


    def setFrame(self, frame):
        self.__frame = frame


    def getFrame(self):
        return self.__frame




#-----------------------------------------------------------------------------------
class FrameSensor(wx.Frame, Widget):
    def __init__(self, parent, id, titulo, mediador):
        estilo = wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, parent, id, title=titulo, size=MAIN_WINDOW_DEFAULT_SIZE, style=estilo)
        self.mediador = mediador
        self.Center()
        #wx.BeginBusyCursor() #http://xoomer.virgilio.it/infinity77/wxPython/wxFunctions.html#BeginBusyCursor
        self.panel = wx.Panel(self, -1)
        self.Bind(wx.EVT_CLOSE, self.OnExit) #Paso el método/función definido para el evento CLOSE

        from src.widgets.widgetMenus import MenuBar
        self.menuBar = MenuBar(self, self.mediador) #Es el barra de menú
        self.SetMenuBar(self.menuBar) #Establecemos la barra de menú

        #self.barraEstado = BarraEstado(self) #Es la barra de estado
        self.barraEstado = EnhancedStatusBar(self, -1) #Es la barra de estado
        self.barraEstado.SetSize((-1, 25))
        self.barraEstado.SetFieldsCount(2)

        self.barraEstado.SetStatusWidths([-1, 60]) #ancho de las divisiones

        self.SetStatusBar(self.barraEstado)


    def OnExit(self, event):
        self.notificarCambios(event)
        self.Destroy()


    def notificarCambios(self, evento):
        #Se tiene que enviar notificaciones del evento al mediador...
        self.mediador.widgetModificado(self, evento)
        #print evento

#---------------------------------------------------------------------------------------

class Autenticacion(wx.Panel, Widget):
    def __init__( self, parent, mediador):
        #wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( xmax,ymax ), style = wx.TAB_TRAVERSAL | wx.ALIGN_CENTER)
        wx.Panel.__init__(self,parent)
        self.mediador = mediador
        #self.SetBackgroundColour(wx.RED)

        main_box_sizer = wx.BoxSizer( wx.HORIZONTAL) #Sizer principal. Agrupa la distribución principal de componentes

        self.panel_sensor_distancia = SensorNivelAlimento(parent, mediador)

        self.panel_sensor_puertas = SensorPuertas(parent, mediador)

        self.panel_control_motor = PanelSensorCompuerta(parent, mediador)


        main_box_sizer.Add(self.panel_sensor_distancia,0,wx.ALL|wx.EXPAND,5)
        main_box_sizer.Add(self.panel_sensor_puertas,0,wx.ALL,5)
        main_box_sizer.Add(self.panel_control_motor,1,wx.ALL|wx.EXPAND,5)

        self.SetSizer(main_box_sizer)
        main_box_sizer.Fit(self)
        parent.SetSizeWH(700,350) #Luego de crear todos los componentes, ajusto el tamaño.
        parent.CenterOnScreen() #Centro en la pantalla.
        self.Layout()


    def __del__( self ):
        pass

    def notificarCambios(self, evento):
        self.mediador.widgetModificado(self, evento)

    def getPanelSensorAlimento(self):
        return self.panel_sensor_distancia

    def getPanelSensorPuerta(self):
        return self.panel_sensor_puertas

    def getPanelMotor(self):
        return self.panel_control_motor


#------------------------------------------------------------------------------------

class SensorNivelAlimento(wx.Panel, Widget):
    def __init__(self, parent, mediador):
        wx.Panel.__init__(self, parent)
        self.mediador = mediador

        self.lbl_alimento = wx.StaticText(self, label="Nivel del alimento")
        self.btn_actualizar = wx.Button(self, label="Actualizar")
        self.btn_actualizar.SetName("btnActualizarSensorAlimento")
        self.grafico = PM.PeakMeterCtrl(self, -1, style=wx.SIMPLE_BORDER, agwStyle=PM.PM_VERTICAL) #SensorProximidad(parent, mediador)
        self.grafico.SetBandsColour('red', 'yellow', 'green')
        self.grafico.SetMeterBands(1, 50) #dos bandas de 50 lineas
        #self.vertPeak.SetBackgroundColour('black')
        self.grafico.SetRangeValue(20-0, 60-20, 200-100) #
        self.grafico.SetInitialSize((80, 250)) #Tamanio inicial

        sizer_sensor_proximidad = wx.BoxSizer(wx.VERTICAL)
        sizer_sensor_proximidad.Add(self.lbl_alimento, 1, wx.ALIGN_CENTRE)
        sizer_sensor_proximidad.Add(self.grafico, 0, wx.ALIGN_CENTRE)
        sizer_sensor_proximidad.Add(self.btn_actualizar, 0, wx.EXPAND)

        self.btn_actualizar.Bind( wx.EVT_BUTTON, self.pp)

        self.SetSizer(sizer_sensor_proximidad)


    def notificarCambios(self, evento):
        self.mediador.widgetModificado(self, evento)


    def getGrafico(self):
        return self.grafico


    def pp(self, event):
        print "Me has apretado"
        wx.BeginBusyCursor() #Cursor en estado procesando
        self.notificarCambios(event)


    def setPos(self, distancia):
        self.grafico.SetData([distancia], 0, 50)


#------------------------------------------------------------------------------------

class SensorPuertas(wx.Panel, Widget):
    '''Esta clase forma parte de la vista de los sensores botones que vigilan el estado de las puertas...
    '''
    def __init__(self, parent, mediador):
        wx.Panel.__init__(self, parent)
        self.mediador = mediador

        self.lbl_puertas = wx.StaticText(self, label="Estado de puertas")

        self.rectangulo = wx.Panel(self, -1, size=(80, 100))
        self.rectangulo.SetBackgroundColour(wx.GREEN)
        self.lbl_estado_actual=wx.StaticText(self.rectangulo, label="Cerrada", size=self.rectangulo.GetSize())
        self.lbl_estado_actual.SetFont(wx.Font(16, wx.ROMAN, wx.SLANT, wx.NORMAL))
        panel_estado_puerta = wx.BoxSizer(wx.VERTICAL)
        panel_estado_puerta.Add(self.lbl_estado_actual, 0, wx.EXPAND)
        self.rectangulo.SetSizer(panel_estado_puerta)

        #self.rectangulo.AddChild(self.lbl_estado_actual)

        self.lbl_ultima_apertura = wx.StaticText(self, label="La última apertura \nfue el día 12/10/2015\n a las 15:35 Hs")

        sizer_puertas = wx.BoxSizer(wx.VERTICAL)
        sizer_puertas.Add(self.lbl_puertas, 0,wx.ALIGN_CENTRE) # el 1 agrega espacio en el próximo componente que se agrega
        sizer_puertas.Add(self.rectangulo, 0, wx.ALIGN_CENTRE)
        sizer_puertas.Add(self.lbl_ultima_apertura, 0,wx.ALIGN_CENTRE)

        self.SetSizer(sizer_puertas)


    def __del__( self ):
        pass


    def notificarCambios(self, evento):
        pass


    def cambiarEstado(self, nuevo_estado, fecha):
        if nuevo_estado == 0: #0 es Cerrada
            self.rectangulo.SetBackgroundColour(wx.GREEN)
            self.lbl_estado_actual.SetLabel("Cerrada")
        else:
            self.rectangulo.SetBackgroundColour(wx.RED)
            self.lbl_estado_actual.SetLabel("Abierta")
            import vlc
            alerta = vlc.MediaPlayer("./sound/campana.mp3")
            alerta.play()

        #ultima_apertura = self.mediador.getAdminVariable().getConexionBd().getUltimaAperturaPuertaTolva()
        #res = ultima_apertura.strftime('%d/%m/%Y')
        self.lbl_ultima_apertura.SetLabel("Último evento \nfue el día " + fecha)



#----------------------------------------------------------------------------------


class PanelSensorCompuerta(wx.Panel, Widget):
    #Panel que funciona como sensor y actuador.
    def __init__(self, parent, mediador):
        #wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.TAB_TRAVERSAL )
        wx.Panel.__init__(self, parent)
        self.mediador = mediador
        self.font_titulo = wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.NORMAL, wx.NORMAL)
        #--------------COMP1------------------
        agrupador_comp1 = wx.BoxSizer( wx.VERTICAL )
        self.lbl_compuerta1 = wx.StaticText(self, wx.ID_ANY, u"Compuerta 1")
        #self.lbl_control_maestro.Wrap( -1 )
        agrupador_comp1.Add( self.lbl_compuerta1, 0, wx.ALL|wx.ALIGN_CENTRE, 5 )

        self.control_posicion_motor1 = wx.Slider(self, -1, style=wx.SL_INVERSE|wx.SL_LABELS|wx.SL_VERTICAL)
        self.control_posicion_motor1.SetRange(3, 10)
        self.control_posicion_motor1.SetName("Control_M1")
        self.control_posicion_motor1.Bind(wx.EVT_SCROLL_CHANGED , self.mover_motor)

        pic_tarea_programada = wx.Bitmap("images/tareas_programadas.png", wx.BITMAP_TYPE_ANY)
        btnConfTareaComp1=wx.BitmapButton(self, -1, pic_tarea_programada)
        btnConfTareaComp1.SetName("Comp1")
        btnConfTareaComp1.Bind(wx.EVT_BUTTON, self.configurarCompuerta)
        
        pic_conf_compuerta = wx.Bitmap("images/config_compuerta.png", wx.BITMAP_TYPE_ANY)
        btnConfComp1 = wx.BitmapButton(self, -1, pic_conf_compuerta)
        btnConfComp1.SetName("Comp1")
        btnConfComp1.Bind(wx.EVT_BUTTON, self.configurarCompuerta)
        
        agrupadorBotonComp1 = wx.BoxSizer(wx.HORIZONTAL)
        agrupadorBotonComp1.Add(btnConfComp1, 0, wx.CENTER)
        agrupadorBotonComp1.Add(btnConfTareaComp1, 0, wx.CENTER)

        agrupador_comp1.Add( self.control_posicion_motor1, 1, wx.ALL|wx.EXPAND, 5 )
        agrupador_comp1.Add(agrupadorBotonComp1, 0, wx.ALL, 5 )


        #------------COMP2----------------
        agrupador_comp2 = wx.BoxSizer( wx.VERTICAL )
        self.lbl_compuerta2 = wx.StaticText(self, wx.ID_ANY, u"Compuerta 2")
        #self.lbl_control_maestro.Wrap( -1 )
        agrupador_comp2.Add( self.lbl_compuerta2, 0, wx.ALL|wx.ALIGN_CENTRE, 5 )

        self.control_posicion_motor2 = wx.Slider(self, -1, style=wx.SL_INVERSE|wx.SL_LABELS|wx.SL_VERTICAL)
        self.control_posicion_motor2.SetRange(3, 10)
        self.control_posicion_motor2.SetName("Control_M2")
        self.control_posicion_motor2.Bind(wx.EVT_SCROLL_CHANGED , self.mover_motor)
        
        btnConfTareaComp2=wx.BitmapButton(self, -1, pic_tarea_programada)
        btnConfTareaComp2.SetName("Comp2")
        btnConfTareaComp2.Bind(wx.EVT_BUTTON, self.configurarCompuerta)

        btnConfComp2 = wx.BitmapButton(self, -1, pic_conf_compuerta)
        btnConfComp2.SetName("Comp2")
        btnConfComp2.Bind(wx.EVT_BUTTON, self.configurarCompuerta)
        
        agrupadorBotonComp2 = wx.BoxSizer(wx.HORIZONTAL)
        agrupadorBotonComp2.Add(btnConfComp2, 0, wx.CENTER)
        agrupadorBotonComp2.Add(btnConfTareaComp2, 0, wx.CENTER)


        agrupador_comp2.Add( self.control_posicion_motor2, 1, wx.ALL|wx.EXPAND, 5 )
        agrupador_comp2.Add(agrupadorBotonComp2, 0, wx.ALL|wx.EXPAND, 5 )

        #-----------COMP3-------------------
        agrupador_comp3 = wx.BoxSizer( wx.VERTICAL )
        self.lbl_compuerta3 = wx.StaticText(self, wx.ID_ANY, u"Compuerta 3")
        #self.lbl_control_maestro.Wrap( -1 )
        agrupador_comp3.Add( self.lbl_compuerta3, 0, wx.ALL|wx.ALIGN_CENTRE, 5 )

        self.control_posicion_motor3 = wx.Slider(self, -1, style=wx.SL_INVERSE|wx.SL_LABELS|wx.SL_VERTICAL)
        self.control_posicion_motor3.SetRange(3, 10)
        self.control_posicion_motor3.SetName("Control_M3")
        self.control_posicion_motor3.Bind(wx.EVT_SCROLL_CHANGED , self.mover_motor)

        btnConfTareaComp3=wx.BitmapButton(self, -1, pic_tarea_programada)
        btnConfTareaComp3.SetName("Comp3")
        btnConfTareaComp3.Bind(wx.EVT_BUTTON, self.configurarCompuerta)

        btnConfComp3 = wx.BitmapButton(self, -1, pic_conf_compuerta)
        btnConfComp3.SetName("Comp3")
        btnConfComp3.Bind(wx.EVT_BUTTON, self.configurarCompuerta)
        
        agrupadorBotonComp3 = wx.BoxSizer(wx.HORIZONTAL)
        agrupadorBotonComp3.Add(btnConfComp3, 0, wx.CENTER)
        agrupadorBotonComp3.Add(btnConfTareaComp3, 0, wx.CENTER)

        agrupador_comp3.Add( self.control_posicion_motor3, 1, wx.ALL|wx.EXPAND, 5 )
        agrupador_comp3.Add(agrupadorBotonComp3, 0, wx.ALL|wx.EXPAND, 5 )

        #-----------comp4--------------------
        agrupador_comp4 = wx.BoxSizer( wx.VERTICAL )
        self.lbl_compuerta4 = wx.StaticText(self, wx.ID_ANY, u"Compuerta 4")
        #self.lbl_control_maestro.Wrap( -1 )
        agrupador_comp4.Add( self.lbl_compuerta4, 0, wx.ALL|wx.ALIGN_CENTRE, 5 )

        self.control_posicion_motor4 = wx.Slider(self, -1, style=wx.SL_INVERSE|wx.SL_LABELS|wx.SL_VERTICAL)
        self.control_posicion_motor4.SetRange(3, 10)
        self.control_posicion_motor4.SetName("Control_M4")
        self.control_posicion_motor4.Bind(wx.EVT_SCROLL_CHANGED , self.mover_motor)

        btnConfTareaComp4=wx.BitmapButton(self, -1, pic_tarea_programada)
        btnConfTareaComp4.SetName("Comp4")
        btnConfTareaComp4.Bind(wx.EVT_BUTTON, self.configurarCompuerta)

        btnConfComp4 = wx.BitmapButton(self, -1, pic_conf_compuerta)
        btnConfComp4.SetName("Comp4")
        btnConfComp4.Bind(wx.EVT_BUTTON, self.configurarCompuerta)
        
        agrupadorBotonComp4 = wx.BoxSizer(wx.HORIZONTAL)
        agrupadorBotonComp4.Add(btnConfComp4, 0, wx.CENTER)
        agrupadorBotonComp4.Add(btnConfTareaComp4, 0, wx.CENTER)
        
        agrupador_comp4.Add( self.control_posicion_motor4, 1, wx.ALL|wx.EXPAND, 5 )
        agrupador_comp4.Add(agrupadorBotonComp4, 0, wx.ALL|wx.EXPAND, 5 )
        #------------------------------------------

        agrupador_maestro = wx.BoxSizer( wx.VERTICAL )
        self.lbl_control_maestro = wx.StaticText(self, wx.ID_ANY, u"Maestro")
        #self.lbl_control_maestro.Wrap( -1 )
        agrupador_maestro.Add( self.lbl_control_maestro, 0, wx.ALL|wx.ALIGN_CENTRE, 5 )

        self.control_posicion_motor = wx.Slider(self, wx.ID_ANY, style=wx.SL_INVERSE|wx.SL_LABELS|wx.SL_VERTICAL )
        self.control_posicion_motor.SetRange(3, 10)
        self.control_posicion_motor.SetName("Control_Maestro")
        self.control_posicion_motor.Bind(wx.EVT_SCROLL_CHANGED , self.mover_motor)
        agrupador_maestro.Add( self.control_posicion_motor, 1, wx.ALL|wx.EXPAND, 5 )


        agrupador_controles_motores=wx.BoxSizer(wx.HORIZONTAL)
        agrupador_controles_motores.Add(agrupador_comp1, 0, wx.ALL|wx.EXPAND, 2)
        agrupador_controles_motores.Add(agrupador_comp2, 0, wx.ALL|wx.EXPAND, 2)
        agrupador_controles_motores.Add(agrupador_comp3, 0, wx.ALL|wx.EXPAND, 2)
        agrupador_controles_motores.Add(agrupador_comp4, 0, wx.ALL|wx.EXPAND, 2)
        agrupador_controles_motores.Add(agrupador_maestro, 0, wx.ALL|wx.EXPAND, 2)


        lbl_compuertas=wx.StaticText(self, wx.ID_ANY, "Compuertas")
        lbl_compuertas.SetFont(self.font_titulo)
        agrupador_principal=wx.BoxSizer(wx.VERTICAL)
        agrupador_principal.Add(lbl_compuertas, 0, wx.ALIGN_CENTRE, 5)
        agrupador_principal.Add(agrupador_controles_motores, 1, wx.ALL|wx.EXPAND, 5)

        self.SetSizer(agrupador_principal)
        self.Layout()


    def mover_motor(self, event):
        print "Cambio a " + str(self.control_posicion_motor.GetValue())
        self.notificarCambios(event)
        #keycode = event.GetKeyCode()
        #print "Tecleo en nombre de " + str(keycode)
        #if keycode == wx.WXK_TAB:
        #    print "Tab"

    def configurarCompuerta(self, event):
        #from src.dialogs.GUITareasProgramadas import ConfigTareasProgramadas
        from src.dialogs.GUIConfigCompuerta import GUIConfigCompuerta
        comp = event.GetEventObject().GetName()
        if comp == "Comp1":
            x = GUIConfigCompuerta(None, 1, 1, mediador=self.mediador) #ConfigTareasProgramadas(None, mediador=self.mediador, idCompuerta=1)
            x.ShowModal()
            x.Destroy()
        if comp == "Comp2":
            #x = ConfigTareasProgramadas(None, mediador=self.mediador, idCompuerta=2)
            x = GUIConfigCompuerta(None, 2, 1, mediador=self.mediador)
            x.ShowModal()
            x.Destroy()
        if comp == "Comp3":
            #x = ConfigTareasProgramadas(None, mediador=self.mediador, idCompuerta=3)
            x = GUIConfigCompuerta(None, 3, 1, mediador=self.mediador)
            x.ShowModal()
            x.Destroy()
        if comp == "Comp4":
            #x = ConfigTareasProgramadas(None, mediador=self.mediador, idCompuerta=4)
            x = GUIConfigCompuerta(None, 4, 1, mediador=self.mediador)
            x.ShowModal()
            x.Destroy()


    def notificarCambios(self, evento):
        self.mediador.widgetModificado(self, evento)


    def cambiarEstadoControlPuerta(self, valor, idCompuerta):
        #El control maestro #self.control_posicion_motor no debe cambiar de valor.
        if idCompuerta==1:
            self.control_posicion_motor1.SetValue(valor)
        elif idCompuerta==2:
            self.control_posicion_motor2.SetValue(valor)
        elif idCompuerta==3:
            self.control_posicion_motor3.SetValue(valor)
        elif idCompuerta==4:
            self.control_posicion_motor4.SetValue(valor)


    def cambiarTodasLasCompuertas(self, valor):
        self.control_posicion_motor1.SetValue(valor)
        self.control_posicion_motor2.SetValue(valor)
        self.control_posicion_motor3.SetValue(valor)
        self.control_posicion_motor4.SetValue(valor)


    def cambiarEstadoControlMaestro(self, valor):
        self.control_posicion_motor.SetValue(valor)



#------------------------------------------------------------------------------------

class SensorProximidad(wx.Panel, Widget):
    def __init__(self, parent, mediador):
        #wx.Panel.__init__(self, parent, id=wx.ID_ANY, size=(80, 110))
        wx.Panel.__init__(self, parent, size=(80, 100))

        self.parent = parent
        self.SetBackgroundColour('#000000')
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.pos=0

    def OnPaint(self, event):
        dc = wx.PaintDC(self)

        dc.SetDeviceOrigin(0, 100)
        dc.SetAxisOrientation(True, True)

        #pos = self.parent.GetParent().GetParent().sel
        #pos = 15
        rect = self.pos / 5

        for i in range(1, 21):
            if i > rect:
                dc.SetBrush(wx.Brush('#075100'))
        #dc.SetBrush(wx.Brush('#0751011'))
                dc.DrawRectangle(10, i*4, 30, 5)
                dc.DrawRectangle(41, i*4, 30, 5)
            else:
                dc.SetBrush(wx.Brush('#36ff27'))
        #dc.SetBrush(wx.Brush('#FFFF00'))
                dc.DrawRectangle(10, i*4, 30, 5)
                dc.DrawRectangle(41, i*4, 30, 5)

    def __del__(self):
        pass

    def notificarCambios(self, evento):
        self.mediador.widgetModificado(self, evento)

    def setPos(self, valor):
        self.pos = valor
        self.Refresh()


class DatePickerCtrlV2(wx.Panel):
    '''
    Esta clase crea una pantalla para introduccion de fechas.
    Se crea un combo que al hacer click sobre el, se despliega un calendario
    que no hace cambiar el tamano de la pantalla. Cuando se hace click sobre el,
    se establece la fecha en el combo y se oculta automaticamente.
    La diferencia de este calendario con el DatePickerCtrlV3 es que no cambia
    el tamano de la pantalla.
    '''
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.calendario = wx.DatePickerCtrl(self, -1)
        self.calendario.Bind(wx.EVT_DATE_CHANGED, self.cambiarControl)
        self.calendario.SetId(1)
        self.listaTexto = wx.ComboBox(self, -1)
        self.listaTexto.SetId(2)
        self.listaTexto.SetValue(self.calendario.GetValue().Format("%d/%m/%Y"))
        self.listaTexto.Bind(wx.EVT_LEFT_DOWN, self.cambiarControl)

        self.distribucion = wx.BoxSizer(wx.VERTICAL)
        self.distribucion.Add(self.listaTexto, 0, wx.EXPAND)
        self.distribucion.Add(self.calendario, 0, wx.EXPAND)

        self.listaTexto.Hide()
        self.SetSizerAndFit(self.distribucion)

    def cambiarControl(self, event):
        if event.GetEventObject().GetId()==1:
            self.listaTexto.Show()
            fechaSeleccionada = self.calendario.GetValue()
            mes=None
            dia=None
            if fechaSeleccionada.GetMonth() in range(1,9):
                mes = str("0" + str(fechaSeleccionada.GetMonth()+1))
            else:
                mes = str(fechaSeleccionada.GetMonth()+1)
            if fechaSeleccionada.GetDay() in range(1,9):
                dia = str("0" + str(fechaSeleccionada.GetDay()))
            else:
                dia = str(fechaSeleccionada.GetDay())
            #print mes
            #print "dia", dia
            #fechaEnCadena = str(fechaSeleccionada.GetDay()) + "/" + str(fechaSeleccionada.GetMonth()+1) + "/"+ str(fechaSeleccionada.GetYear())
            fechaEnCadena = dia + "/" + mes + "/"+ str(fechaSeleccionada.GetYear())
            self.listaTexto.SetValue(fechaEnCadena)
            #self.listaTexto.Append(fechaEnCadena)
            self.calendario.Hide()
            self.listaTexto.SetSize(self.calendario.GetSize())
            #self.SetSizerAndFit(self.distribucion)
            #self.Parent.Fit()
        else:
            self.listaTexto.Hide()
            self.calendario.Show()
            #self.SetSizerAndFit(self.distribucion)
            #self.Parent.Fit()

    def GetValue(self):
        return self.listaTexto.GetValue()


    def SetValue(self, fecha):
        mes=None
        dia=None
        if fecha.GetMonth() in range(1,9):
            mes = str("0" + str(fecha.GetMonth()+1))
        else:
            mes = str(fecha.GetMonth()+1)
        if fecha.GetDay() in range(1, 9):
            dia = str("0"+ str(fecha.GetDay()))
        else:
            dia = str(fecha.GetDay())
        #print mes
        #self.listaTexto.SetValue(str(fecha.GetDay()) + "/" + str(fecha.GetMonth()+1) + "/" + str(fecha.GetYear()))
        self.listaTexto.SetValue(dia + "/" + mes + "/" + str(fecha.GetYear()))
        self.calendario.SetValue(fecha)


    def getCalendarCtrl(self):
        return self.calendario




class DatePickerCtrlV3(wx.Panel):
    def __init__(self, parent):
        '''Esta clase crea una pantalla para introduccion de fechas.
        En la parte superior tiene un combobox y cuando se hace click sobre el
        se despliega un calendario. La ventana padre se ajusta automaticamente...
        Al hacer click sobre el calendario, se establece la fecha en el combo
        y el calendario se oculta. La pantalla recalcula nuevamente su tamano.
        '''

        wx.Panel.__init__(self, parent)
        self.listaFecha = wx.ComboBox(self, -1)
        self.listaFecha.SetId(1)
        self.listaFecha.Bind(wx.EVT_LEFT_DOWN, self.activarControl)
        self.listaFecha.Bind(wx.EVT_COMBOBOX_DROPDOWN, self.activarControl)
        self.calendario = wx.calendar.CalendarCtrl(self, -1)
        self.calendario.Bind(wx.calendar.EVT_CALENDAR, self.activarControl)
        self.Bind(wx.calendar.EVT_CALENDAR_SEL_CHANGED, self.activarControl, self.calendario)

        self.distribucion = wx.BoxSizer(wx.VERTICAL)
        self.distribucion.Add(self.listaFecha, 0, wx.EXPAND)
        self.distribucion.Add(self.calendario)

        self.calendario.Hide()
        self.SetSizerAndFit(self.distribucion)

    def activarControl(self, event):
        if event.GetEventObject().GetId()==1:
            self.calendario.Show()
            self.SetSizerAndFit(self.distribucion)
            self.Parent.Fit()
        else:
            fechaSeleccionada = str(self.calendario.GetDate().GetDay()) + "/" + str(self.calendario.GetDate().GetMonth()) + "/" +str(self.calendario.GetDate().GetYear())
            self.listaFecha.SetValue(fechaSeleccionada)
            #self.listaFecha.Append(fechaSeleccionada)
            self.calendario.Hide()
            self.SetSizerAndFit(self.distribucion)
            self.Parent.Fit()

    def GetValue(self):
        return self.listaFecha.GetValue()

    def SetValue(self, fecha):
        self.listaFecha.SetValue(str(fecha.GetDay()) + "/" + str(fecha.GetMonth()+1) + "/" + str(fecha.GetYear()))
        self.calendario.SetDate(fecha)


class TimerCtrl(wx.Panel):
    def __init__(self, parent):
        import wx.lib.masked as masked
        wx.Panel.__init__(self, parent)
        #text2 = wx.StaticText( self, -1, "24-hour format:")
        spin2 = wx.SpinButton( self, -1, wx.DefaultPosition, (-1,-1), wx.SP_VERTICAL )
        self.time24 = masked.TimeCtrl(
                        self, -1, name="24 hour control", fmt24hr=True,
                        spinButton = spin2
                        )

        self.distribucion = wx.BoxSizer(wx.HORIZONTAL)
        self.distribucion.Add(self.time24, 0, wx.EXPAND)
        self.distribucion.Add(spin2, 0, wx.EXPAND)
        self.SetSizerAndFit(self.distribucion)

    def GetValue(self):
        return self.time24.Value


    def setValue(self, value):
        self.time24.SetValue(value)




class ListBoxListaAlimentos(wx.ComboBox):
    '''Este widget se utiliza en la vista de configuracion de tareas programadas
    para listar las configuraciones de alimentos...
    '''
    def __init__(self, parent):
        wx.ComboBox.__init__(self, parent)
        #self.AppendItems(["dsd", "dsd"])
        service_db= MySQLdb.connect(host='localhost', user='dcapeletti' , passwd='33426000', db='comedor')

        self.__bd = service_db
        cur = self.__bd.cursor()
        cur.execute("SELECT idCONFIG_PARAMETROS_KILOS, ALIMENTOS.Nombre, Kg FROM comedor.CONFIG_PARAMETROS_KILOS_ALIMENTOS inner join ALIMENTOS on CONFIG_PARAMETROS_KILOS_ALIMENTOS.Tipo_alimento = ALIMENTOS.id_ALIMENTO")
        for val in cur:
            self.Append(str(val[2]) + ' kg de ' +val[1], val[0])
            #print val[0]

        self.Bind(wx.EVT_COMBOBOX, self.OnComboBoxChange)
        self.__id_config_seleccionado=None


    def OnComboBoxChange(self, event):
        self.index = self.GetSelection()
        self.__id_config_seleccionado = self.GetClientData(self.index)
        #print 'Id config seleccionado: %u\n' % self.__id_config_seleccionado


    def GetIdSeleccionado(self):
        self.__id_config_seleccionado = self.GetClientData(self.FindString(self.GetValue()))
        return self.__id_config_seleccionado
