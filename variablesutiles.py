# coding=utf-8
'''
Created on 20/10/2015

@author: diegoariel
# Copyright 2015 Diego Ariel Capeletti
# Este archivo ha sido desarrollado para el control digital del servo
# que comanda la compuerta del proyecto ....
#
# This program is licensed as Open Source Software using the Apache License,
# Version 2.0 (the "License"); you may not use this file except in compliance
# with the License. You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# You are free to use, copy, distribute, and display the work, and to make
# derivative works. If you do, you must give the original author credit. The
# author specifically permits (and encourages) teachers to post, reproduce,
# and distribute some or all of this material for use in their classes or by
# their students.
'''

#--------------------------------------------------------------------------------
class Singleton (object):
    instance = None       
    def __new__(cls, *args, **kargs): 
        if cls.instance is None:
            cls.instance = object.__new__(cls, *args, **kargs)
        return cls.instance

#--------------------------------------------------------------------------------
class AdminVariables(Singleton):
    """
    Esta clase no lleva __init__ porque hereda de un Singleton, de otro modo no funcionará
    como un Singleton...
    Esta clase se puede instanciar todas las veces que se necesite, siempre contendrá los 
    mismos valores, excepto que se los cambien.
    """
        
    def setHostServer(self, host):
        self.host = host
        
    def getHostServer(self):
        return self.host
    
    def setNet(self, networking):
        self.net = networking
        
    def getNet(self):
        return self.net
    
    def setHostServerBD(self, serverBD):
        #Establece el nombre del servidor que contiene la BD...
        self.serverBD = serverBD
        
    def getHostServerBD(self):
        return self.serverBD
    
    def setNombreBD(self, nombreBd):
        #Establece el nombre a la base de datos que se tiene que conectar
        self.base_dato = nombreBd
        
    def getNombreBD(self):
        return self.base_dato
    
    def setConexionBd(self, valor):
        self.serviceBd=valor
    
    def getConexionBd(self):
        return self.serviceBd
        
        
        
        