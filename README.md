Cliente LibAgro, es un programa de escritorio que permite controlar el dosificador de alimento T4C (máquina) desde una computadora como el operador necesite.
Este programa interactúa con el ![servidor de LibAgro](https://gitlab.com/dcapeletti/ServerLibAgro), que se encuentra instalado en la máquina y es encargado de recibir los comandos del cliente, interpretarlos y enviar las señales al hardware.

![Demo_Software_cliente_LibAgro](https://ia601504.us.archive.org/17/items/DemoSoftwareClienteLibAgro/Demo%20Software%20cliente%20LibAgro.mp4)

Dentro de las características mas destacadas se encuentran:
* [x] Posibilidad de controlar una o mas compuertas de manera independiente o todas juntas al mismo tiempo.
* [x] Permite al usuario crear y gestionar las tareas programadas para la dosificación automática de alimento. Por ejemplo, establecer los días y horarios para cuándo se necesite entregar alimento según se requiera.
* [x] Permite al usuario establecer diversar configuraciones en el dosificador, como ser fecha, hora, entre otros. 
* [x] Posibilidad de determinar la cantidad de alimento que se encuentra en la tolva del dosificador.
* [x] Posibilidad de determinar el estado de la tapa superior de la tolva (abierta o cerrada).
* [x] Emite diversas alertas al usuario sobre distintos eventos que se producen en el dosificador de alimento (máquina). Por ejemplo, si una compuerta está abierta, si una tarea programada se está ejecutando, si la tapa superior está abierta o si hay un problema de comunicación con la máquina (problema de red, corte energía eléctrica, etc).
* [ ] Permite al usuario controlar y gestionar los eventos del hardware por email. Por ejemplo si una tarea programada no pudo ser ejecutada o si la tolva tiene un nivel mínimo de alimento. Característica en implementación no finalizada.
* [ ] Permite al usuario controlar el dosificador desde cualquier parte de internet. Característica próxima a implementarse.


**LICENCIA**

Cliente LibAgro es software libre y se distribuye bajo la licencia [GNU General Public License v3.0](https://gitlab.com/dcapeletti/ClienteLibAgro/blob/master/LICENSE)


**WIKI**

Si desea obtener mas ayuda, por ejemplo para instalar y configurar Cliente LibAgro visite https://gitlab.com/dcapeletti/ClienteLibAgro/wikis/Cliente-LibAgro