# coding=utf-8
'''
Created on 20/10/2015

@author: diegoariel
'''

class Observador():
    def actualizar(self, sujetoDato, eventoInteres):
        raise NotImplemented("Debe implementar este método.")
    

class SujetoDato(object):
    def __init__(self):
        self._observadores = [] #Tupla o lista de observadores. Se podría usar conjuntos (set) para no repetir observadores
    
    def addObserver(self, observador):
        self._observadores.append(observador)
        
    def removeObserver(self, observador):
        self._observadores.remove(observador)
        
    def notificarCambios(self, eventoInteres):
        for obs in self._observadores:
            obs.actualizar(self, eventoInteres)